#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt

if __name__ == "__main__":

    colors = ['b', 'g', 'r', 'm', 'k', 'c', 'y']
    symbols = ['o', '^', 'v', 's', 'd', 'p', 'h']
    symsize = [9, 10, 10, 8, 10, 10, 10]
    symevery = [5, 5, 5, 4, 5, 5, 5]
    labels = ['#CV=25K', '#CV=100K', '#CV=400K', 'case D', 'case E']

    jobs_1 = []
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_ksp_lines.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_fine_ksp_lines.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_sfine_ksp_lines.hst"))

    jobs = [jobs_1]
    jtags = ["all", "fine"]
    for jgroup, jtag in zip(jobs, jtags):

        plt.figure(1)
        plt.clf()

        for j, c, l, s, ssize, se in zip(jgroup, colors, labels, symbols, symsize, symevery):
            j.init_axis()

            plt.figure(1)

            np.square(j.yaxis_res_nl[1])
            np.square(j.yaxis_res_nl[2])
            np.square(j.yaxis_res_nl[3])
            np.square(j.yaxis_res_nl[4])
            np.square(j.yaxis_res_nl[5])
            j.yaxis_res_nl[0] = j.yaxis_res_nl[1] + j.yaxis_res_nl[2] + \
                j.yaxis_res_nl[3] + j.yaxis_res_nl[4] + j.yaxis_res_nl[5]
            np.sqrt(j.yaxis_res_nl[0])

            plt.semilogy(j.xaxis_nl, j.yaxis_res_nl[0], c + s + '-',
                         label=l, markevery=se, markersize=ssize)

        # -------
        plt.legend(loc='lower left', fontsize=20)
        plt.xlabel('PTC iteration', fontsize=24)
        plt.ylabel('$\| {R}({U}_h) \|_2$', fontsize=24)
        plt.tick_params(labelsize=24)

        plt.locator_params(axis='x', nbins=5)
        plt.locator_params(axis='y', numticks=5)
        plt.savefig('plots/res_' + jtag + '.eps', bbox_inches='tight')
