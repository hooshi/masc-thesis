#!/usr/bin/python

import os

cases = \
[
    '',
    '1_compare/plots/',
    '2_euler_sphere/plots/',
    '3_flat_plate/plots/',
    '4_naca/plots/',
]

def renewlink(casenum, name):
    target = 'results/%s%s' % (cases[casenum], name)
    link = 'result_%d_%s' % (casenum, name)
    
    os.system('cd ..; rm -rf %s' % link);
    os.system('cd ..; ln -s -T %s %s' % (target, link) );
    os.system('echo \'cd ..; ln -s -T %s %s\'' % (target, link) );

renewlink(1, 'res_comparison.eps')
renewlink(1, 'view_close.eps')
renewlink(1, 'view_whole.eps')

renewlink(2, 'entropy.eps')
renewlink(2, 'mesh.png')
renewlink(2, 'h4p2.eps')
renewlink(2, 'h4p3.eps')
renewlink(2, 'h4p4.eps')

renewlink(3, 'mesh.eps')
renewlink(3, 'mut.eps')
renewlink(3, 'anslib_nut.png')
renewlink(3, 'fun3d_nut.png')

