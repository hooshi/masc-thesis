#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 


if __name__ == "__main__":

    # ---------------------------------------------------
    # MESH SIZES
    # ---------------------------------------------------
    #sizes = [8.,14.,25.,40.];
    #sizes = [np.cbrt(15360), np.cbrt(63540), np.cbrt(322500), np.cbrt(1017600)];
    #hh = np.array([sizes[0]/sizes[0], sizes[1]/sizes[0], sizes[2]/sizes[0], sizes[3]/sizes[0]])

    sizes = [14.,25.,40.];
    #sizes = [np.cbrt(63540), np.cbrt(322500), np.cbrt(1017600)];
    hh = np.array([sizes[0]/sizes[0], sizes[1]/sizes[0], sizes[2]/sizes[0]])
    

    # ---------------------------------------------------
    # Entropy norms
    # ---------------------------------------------------
    S = []
    # a1
    S.append(np.array([2.405681e-02, 1.994755e-02, 1.592839e-02]))
    #S.append(np.array([2.144235e-02, 2.405681e-02, 1.994755e-02, 1.592839e-02]))
    #S.append(np.array([2.144235e-02, 2.405681e-02, 1.994755e-02, 1.592839e-02]))
    
    # a2, curve3, curve, facet
    S.append(np.array([4.636350e-04, 1.281985e-04, 4.683948e-05]))
    # S.append(np.array([2.769819e-03, 4.636350e-04, 1.281985e-04, 4.683948e-05]))a
    # S.append(np.array([2.525616e-03, 4.400782e-04, 1.191065e-04, 4.320160e-05]))

    # a1
    S.append(np.array([6.199170e-05, 1.294311e-05, 4.929109e-06]))
    #S.append(np.array([1.467434e-03, 6.199170e-05, 1.294311e-05, 4.929109e-06]))
    # S.append(np.array([1.532920e-03 , 6.933847e-05, 1.294312e-05, 4.801597e-06]))

    # a1
    S.append(np.array([1.633790e-05, 1.086930e-06, 1.091083e-07]))
    #S.append(np.array([3.421774e-04, 1.633790e-05, 1.086930e-06, 1.091083e-07]))
    # S.append(np.array([4.014887e-04, 1.576764e-05, 1.245873e-06, 1.727088e-07]))

    ppS = []
    bid=0
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[0][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[1][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[2][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[3][bid:]), 1) )
    print ppS[0]
    print ppS[1]
    print ppS[2]
    print ppS[3]

    
    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(111)

    kwargs = {'markersize':10, 'markerfacecolor':'none',
              'markeredgecolor':'k', 'markeredgewidth':'2',
              'linewidth':2}
    
    ln2, = ax.loglog(hh, S[1], 'k-o', **kwargs)
    ln3, = ax.loglog(hh, S[2], 'k--^', **kwargs)
    ln4, = ax.loglog(hh, S[3], 'k-.v', **kwargs)
             
    # ---------------------------------------------------
    # Annotations on plots themselves
    # ---------------------------------------------------
    FONT_SIZE=17

    xend = hh[-1]
    yend = S[1][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[1][0], xy=(xend, yend),
                xytext=(xend+0.4, yend),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=FONT_SIZE )

    xend = hh[-1]
    yend = S[2][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[2][0], xy=(xend, yend),
                xytext=(xend+0.4, yend*2),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=FONT_SIZE )

    xend = hh[-1]
    yend = S[3][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[3][0], xy=(xend, yend),
                xytext=(xend+0.4, yend*2),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=FONT_SIZE )
    

    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    ax.legend([ln2, ln3, ln4],
               ['$k=1$','$k=2$','$k=3$'],
               loc='upper right', fontsize=FONT_SIZE)
    
    plt.xlabel('$h_0/h$', fontsize=FONT_SIZE)
    plt.ylabel(r'$\| S_h-S_\infty \|_2$', fontsize=FONT_SIZE)
    plt.tick_params(labelsize=FONT_SIZE)

    ax.tick_params(axis='x',which='minor',labelsize=FONT_SIZE)
    ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.0f"))
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.0f"))

    #for label in ax.xaxis.get_ticklabels(1)[1::2]:
    #    label.set_visible(False)

    ax.set_xlim([0, 6])

    # plt.locator_params(axis='x', numticks=4)
    #plt.locator_params(axis='y', numticks=5)
    plt.savefig('plots/entropy.eps', bbox_inches='tight')
    plt.show()


