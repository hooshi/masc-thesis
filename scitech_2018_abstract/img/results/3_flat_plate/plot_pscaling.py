#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 


if __name__ == "__main__":

    # ---------------------------------------------------
    # Number of processors
    # ---------------------------------------------------
    nproc = np.array( range(1,11) )


    # ---------------------------------------------------
    # Time consumptions
    # ---------------------------------------------------
    totid = 0
    jacid = 1
    kspid = 2
    timeL= np.array \
          ([ \
             (15905, 9213, 6000, 4780, 3795, 3300, 3111, 2608, 2612, 2138), \
             (9272, 5052, 3317, 2633, 2022, 1789, 1530, 1347, 1199, 1066), \
             (6302, 3983, 2563, 2049, 1691, 1439, 1518, 1205, 1279, 962) \
          ])
    timeR= np.array \
           ([ \
              (16238,8934, 5644, 4958, 4069, 3590, 3458, 2903, 2549, 2200), \
              (9365, 4978, 3335, 2550, 2084, 1749, 1453, 1305, 1202, 1049), \
              (6559, 3785 ,2125, 2323, 1912, 1779, 1953, 1551, 1350, 1112) \
           ])

    timeL = timeL.astype(float)
    timeR = timeR.astype(float)

    speedupL = timeL
    speedupR = timeR
    for jj in reversed(range(timeL.shape[1])):
        speedupL[:,jj] = timeL[:,0] / timeL[:,jj]
        speedupR[:,jj] = timeR[:,0] / timeR[:,jj]

    plt.plot([1, 10], [1, 10], color='k', linewidth=2, linestyle='--',
             label='Ideal')
    plt.plot(nproc, speedupL[0,:], color='b', linewidth=2, marker='o',
             label='Total', markersize=10, linestyle='-')
    plt.plot(nproc, speedupL[1,:], color='r', linewidth=2, marker='^',
             label='Jac. int.', markersize=10, linestyle='-')
    plt.plot(nproc, speedupL[2,:], color='g', linewidth=2, marker='v',
             label= 'Lin. solve', markersize=10, linestyle='-')

    # plt.plot(nproc, speedupR[0,:], color='b', linewidth=2, marker='o',
    #          markersize=10, linestyle='-.',
    #          markerfacecolor='none', markeredgecolor='b', markeredgewidth=2)
    # plt.plot(nproc, speedupR[1,:], color='r', linewidth=2, marker='^',
    #          markersize=10, linestyle='-.',
    #          markerfacecolor='none',  markeredgecolor='r', markeredgewidth=2)
    # plt.plot(nproc, speedupR[2,:], color='g', linewidth=2, marker='v',
    #          markersize=10, linestyle='-.',
    #          markerfacecolor='none', markeredgecolor='g', markeredgewidth=2)
    
    plt.legend(loc='upper left', fontsize=20)
    plt.xlabel('# of processors', fontsize=20)
    plt.ylabel('Speed-up', fontsize=20)
    plt.tick_params(labelsize=20)


    plt.savefig('plots/scaling.eps', bbox_inches='tight')
    plt.show()
