%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Background
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\section{Discretization}

%% ---------------------------------------------------------------------------
%% FVM
%% ---------------------------------------------------------------------------
\subsection{Finite Volume Method}

\begin{frame}
  \frametitle{Finite Volume Method}
  \small

  \begin{overlayarea}{\linewidth}{4cm}
    \only<1-4>%
    {%
      \begin{exampleblock}{}
        \begin{itemize}
        \item<+-4>%
          Given a set of control volumes $\mathcal T_h$
        \item<+-4>%
          Find $\vvec u_h(\xyz; \vvec U_h)$
        \item<+-4>%
          $\vvec U_h$ $\equiv$ \dof{} vector $\equiv$ control volume
          average values
        \item<+-4>% 
          Equations must be in the conservative form:
          $ \frac{ \partial \vvec{u} }{ \partial t} + \nabla \cdot
          \left( \mmat{F}(\vvec u) - \mmat{Q}(\vvec u, \nabla \vvec{u})
          \right) = \vvec{S}(\vvec u, \nabla \vvec{u}) $
        \end{itemize}
      \end{exampleblock}
    }

    \only<5>{%
      \begin{block}{Using the divergence theorem}
        \begin{multline*}
          \frac{d\vvec U_{h,\tau}}{dt}
          + \frac{1}{\Omega_\tau}
          \int_{ \partial \tau }
          \left(
            \vvec \NumFluxF(\vvec u_h^+,\vvec u_h^-)-
            \vvec \NumFluxQ(
            \vvec u_h^+, \nabla \vvec u_h^+, \vvec u_h^-, \nabla \vvec u_h^-
            ) 
          \right) dS \\
          - \frac{1}{\Omega_\tau}
          \int_{\tau} \vvec S(\vvec u_h, \nabla \vvec u_h) d\Omega
          = 0
        \end{multline*}
      \end{block}
    }

    \only<6-7>{%
      \begin{block}{Discretized system of equations}
        \[ \frac{d \vvec U_h}{dt} + \vvec R( \vvec U_h) = 0  \]
      \end{block}
    }
  \end{overlayarea}

  \begin{overlayarea}{\linewidth}{4cm}
    \only<7>{%
      \begin{block}{Building blocks}
        \begin{itemize}
        \item $K$-exact reconstruction: Defining $\vvec u_h$ in terms
          of $\vvec U_h$
        \item Numerical fluxes $\NumFluxF$ and $\NumFluxQ$
        \end{itemize} 
      \end{block}
    }

    \only<1-6>{%
      \begin{figure}
        \centering
        \includegraphics[width=.5\linewidth]{img/flux_integral.pdf}
      \end{figure}
    }
  \end{overlayarea}
  
\end{frame}

%% ---------------------------------------------------------------------------
%% RANS + SA equations
%% ---------------------------------------------------------------------------
\subsection{RANS+ negative S-A Equations}

\begin{frame}
  \frametitle{RANS + Negative S-A Equations}

  
  \[
    \vvec u = 
    \begin{bmatrix} 
      \textcolor{invic}{\rho}       \\
      \textcolor{invic}{\rho \vel}  \\
      \textcolor{invic}{E}                 \\
      \textcolor{ransc}{\rho \nut}           
    \end{bmatrix} \quad
    \mmat F = 
    \begin{bmatrix} 
      \textcolor{invic}{\rho \vel^T}                  \\
      \textcolor{invic}{\rho \vel \vel^T + P \mmat I} \\
      \textcolor{invic}{(E+P) \vel^T}                 \\
      \textcolor{ransc}{\nut \rho \vel^T}
    \end{bmatrix} \quad
    \mmat Q =%
    \begin{bmatrix} %
      \textcolor{invic}{0} \\%
      \textcolor{viscc}{\mmat \tau} \\%
      \textcolor{viscc}{%
        (E+P) \mmat \tau \vel + \frac{R \gamma}{\gamma - 1}%
        \left( \frac{\mu}{\Pran} + \frac{\mu_T}{\Pran_T} \right)%
        \nabla T%
      } \\
      \textcolor{ransc}{%
        -\frac{1}{\sigma} ( \mu + \mu_T ) \nabla \nut%
      }%
    \end{bmatrix}
  \]

  \[
    \vvec S =
    \begin{bmatrix} 
      \textcolor{invic}{0}             \\
      \textcolor{invic}{0}             \\
      \textcolor{invic}{0}             \\
      \textcolor{ransc}{\Diff + \rho (\Prod-\Dist+\Trip)}
    \end{bmatrix}
  \]

  \large
  \centering
  \vspace{1em}
    \begin{tabular}{|c|c|c|}
      \hline
      \textcolor{invic}{\hspace{1em}Euler\hspace{1em}}
      &\textcolor{viscc}{Laminar Navier-Stokes}
      &\textcolor{ransc}{RANS + S-A} \\
      \hline
    \end{tabular}


\end{frame}

%% ---------------------------------------------------------------------------
%% K-exact recon
%% ---------------------------------------------------------------------------
\subsection{$K$-exact Reconstruction}

\begin{frame}
  \frametitle{$K$-exact reconstruction}

  \centering
  Average values $\vvec U_h \quad \longrightarrow \quad$
  piecewise continuous $\vvec u_h(\xyz)$
  % 
  \vspace{1cm}
  
  \centering
  \begin{figure}
    \includegraphics[width=.3\linewidth]{extra_img/nejat_recon_k1.pdf} \hfil
    \includegraphics[width=.3\linewidth]{extra_img/nejat_recon_k2.pdf} \\
    \makebox[.3\linewidth]{$k=1$} \hfil
    \makebox[.3\linewidth]{$k=2$}
  \end{figure}
\end{frame}


\begin{frame}
  \frametitle{$K$-exact reconstruction --- Continued}

  For every control volume $\tau$:
  \begin{equation*}
    u_h(\xyz; \vvec U_h)|_{x \in \tau} =
    u_{h,\tau}(\xyz; \vvec U_h) =
    \sum _{i=1}^{\nrec} a_{\tau}^i(\vvec U_h) \phi_{\tau}^i(\xyz),
  \end{equation*}
  where
  \begin{multline*}
    \left\{ \phi_\tau^i(\xyz) | i = 1 \ldots \nrec \right\}
    = \\
    \left\{ \frac{1}{a!b!c!}
      (x_1-x_{\tau1})^a
      (x_2-x_{\tau2})^b
      (x_3-x_{\tau3})^c
      | a+b+c \leq k \right\}.
  \end{multline*}
\end{frame}

\begin{frame}
  \frametitle{$K$-exact reconstruction --- Continued}
  \small

  \definecolor{k1color}{rgb}{0,   0, 0.5}
  \definecolor{k2color}{rgb}{0.5, 0, 0.5}
  \definecolor{k3color}{rgb}{0, 0.5, 0.5}
  
  \begin{itemize}
  \item Select a specific set of each control volume's neighbors as its
    reconstruction stencil $\Stencil(\tau)$
  \item $| \Stencil(\tau) | \geq \MinNeigh(k) \approx 1.5 \nrec(k)$
  \end{itemize}

  \vspace{1cm}
  \centering
  \begin{minipage}{.5\textwidth}
    \centering
    \includegraphics[width=.99\linewidth]{extra_img/stencil.pdf}
  \end{minipage}\hspace{1cm}\hfil
  % 
  \begin{minipage}{.3\textwidth}
    $k=1$
    \tikz{\fill[k1color]  circle(1ex);} \\
    $k=2$
    \tikz{\fill[k1color]  circle(1ex);}
    \tikz{\fill[k2color]  circle(1ex);}\\
    $k=3$
    \tikz{\fill[k1color]  circle(1ex);}
    \tikz{\fill[k2color]  circle(1ex);}
    \tikz{\fill[k3color]  circle(1ex);}\\
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{$K$-exact reconstruction --- Continued}

  \begin{itemize}
  \item Predict the average values of $\Stencil(\tau)$ members closely
  \item Satisfy conservation of the mean
  \end{itemize}

  \vspace{1cm}
  \begin{exampleblock}{}
    \[
    \begin{aligned}
      % 
      & \underset{a_{\tau}^1 \ldots a_{\tau}^{\nrec}}{\text{minimize}}
      & & \sum_{\sigma \in \Stencil(\tau)}
      \left(
        \frac{1}{\Omega_\sigma}\int_{\sigma}  u_{h,\tau}(\xyz) d\Omega -
        U_{h,\sigma} 
      \right)^2    
      \\
      & \text{subject to}
      & & \frac{1}{\Omega_\tau} \int_{\tau}
      u_{h}(\xyz) d\Omega = U_{h,\tau}
    \end{aligned}
    \]
  \end{exampleblock}{}

\end{frame}

%% ---------------------------------------------------------------------------
%% Flux functions
%% ---------------------------------------------------------------------------
\subsection{Numerical Flux Functions}

\begin{frame}
  \frametitle{Numerical Flux Functions}
  \begin{block}{Inviscid flux --- Roe's flux function}
    $\vvec \NumFluxF (\vvec u_h^+,\vvec u_h^-)$ = approximate flux in
    \[ \frac{\partial \vvec u}{ \partial t} + \frac{\partial \mmat
      F(\vvec u) \nvec n}{ \partial s} =0 \]
  \end{block}

  \begin{block}{Viscous flux --- averaging with damping}
    $
    \vvec \NumFluxQ
    (
    \vvec u_h^+, \nabla \vvec u_h^+,
    \vvec u_h^-, \nabla \vvec u_h^-
    ) =
    \mmat Q(\vvec u_h^*, \nabla \vvec u_h^*)\nvec n
    $ \\[1em]
    where
    $
    \vvec u_h^* = \frac{1}{2} \left( \vvec u_h^+ + \vvec u_h^-
    \right)
    $ \\[1em]
    and
    $
    \nabla \vvec u_h^* =
    \frac{1}{2} \left( \nabla \vvec u_h^+ + \nabla \vvec u_h^- \right) +
    \eta \left(
      \frac{\vvec u_h^+ - \vvec u_h^-}{ \|\xyz_{\tau+} - \xyz_{\tau-} \|_2  }
    \right)   \nvec n
    $
  \end{block}
  
\end{frame}



%%% Local Variables:
%%% TeX-master: "main"
%%% End:
