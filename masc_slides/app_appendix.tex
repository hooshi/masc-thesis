\section{Backup Slides}

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Intro
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\subsection{Intro}

\begin{frame}
  \frametitle{FV vs DG}

  \centering
  \includegraphics[width=0.9\linewidth]{extra_img/entropy_dof.pdf} \\
  {\scriptsize Inviscid flow around a circle \citeS{bassi1997}}

\end{frame}

%% ---------------------------------------------------------------------------
%% Basic definitions
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Common Goal}

  \begin{itemize}
  \item Given a PDE  $\vvec{\mathcal{L}}\vvec u(\xyz) = 0$    
  \item Find a discrete solution $\vvec u_h(\vvec x;\vvec U_h)$
  \item Such that the discretization error $\vvec e_h = \vvec u_h -\vvec u$
  \item Has an asymptotic behavior $\| \vvec e_h \| = \BigO(h^p)$
  \end{itemize}
  
  \vspace{1cm}
  \visible{
    \begin{exampleblock}{}
      The method is said to be $p$th-order accurate.\\ 
      \vspace{0.3cm}
      Traditional methods are second-order accurate.
    \end{exampleblock}
  }
  
\end{frame}

%% ---------------------------------------------------------------------------
%% Higher-order methods 
%% ---------------------------------------------------------------------------

\begin{frame}
  \frametitle{Higher-Order Methods --- Advantages}

  \textbf{Reduction of computational costs.}
  \vspace{0.5cm}
  
  \begin{itemize}
  \item When modeling errors are dominant:
    \begin{itemize}
    \item Limited level of reduction in discretization error is of interest.
    \item $hp$-adaptive methods.
    \end{itemize}
  \item When numerical errors are dominant:
    \begin{itemize}
    \item E.g., complicated full-body aircraft geometries.
    \item E.g., advanced turbulence modeling schemes.
    \item More accurate solution are valuable ( 1\% better accuracy
      in finding drag ).
    \item Accurate solutions can be obtained on coarser meshes.
    \end{itemize}
  \item Unstructured finite volume methods
    \begin{itemize}
    \item Complex geometries
    \item Fewer number of degrees of freedom.
    \item Easier integration into commercial solvers.
    \end{itemize}

  \end{itemize}

\end{frame}

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Recon
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{$K$-exact reconstruction --- Continued}

  \begin{itemize}
  \item Satisfy conservation of the mean.
  \item Predict the average values of $\Stencil(\tau)$ closely.  
  \end{itemize}

  \vspace{1cm}
  \begin{overlayarea}{\linewidth}{4cm}
    \begin{exampleblock}{}
    \only<1>{
      \[
      \begin{aligned}
        % 
        & \underset{a_{\tau}^1 \ldots a_{\tau}^{\nrec}}{\text{minimize}}
        & & \sum_{\sigma \in \Stencil(\tau)}
        \left(
          \frac{1}{\Omega_\sigma}\int_{\sigma}  u_{h,\tau}(\xyz) d\Omega -
          U_{h,\sigma} 
        \right)^2    
        \\
        & \text{subject to}
        & & \frac{1}{\Omega_\tau} \int_{\tau}
        u_{h}(\xyz) d\Omega = U_{h,\tau}
      \end{aligned}
    \]
  }

  \only<2>{
    \[
      I_{\tau\sigma}^i = \int_{\sigma} \phi_\tau^i(\xyz) d\Omega
      \qquad \sigma \in \Stencil(\tau) \cup \{\tau \}
    \]

    \[
      \label{eq:recon_min_mat}
      \begin{bmatrix} 
        I_{\tau\tau}^1         &\dots  &I_{\tau\tau}^{\nrec}        \\
        \hline 
        I_{\tau\sigma_1}^1      &\dots  &I_{\tau\sigma_1}^{\nrec}     \\
        \vdots               &\ddots & \vdots                \\
        I_{\tau\sigma_{\text{NS}(\tau)}}^1   &\dots  &I_{\tau\sigma_{\text{NS}(\tau)}}^{\nrec}
      \end{bmatrix}
      \begin{bmatrix} 
        a_\tau^1 \\
        \vdots \\
        a_\tau^{\nrec} 
      \end{bmatrix} 
      =
      \begin{bmatrix} 
        U_{h,\tau}  \\
        \hline
        U_{h,\sigma_1}  \\
        \vdots \\
        U_{h,\sigma_{\text{NS}(\tau)}}  
      \end{bmatrix}
    \]
  }

  \only<3->{
    \[
      \begin{bmatrix} 
        a_\tau^1 \\
        \vdots \\
        a_\tau^{\nrec} 
      \end{bmatrix}
      =
      \mmat A_\tau^{\dagger} 
      \begin{bmatrix} 
        U_{h,\tau}  \\
        U_{h,\sigma_1}  \\
        \vdots \\
        U_{h,\sigma_{\text{NS}(\tau)}}  
      \end{bmatrix}
    \]
  }
  \end{exampleblock}{}
  \end{overlayarea}

\end{frame}

\subsection{Recon Optimization}
\begin{frame}
  \frametitle{Reconstruction Optimization Problem}
  
  \begin{itemize}
  \item $Ax =  b$ subject to $Bx=0$
  \item Change of variables $x=By$ where the columns of $B$ are the
    null space of $A$.
  \item $Ax=0$ reduces to $(AB=C)y=0$ which is always satisfied.
  \item Solve the unconstrained problem $Cy=b$, i.e., $y=C^\dagger b$
    \begin{itemize}
    \item QR (Householder or Gram-schmidt): $C=Q_1 R_1$ ,
      $C^\dagger = R_1^{-1}Q^T$
    \item SVD (most stable): $C=U\Sigma V^T$, $C^\dagger= W^T \Sigma^{-1} U$
    \item Normal equations: $C^\dagger=(CC^T)^{-1}C^T$
    \end{itemize}
  \item Finally, $x=By$.
  \end{itemize}
\end{frame}


%% ---------------------------------------------------------------------------
%% Flux
%% ---------------------------------------------------------------------------
\subsection{Numerical Flux Functions}

\begin{frame}
  \frametitle{Numerical Flux Functions}
  \begin{block}{Inviscid Flux --- Roe's Flux Function}
    $\vvec \NumFluxF (\vvec u_h^+,\vvec u_h^-)$ = approximate solution
    for $\mmat F(s=0) \nvec n$ in $
    \begin{cases}
      \frac{\partial \vvec u}{ \partial t} + \frac{\partial \mmat
        F(\vvec u) \nvec n}{ \partial s} =0 \\
      \vvec u(s<0,t=0) = u_h^- \\
      \vvec u(s>0,t=0) = u_h^+\\
    \end{cases}
    $
  \end{block}

  \begin{block}{Inviscid Flux --- Averaging with Damping}
    $
    \vvec \NumFluxQ
    (
    \vvec u_h^+, \nabla \vvec u_h^+,
    \vvec u_h^-, \nabla \vvec u_h^-
    ) =
    \mmat Q(\vvec u_h^*, \nabla \vvec u_h^*)\nvec n,
    $ \\[1em]
    where
    $
    \vvec u_h^* = \frac{1}{2} \left( \vvec u_h^+ + \vvec u_h^-
    \right),
    $ \\[1em]
    and
    $
    \nabla \vvec u_h^* =
    \frac{1}{2} \left( \nabla \vvec u_h^+ + \nabla \vvec u_h^- \right) +
    \eta \left(
      \frac{\vvec u_h^+ - \vvec u_h^-}{ \|\xyz_{\tau+} - \xyz_{\tau-} \|_2  }
    \right)   \nvec n
    $
  \end{block}
  
\end{frame}

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Scaling
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------

\subsection{Parallel Scaling}
\begin{frame}
  \frametitle{Parallel Scaling}

  \begin{block}{Strong Scaling Test}
    \begin{itemize}
    \item Solving the same problem with different number of processors
    \item Inviscid flow, sphere: $\ncv=322$K and $k=3$
    \item Turbulent flow, flat plate: $128\times 68 \times 14$ mesh and $k=3$
    \end{itemize}
  \end{block}

  \centering
  \footnotesize
  \includegraphics[width=0.45\linewidth]{img/result_2_scaling.eps}
  \includegraphics[width=0.45\linewidth]{img/result_3_scaling.eps}  \\
  \makebox[0.45\linewidth][c]{Sphere}
  \makebox[0.45\linewidth][c]{Flat plate}
\end{frame}

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Non-dimensionalization
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\subsection{Non-dimensionalization}

%% ---------------------------------------------------------------------------
%% Flow Variables
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Nondimensionalization -- Flow Variables}

  Reference values:
  \[
    \begin{array}{cccc}
      \rho^* \sim \rho_\infty
      &\vel^* \sim c_\infty
      &T^* \sim \frac{c_\infty}{\gamma R}
      &P^* \sim \rho_\infty c_\infty^2 \\
      t^* \sim \frac{L}{c_\infty}  
      &\mu^* \sim \mu_\infty  
      &\mu^*_T \sim  \mu_\infty 
      &\nu^*_T  \sim  \frac{\mu_\infty}{\rho_\infty} \\
      \nut^*  \sim  \mu' 
      & \mmat \tau \sim \frac{\mu_\infty c_\infty}{L}
      &d \sim L & \\
    \end{array}
  \]

  Pressure and temperature: 
  \[ \arraycolsep=1.4pt\def\arraystretch{2.2}
    \begin{array}{ccc}
      c^* = \sqrt{\frac{\gamma P^*}{\rho^*}}
      &\Rightarrow
      & c = \sqrt{\frac{\gamma P}{\rho}} \\
      P^* = \rho^* R T^*
      &\Rightarrow
      &P = \frac{\rho T}{\gamma} \\
      E^* = \rho \frac{R (\gamma-1)}{\gamma} T +
      \frac{1}{2} (\vel^* \cdot \vel^*)
      &\Rightarrow
      & P = (\gamma-1) \left( E - \frac{1}{2}\rho(\vel \cdot \vel) \right) \\
    \end{array}
  \]
  
  Dimensionless numbers:
    \[
    \begin{array}{ccc}
      \Mach = \frac{v_\infty}{c_\infty}
      & \Reyn = \frac{\rho_\infty v_\infty L}{\mu_\infty}
      & \Pran = \frac{c_p \mu}{k}\\
    \end{array}
  \]
\end{frame}

%% ---------------------------------------------------------------------------
%% Lift and Drag
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Nondimensionalization --- Lift and Drag}

  \[ \text{Pressure Force} \sim \rho_\infty c_\infty^2 L^2\]
  \[ \text{Viscous Force} \sim \mu_\infty c_\infty L^2\]
  
  
  \[ \arraycolsep=1.4pt\def\arraystretch{2.2}
    \begin{array}{ccc}
      C_{D} = \frac{D^*}{(1/2) \rho_\infty v_\infty^2 A}
      &\Rightarrow
      & C_{D} = \frac{D}{(1/2) \Mach^2 (A/L^2)} \\
      C_{Dv} = \frac{D^*}{(1/2) \rho_\infty v_\infty^2 A}
      &\Rightarrow
      & C_{Dv} = \frac{D}{(1/2) \Mach \Reyn (A/L^2)} \\
      C_{f} = \frac{\nvec{m}^T \mmat{\tau}^* \nvec{n}}
      {(1/2) \rho^* v_\infty^2 A}
      &\Rightarrow
      & C_{f} = \frac{\nvec{m}^T \mmat{\tau} \nvec{n}}
        {(1/2) \rho \Mach \Reyn (A/L^2)} \\
    \end{array}
  \]
  
\end{frame}


%% ---------------------------------------------------------------------------
%% Sutherland's Law
%% ---------------------------------------------------------------------------
\begin{frame}
    \frametitle{Nondimensionalization --- Sutherland's Law}
    
  \[
    \frac{\mu^*}{\mu_\rref}
    =
    \left( \frac{T^*}{T_\rref} \right) ^ {3/2}
    \frac{1+(S^*/T_\rref)}{(T^*/T_\rref)+(S^*/T_\rref)}
  \]

  \[
    \mu 
    =
    T
    \frac{\mu_\infty}{\mu_\rref} \frac{(T_\rref/T_\infty) + S}{T+S}
  \]
  \vspace{0.5cm}

  \[
    \begin{array}{ccc}
      S = 110.4 K &T_\rref = 273.15 K &\mu_\rref=1.716 \times 10^{-5}
    \end{array}
  \]
\end{frame}

%% ---------------------------------------------------------------------------
%% Flux Matrices
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Nondimensionalization --- Flux Matrices}

    \[
    \mmat F^* = 
    \begin{bmatrix} 
      \rho^* \vel^{*T}                  \\
      \rho^* \vel^* \vel^{*T} + P^* \mmat I \\
      (E^*+P^*) \vel^{*T}                 \\
      \nut^* \rho^* \vel^{*T}
    \end{bmatrix} \quad
    \mmat Q^* =
    \begin{bmatrix} 
      0                  \\
      \mmat \tau^*         \\
      (E^*+P^*) \mmat \tau^* \vel^* + \frac{R \gamma}{\gamma - 1}
      \left( \frac{\mu^*}{\Pran} + \frac{\mu^*_T}{\Pran_T} \right) \nabla T^* \\
      -\frac{1}{\sigma} ( \mu^* + \mu^*_T ) \nabla \nut^*
    \end{bmatrix}
  \]

  \vspace{1cm}
  \[
    \mmat F = 
    \begin{bmatrix} 
      \rho \vel^T                  \\
      \rho \vel \vel^T + P \mmat I \\
      (E+P) \vel^T                 \\
      \nut \rho \vel^T          
    \end{bmatrix}
    % 
    \quad
    % 
    \mmat Q =
    \begin{bmatrix} 
      0                                      \\
      \frac{\Mach}{\Reyn} \mmat \tau         \\
      (E+P) \mmat \tau \vel +
      \frac{1}{\gamma - 1}
      \left( \frac{\mu}{\Pran} + \frac{\mu_T}{\Pran_T} \right) \nabla T \\
      -\frac{\Mach}{\Reyn \sigma} ( \mu + \mu_T ) \nabla \nut
    \end{bmatrix}
  \]
\end{frame}

%% ---------------------------------------------------------------------------
%% Spalart-Allmaras
%% ---------------------------------------------------------------------------
%% In thesis.

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% Precon
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\subsection{Solution Scheme}

%% ---------------------------------------------------------------------------
%% PTC
%% ---------------------------------------------------------------------------
\subsection{Solution Scheme}
\begin{frame}{Solution Scheme --- PTC}
  \footnotesize

  \begin{itemize}
  \item Seeking the steady state solution of:
    \[ \frac{d \vvec U_h}{dt} + \vvec R( \vvec U_h) = 0 \]
  \item Newton: \\
    \[ \frac{\partial \vvec R}{\partial \vvec U_h} \delta \vvec U_h =
    -\vvec R ( \vvec U_h ), \quad \vvec U_h \gets \vvec{U_h} +
    \vvec{\delta U_h} \]
  \item Backward Euler: \\
    \[ \frac{ \vvec U_h^+ - \vvec U^{}_h }{\Delta t} + \vvec R ( \vvec
    U_h ) = 0\]
  \item Pseudo transient continuation: \\
    \[ \left( \frac{\mmat V}{\Delta t} + \frac{\partial \vvec R}{\partial
        \vvec U_h} \right) \delta \vvec U_h = -\vvec R ( \vvec U_h ) \]
  \item A linear system must be solved: \\
    \[ \mmat A \vvec x = \vvec b \quad (*) \]
  \end{itemize}  
\end{frame}

\begin{frame}{Solution Scheme --- Preconditioning}
  \begin{itemize}
  \item GMRES can stall
  \item Right preconditioning
    $ \quad \mmat {AP} \vvec y = \vvec b, \quad \vvec x = \mmat P \vvec y $
  \item Incomplete LU factorization; fill level $p$
    \begin{itemize}
    \item $\mmat A^* \approx \IL \IU$
    \item $P=(\IL \IU)^{-1}$ ( to find $\vvec v= \mmat P \vvec z$,
      solve $(\IL \IU) \vvec v= \vvec z$ )
    \item Reordering
      $\quad \mmat{\sigma A \sigma^T P } \vvec y = \mmat\sigma \vvec b$
    \end{itemize}
  \end{itemize}

  \begin{overlayarea}{\linewidth}{4cm}
    \vspace{1em}
    \centering
    \setlength{\tempwidth}{0.24\linewidth}
    \only<1>{
    \includegraphics[width=\tempwidth]{extra_img/mat_A}
    \includegraphics[width=\tempwidth]{extra_img/mat_L0}
    \includegraphics[width=\tempwidth]{extra_img/mat_U0}
    \includegraphics[width=\tempwidth]{extra_img/mat_LU0} \\
    \makebox[\tempwidth][c]{$\mmat A$}%
    \makebox[\tempwidth][c]{$\IL(0)$}%
    \makebox[\tempwidth][c]{$\IU(0)$}%
    \makebox[\tempwidth][c]{$\IL\IU(0)$} }
    \only<2>{
    \includegraphics[width=\tempwidth]{extra_img/mat_A}
    \includegraphics[width=\tempwidth]{extra_img/mat_L1}
    \includegraphics[width=\tempwidth]{extra_img/mat_U1}
    \includegraphics[width=\tempwidth]{extra_img/mat_LU1} \\
    \makebox[\tempwidth][c]{$\mmat A$}%
    \makebox[\tempwidth][c]{$\IL(1)$}%
    \makebox[\tempwidth][c]{$\IU(1)$}%
    \makebox[\tempwidth][c]{$\IL\IU(1)$} }
  \only<3>{
    \includegraphics[width=0.6\linewidth]{extra_img/rcm} \\
    \makebox[0.35\linewidth][c]{$\mmat A$}%
    \makebox[0.35\linewidth][c]{$\mmat{\sigma A \sigma}^T$}%
  }
  \end{overlayarea}
\end{frame}


%% ---------------------------------------------------------------------------
%% Lines
%% ---------------------------------------------------------------------------
\begin{frame}{Solution Scheme --- Lines of Strong Unknown Coupling}

  \begin{itemize}
  \item Assign binary weights $W_{\tau\sigma}$.
  \item Advection-diffusion equation
    $\nabla \cdot (\vel u - \mu_L \nabla u) = 0$.
  \item
    $W_{\tau\sigma} = \max \left( \frac{\partial R_\sigma }{\partial
        u_\tau}, \frac{\partial R_\tau }{\partial u_\sigma} \right )$
  \item Greedy clustering algorithm
    \begin{enumerate}
    \item Pick an unmarked control volume $\tau$
    \item Pick the neighbour $\sigma$ with the highest weight
    \item If $\sigma$ is marked go to 1.
    \item Add $\sigma$ to line and mark it.
    \item $\tau=\sigma$, go to 2.
    \end{enumerate}
  \end{itemize}

\end{frame}


%% ---------------------------------------------------------------------------
%% Lines
%% ---------------------------------------------------------------------------
\begin{frame}{Solution Scheme --- Comparison Details}
  \footnotesize
  \centering
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    Preconditioner &\nptc   &\ngmres
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=25$K} \\
    \hline
          A  &$37$   &$956$      &$1.4$      &$416$   &$635$  \\
          B  &$44$   &$10,893$   &$0.7$      &$264$   &$572$  \\
          C  &$51$   &$13,692$   &$0.7$      &$328$   &$705$  \\
          D  &$34$   &$1,856$    &$0.7$      &$134$   &$379$   \\
          E  &$34$   &$1,787$    &$0.7$      &$118$   &$361$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=100$K} \\
    \hline
          A  &$39$  &$4,640$   &$5.9$      &$3,122$  &$4,068$     \\
          B  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          C  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          D  &$-$   &$-$       &$3.2$      &$-$      &$-$         \\
          E  &$36$  &$4,396$   &$3.2$      &$1,308$  &$2,348$      \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
  \end{tabular}
\end{frame}

%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
%% 3-D results
%% ---------------------------------------------------------------------------
%% ---------------------------------------------------------------------------
\subsection{3-D Results}

%% ---------------------------------------------------------------------------
%% Poisson's Equations
%% ---------------------------------------------------------------------------
\subsection{Poisson's Equation in a Cubic Domain}

\begin{frame}
  \frametitle{Poisson's Equation}

  \begin{itemize}
  \item $\nabla^2 u = f$ 
  \item Manufactured solution
    $u = \sinh(\sin(x_1))\sinh(\sin(x_2))\sinh(\sin(x_3))$ 
  \item Domain $\Omega=[0\quad 1]^3$
  \item Dirichlet boundary conditions
  \end{itemize}

  \begin{figure}
    \centering
    \includegraphics[width=0.3\linewidth]{img/result_1_exact.png}\hspace{0.2in}
    \includegraphics[width=0.07\linewidth]{img/result_1_bar.png}
  \end{figure}

\end{frame}


\begin{frame}
  \frametitle{Poisson's Equation --- Accuracy Analysis}

  \begin{figure}
    \small
    \centering
    \includegraphics[width=0.65\linewidth]{img/result_1_norms.eps} \hfil \\
    Error versus mesh length scale \hfil \\
  \end{figure}
  
  Poor performance of $k=2$ is expected \citeS{ollivier2002}.

\end{frame}


\begin{frame}
  \frametitle{Poisson's Equation --- Meshes}

  \footnotesize

  \begin{exampleblock}{}
    \centering
    $N=10,20,40$.
  \end{exampleblock}
  
  \begin{figure}
    \centering
    \setlength{\tempwidth}{0.24\linewidth}
    \includegraphics[width=\tempwidth]{img/result_1_mesh_hex.png} \hfil %
    \includegraphics[width=\tempwidth]{img/result_1_mesh_prism.png} \\
    \makebox[\tempwidth][c]{Hexahedra}\hfil%
    \makebox[\tempwidth][c]{Prisms}\hfil \\
    \includegraphics[width=\tempwidth]{img/result_1_mesh_pyramid.png} \hfil%
    \includegraphics[width=\tempwidth]{img/result_1_mesh_tet.png} \\
    \makebox[\tempwidth][c]{Pyramids+Tetrahedra}\hfil%
    \makebox[\tempwidth][c]{Tetrahedra}\hfil 
  \end{figure}

\end{frame}

%% ---------------------------------------------------------------------------
%% Sphere
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Inviscid Flow Around Sphere --- Mach Contours}

  \small
  \begin{figure}
    \setlength{\tempwidth}{0.25\linewidth}
    \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/result_2_h2p2.eps}}
    \centering
    \hspace{\baselineskip}
    \columnname{$k=1$}\hfil
    \columnname{$k=2$}\hfil
    \columnname{$k=3$} \\[1em]
    % 
    \rowname{$\ncv = 64$K} \hfil
    \includegraphics[width=\tempwidth]{img/result_2_h2p2.eps}\hfil
    \includegraphics[width=\tempwidth]{img/result_2_h2p3.eps}\hfil
    \includegraphics[width=\tempwidth]{img/result_2_h2p4.eps} \\[1em]
    % 
    \rowname{$\ncv = 1$M} \hfil
    \includegraphics[width=\tempwidth]{img/result_2_h4p2.eps}\hfil
    \includegraphics[width=\tempwidth]{img/result_2_h4p3.eps}\hfil
    \includegraphics[width=\tempwidth]{img/result_2_h4p4.eps} \\[1em]
    \caption{Computed Mach contours on the $x_3=0$ symmetry plane for the sphere
      problem}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{Sphere --- Convergence}

  \begin{itemize}
  \item Norm of the residual vector per PTC iteration
  \item Free-stream state as initial conditions.
  \end{itemize}  

  \vspace{1cm}
  \centering
  \includegraphics[width=0.8\linewidth]{img/result_2_ptc.eps}

\end{frame}

\begin{frame}
  \frametitle{Sphere --- Performance}
  \scriptsize\centering

  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=64$K} \\
    \hline
    $1$  &$15$   &$182$      &$2.94$      &$24$   &$107$  \\
    $2$  &$15$   &$181$      &$3.91$      &$20$   &$112$  \\
    $3$  &$15$   &$255$      &$6.00$      &$31$   &$320$  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=322$K} \\
    \hline
    $1$  &$16$   &$207$   &$13.24$      &$134$   &$579$     \\
    $2$  &$16$   &$212$   &$14.05$      &$132$   &$620$     \\
    $3$  &$16$   &$300$   &$28.39$      &$271$   &$1,879$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=1$M} \\
    \hline
    $1$  &$17$   &$275$      &$39.48$      &$486$   &$1,969$  \\
    $2$  &$17$   &$277$      &$45.52$      &$536$   &$2,150$  \\
    $3$  &$17$   &$385$      &$85.78$      &$793$   &$5,904$  \\
    \hline
  \end{tabular}

\end{frame}

%% ---------------------------------------------------------------------------
%% Flat plate
%% ---------------------------------------------------------------------------
\begin{frame}
  \frametitle{Flat Plate --- Convergence}

  \begin{itemize}
  \item Norm of the residual vector per PTC iteration
  \item Solution of $(k+1)$-exact scheme is initialized with that of $k$-exact.
  \end{itemize}  

  \begin{figure}
    \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
    \centering
    \includegraphics[height=\tempheight]{img/result_3_ptc.eps}
  \end{figure}

\end{frame}

\begin{frame}
  \frametitle{Flat Plate --- Drag}
  \footnotesize
  
  Computed value and convergence order of the drag coefficient and the
  skin friction coefficient at the point $\vvec x =(0.97,0,0.5)$

  \vspace{1cm}
  \scriptsize 
  \begin{tabular}{|c|ccc|ccc|}
    \cline{2-7}
    \multicolumn{1}{c|}{} &\multicolumn{3}{c|}{$C_D$}
                          &\multicolumn{3}{c|}{$C_f$} \\
    \cline{2-7}
    %%%%%%%%%% 
    \hline
    NASA TMR    &\multicolumn{3}{c|}{$0.00286$}
                &\multicolumn{3}{c|}{$0.00271$} \\
    \hline
    %%%%%%%%%%% 
    \hline
    \diagbox{Mesh}{$k$} &$1$ &$2$ &$3$ &$1$ &$2$ &$3$ \\
    \hline
    $60\times34\times7$  &$0.00396$ &$0.00233$ &$0.00233$
    &$0.00350$ &$0.00228$ &$0.00222$ \\
    $120\times68\times14$  &$0.00301$ &$0.00281$ &$0.00285$
    &$0.00283$ &$0.00268$ &$0.00271$ \\
    $240\times136\times28$  &$0.00287$ &$0.00286$ &$0.00286$
   &$0.00274$ &$0.00273$ &$0.00273$ \\
    \hline
    %%%%%%%%%%%% 
    \hline
    Convergence order    &$2.8$ &$3.3$ &$5.4$
                          &$3$ &$3$ &$5.1$ \\
    \hline
  \end{tabular}
\end{frame}

\begin{frame}
  \frametitle{Flat Plate --- Performance}
  \scriptsize\centering
  
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$60\times34\times7$ mesh} \\
    \hline
    $1$  &$26$   &$844$      &$0.42$     &$31$   &$55$    \\
    $2$  &$26$   &$1,009$     &$1.35$     &$42$   &$124$   \\
    $3$  &$26$   &$1,071$     &$2.10$     &$57$   &$202$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$120\times68\times14$ mesh} \\
    \hline
    $1$  &$28$   &$1,436$   &$5.24$      &$510$   &$713$     \\
    $2$  &$29$   &$1,864$   &$8.30$      &$742$   &$1,489$     \\
    $3$  &$29$   &$2,041$   &$14.63$     &$825$   &$2,124$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$240\times136\times28$ mesh} \\
    \hline
    $1$  &$29$   &$2,492$      &$38.77$     &$6,222$   &$7,884$  \\
    $2$  &$27$   &$3,305$      &$60.70$     &$12,353$   &$18,137$  \\
    $3$  &$27$   &$2,906$      &$121.81$    &$23,141$   &$35,412$  \\
    \hline
  \end{tabular}

\end{frame}

%% ---------------------------------------------------------------------------
%% Extruded Airfoil
%% ---------------------------------------------------------------------------

\begin{frame}
  \frametitle{Extruded \nacaoolz{} --- $\nut$}
  \scriptsize
  Distribution of the turbulence working variable for the
  extruded \nacaoolz{} problem on the $x_3=0$ plane, $k=3$.
  
\begin{figure}
  \centering
  \newcommand{\tempwidtha}{0.6\linewidth}
  \newcommand{\tempwidthb}{0.08\linewidth}
  \includegraphics[width=\tempwidtha]{img/result_4_nut_hex.png}\hfil
  \makebox[\tempwidthb][c]{} \\
  %%%%% 
  \makebox[\tempwidtha][c]{(a) Hexahedral mesh}  \hfil
  \makebox[\tempwidthb][c]{}\hfil \\
  %%%% 
  \includegraphics[width=\tempwidtha]{img/result_4_nut_mix.png} \hfil
  \includegraphics[width=\tempwidthb]{img/result_4_nut_bar.png} \\
  %%%% 
  \makebox[\tempwidtha][c]{(b) Mixed prismatic-hexahedral mesh} \hfil
  \makebox[\tempwidthb][c]{}
  %%%%% 
\end{figure}

\end{frame}

\begin{frame}
  \frametitle{Extruded \nacaoolz{} --- Drag}
  \footnotesize
  
  Computed value and convergence order of the drag coefficient and the
  skin friction coefficient at the point $\vvec x =(0.97,0,0.5)$

  \vspace{1cm}
  \footnotesize\centering
  
  \begin{tabular}{*{4}{c}}
    \hline
    $k$    &$C_{Dp}$ &$C_{Dv}$ &$C_L$ \\
    \hline
    \multicolumn{4}{c}{NASA TMR} \\
    \hline
    $-$      &$0.00607$  &$0.00621$ &$1.0910$      \\
    \hline
    \multicolumn{4}{c}{Hex mesh} \\
    \hline
    $1$     &$0.01703$  &$0.00582$ &$1.0619$   \\
    $2$     &$0.01702$  &$0.00497$ &$1.0507$   \\
    $3$     &$0.00301$  &$0.00472$ &$1.0417$   \\
    \hline
    \multicolumn{4}{c}{Mixed mesh} \\
    \hline
    $1$     &$0.01129$  &$0.00574$ &$1.0735$   \\
    $2$     &$0.00365$  &$0.00565$ &$1.0776$   \\
    $3$     &$0.00550$  &$0.00536$ &$1.0869$   \\
    \hline
    % \multicolumn{4}{c}{$2$-D Mixed mesh} \\
    % \hline
    % $1$     &$0.01095$  &$0.00704$  &$1.0778$  \\
    % $2$     &$0.00380$  &$0.00556$  &$1.0817$  \\
    % $3$     &$0.00545$  &$0.00583$  &$1.0951$  \\
    % \hline
  \end{tabular}

\end{frame}

\begin{frame}
  \frametitle{Extruded \nacaoolz{} --- Performance}
  \footnotesize\centering
  
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)                       \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Hex mesh, $\ncv=100$K}                       \\
    \hline
    $1$  &$33$   &$1,154$      &$4.77$      &$317$      &$744$      \\
    $2$  &$31$   &$1,788$      &$6.82$      &$730$      &$2,097$    \\
    $3$  &$31$   &$2,415$      &$12.23$      &$1,057$    &$3,215$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Mixed mesh, $\ncv=176$K}                 \\
    \hline
    $1$  &$34$   &$1,132$   &$8.70$      &$458$     &$1,164$    \\
    $2$  &$32$   &$1,769$     &$10.87$   &$800$     &$2,427$    \\
    $3$  &$31$   &$2,185$     &$26.47$   &$1,311$   &$4,666$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
  \end{tabular}


\end{frame}


%%% Local Variables:
%%% TeX-master: "main"
%%% End:
