% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass{beamer}

% path to graphics
\graphicspath{{img/}}

% write code
\usepackage{listings}
\lstset{basicstyle=\footnotesize,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue},
}

\beamertemplatenavigationsymbolsempty

% There are many different themes available for Beamer. A comprehensive
% list with examples is given here:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
% You can uncomment the themes below if you would like to use a different
% one:
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{default}
\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
%usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\title{libMesh}

% A subtitle is optional and this may be deleted
\subtitle{A New User's Experience}

\author{Shayan Hoshyari}

\institute[University of British Columbia] 
{
}

\date{April, 2016}

% subject for pdf file
\subject{Finite Element Packages}

% add some logo and stuff
\pgfdeclareimage[height=1cm]{libmesh-logo}{img/libmesh-mesh-small}
\logo{\pgfuseimage{libmesh-logo}}

% Let's get started
\begin{document}

% ****************************************************************************
% *                             Title page
% ****************************************************************************
\begin{frame}
  \titlepage
\end{frame}

% ****************************************************************************
% *                           Table of contents
% ****************************************************************************
\begin{frame}{Outline}
  \tableofcontents
\end{frame}



% ****************************************************************************
% *                             First Section
% ****************************************************************************
\section{Getting Started}

% ----------------------------------------------------------------------------
% -                           Introduction
% ----------------------------------------------------------------------------
\subsection{Introduction}

% ----------------------------------------------------------------------------
\begin{frame}{Motivation}
  Major components of a mesh based numerical solution technique:
  %
  \vspace{0.5cm}
  \begin{enumerate}
  \item<+-> Read the mesh from file
  \item<+-> Initialize data structures
  \item<+-> Construct a discrete representation of the governing equations
  \item<+-> Solve the discrete system
  \item<+-> Write out results
  \item<+-> Optionally estimate error, refine the mesh, and repeat
  \end{enumerate}
  
  \vspace{1cm}
  \uncover<+->{libMesh is a \alert<+->{C++ library} that offers functionality to handle the tasks above, with the
  exception of step 3.}

\end{frame}

% ----------------------------------------------------------------------------
% -                         Installing Libmesh
% ----------------------------------------------------------------------------
\subsection{Installing libMesh}

% ----------------------------------------------------------------------------
\begin{frame}{Resources}

  \begin{block}{}

    \begin{itemize}
             
    \item \textbf{libMesh website:} \\
      {https://libmesh.github.io}
      
     \item \textbf{Stable Releases:} \\
       {https://github.com/libMesh/libmesh/releases}
      
     \item \textbf{Development tree:} \\
       {\$ git clone git://github.com/libMesh/libmesh.git}
      
    \end{itemize}

  \end{block}

\end{frame}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstI{
  \begin{lstlisting}[language=bash]
# libMesh important directories
export LIBMESH_SRC="/path/to/libmesh/source/dir"
export LIBMESH_DIR="/path/to/libmesh/install/dir"

# Flavour to be used in runtime:
export METHOD="opt"

# If you wish to compile with PETSc
# MPI path is taken from PETSc configuration
export PETSC_ARCH="petsc-architecture"
export PETSC_DIR="/path/to/petsc"
  \end{lstlisting}
}

\begin{frame}{Environment Variables}

  \begin{block}{}
    \lstI
  \end{block}

\end{frame}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstII{
  \begin{lstlisting}[language=bash]
# Read through configure help first
./configure --help

# Configure libMesh
./configure METHODS="dbg opt gprof" \
--disable-fortran \
--with-metis=PETSc \
--disable-strict-lgpl \
--prefix=/home/hooshi/code/libmesh/mpich-petsc \
--enable-laspack \
--enable-unique-ptr 

# Make and install
make -j 8 
make install
  \end{lstlisting}
}

\begin{frame}{Configure and Make}

  \begin{block}{}
    \lstII
  \end{block}

\end{frame}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstIII{
  \begin{lstlisting}[language=bash]
    `libmesh-config --cxx` \
    -o foo foo.C \
    `libmesh-config --cxxflags --include --ldflags`
  \end{lstlisting}
}

\begin{frame}{Linking With libMesh}

  For small applications use the `libmesh-config' executable found in
  the install directory. 
%
  \begin{block}{}
    \lstIII
  \end{block}
%
  \vspace{1cm}
  For larger applications include Make.common in your
  Makefile. See Makefiles in the example folders.

\end{frame}

% ****************************************************************************
% *                            Application
% ****************************************************************************
\section{Application}

% ----------------------------------------------------------------------------
% -                        A Sample Problem   
% ----------------------------------------------------------------------------
\subsection{A Sample Problem}

% ----------------------------------------------------------------------------
\begin{frame}{Poisson Problem}

  \begin{itemize}
  \item The equation: \\
    $\Delta u = f \qquad u \in \Omega$

    \vspace{0.5cm}
  \item Penalty method for Dirichlet boundary condition:
    \\ $\frac{1}{\epsilon} u(x) + u_n(x) = \frac{1}{\epsilon} u_0(x)
    \qquad x \in \partial \Omega$

    \vspace{0.5cm}
  \item A mesh with appropriate boundary tags should be available in one
    of the many supported formats, e.g., gmsh, Triangle and Tetgen.

  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------
\begin{frame}{Supported Mesh Elements}

  \begin{block}{Effect}
    \begin{itemize}
    \item The mapping from the reference element to the computational element.
    \item Limits on the available finite element basis functions.
    \end{itemize}
  \end{block}

  \begin{itemize}
  \item 2, 3, and 4 noded edges (Edge2, Edge3, Edge4)
    
  \item 3 and 6 noded triangles (Tri3, Tri6)
  \item 4, 8, and 9 noded quadrilaterals (Quad4, Quad8, Quad9)

  \item 4 and 10 noded tetrahedrals (Tet4, Tet10)
  \item 8, 20, and 27 noded hexahedrals (Hex8, Hex20, Hex27)
  \item 6, 15, and 18 noded prisms (Prism6, Prism15, Prism18)
  \item 5, 13, and 14 noded pyramids (Pyramid5, Pyramid13, Pyramid14)
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------
% -                         The  Main Function
% ----------------------------------------------------------------------------
\subsection{The Code}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstMAIN{
  \begin{lstlisting}[language=C++]
//Assembe Function to be introduced later
void assemble_poisson(EquationSystems& es, const string&);

int main (int argc, char** argv)
{
  // Initialize libraries.
  LibMeshInit init (argc, argv);

  // Create a mesh
  Mesh mesh(init.comm());

  // Read the mesh
  GmshIO gmsh(mesh);
  gmsh.read("../mesh/simple_box_5.msh");
  mesh.all_second_order(true);
  mesh.prepare_for_use();
 
 // Create an equation systems object.
  EquationSystems equation_systems (mesh);
  \end{lstlisting}
}

\begin{frame}{The main function}
  \lstMAIN
\end{frame}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstMAINII{
  \begin{lstlisting}[language=C++]
  // Add a system to be solved
  equation_systems.
  add_system<LinearImplicitSystem> ("Poisson");

  // Add a variable
  equation_systems.get_system("Poisson")
  .add_variable("u", SECOND, HIERARCHIC);

  // Attach the assembler function
  equation_systems.get_system("Poisson")
  .attach_assemble_function (assemble_poisson);
  \end{lstlisting}
}

\begin{frame}{The main function}
  \lstMAINII
\end{frame}

% ----------------------------------------------------------------------------
\defverbatim[colored]\lstMAINIII{
  \begin{lstlisting}[language=C++]
  // Initialize the data structures.
  equation_systems.init();

  // Solve the system
  equation_systems.get_system("Poisson").solve();

  // write the results
  VTKIO (mesh).write_equation_systems
  ("out.pvtu", equation_systems);

  return 0;
}
  \end{lstlisting}
}

\begin{frame}{The main function}
  \lstMAINIII
\end{frame}

% ----------------------------------------------------------------------------
% -                                 Systems
% ----------------------------------------------------------------------------
\begin{frame}{Systems}
\begin{block}{Some useful ones}
  \begin{itemize}
    \item ExplicitSystem
    \item LinearImplicitSystem
    \item TransientLinearImplicitSystem
    \item DifferentiableSystem
    \item FEMSystem
  \end{itemize}
\end{block}
\end{frame}


% ----------------------------------------------------------------------------
% -                             Basis Functions
% ----------------------------------------------------------------------------
\begin{frame}{Finite Element Families}
 
  \begin{itemize}
  \item Lagrange (Up to quadratic)
  \item High Order C0
    \begin{itemize}
    \item Hierarchic
    \item Bernstein
    \item Szabo-Babuska
    \end{itemize}
  \item C1 elements 
    \begin{itemize}
    \item Hermite
    \item Clough-Tocher
    \end{itemize}
  \item Discontinuous elements 
    \begin{itemize}
    \item Monomials
    \item L2-Lagrange
    \item L2-Hierarchic
    \end{itemize}
  \item Vector-valued elements 
    \begin{itemize}
    \item Lagrange-Vec
    \item Nedelec first type
    \item No Raviart-Thomas as of now
    \end{itemize}
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------
% -                           Assemble Function
% ----------------------------------------------------------------------------
\begin{frame}{Assemble Function}

  \begin{block}{}
    Nomenclature for the $i$th degree of freedom and $q$th quadrature
    point (volume or surface).
  \end{block}

  \vspace{1cm}
  \includegraphics[width=\textwidth]{names.png}
\end{frame}

% ----------------------------------------------------------------------------
\begin{frame}{Assembling Local LHS and RHS}
\large
{\tt 
\alert<.(2)>{for(q=0; q<Nq; ++q)} \\
\hspace{0.25cm}    for (i=0; i<Ns; ++i) \\
\hspace{0.25cm}    \{ \\
\hspace{0.5cm}        Fe(i) += \alert<.(3)>{JxW[q]} * \alert<.(4)>{f(xyz[q])}*\alert<.(5)>{phi[i][q]}; \\
\hspace{0.5cm} \\
\hspace{0.5cm}    for (j=0; j<Ns; ++j) \\
\hspace{0.75cm}     Ke(i,j) += \alert<.(6)>{JxW[q]}*\alert<.(7)>{(dphi[j][q]*dphi[i][q])}; \\
\hspace{0.25cm}     \} 
}
                 
\vspace{1.2cm}

$F_i^e =$ 
\alert<.(2)>{$ \sum_{q=1}^{N_q}$} 
\alert<.(4)>{$f(x(\xi_q))$}
\alert<.(5)>{$\phi_i(\xi_q)$}
\alert<.(3)>{$| J(\xi_q)| w_q$}

\vspace{0.5cm} 

$ K_{ij}^e =$
\alert<.(2)>{$ \sum_{q=1}^{N_q}$}
\alert<.(7)>{$\nabla \phi_j(\xi_q) \cdot \nabla \phi_i(\xi_q)$}
\alert<.(6)>{$| J(\xi_q)| w_q $}

\end{frame}

% ****************************************************************************
% *                            Going Further
% ****************************************************************************
\section{Further Functionality}

\begin{frame}{Further Functionality}

\begin{itemize}
  \item Runtime selection of solver package: PETSc, Trillinos, Laspack
  \item Code can be parallelized with minor changes through threading or MPI
  \item Various error estimation algorithms
  \item Mesh adaptation and repartitioning in parallel
  \item Discontinuous Galerkin Methods

\end{itemize}

\end{frame}

% ----------------------------------------------------------------------------
% -                           References
% ----------------------------------------------------------------------------
\appendix
\section<presentation>*{\appendixname}
\subsection<presentation>*{References}

\begin{frame}{References}
    
  \begin{thebibliography}{10}
    
  

  \bibitem{libmesh1}
    Roy Stogner, Derek Gaston
    \newblock {\em libMesh Finite Element Library}.
    \newblock \url{http://users.ices.utexas.edu/~roystgnr/libmeshpdfs/roystgnr/sandia_libmesh.pdf}

  \bibitem{libmesh2}
    Benjamin S.~Kirk, John W.~Peterson, Roy H.~Stogner
    \newblock {\em The libMesh Finite Element Library}.
    \newblock \url{http://www.training.prace-ri.eu/uploads/tx_pracetmo/libmesh.pdf}

  \end{thebibliography}
\end{frame}


\end{document}
