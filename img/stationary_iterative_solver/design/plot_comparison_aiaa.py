#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
import matplotlib

if __name__ == "__main__":

    matplotlib.rcParams['mathtext.fontset'] = 'stix'
    
    # ADD the subplots
    plt.close('all')
    fig, axs = plt.subplots(1, 2, sharey=True)
    ax = fig.add_subplot(111)
    axs[0].set_zorder(100)
    axs[1].set_zorder(100)
    ax.set_zorder(0)
    fig.set_figwidth(17)
    plt.subplots_adjust(wspace=.1, hspace=0)


    colors = ['b', 'g', 'r', 'c', 'k', 'm', 'y']
    symbols = ['o', '^', 'v', '<', '>', 'p', 'h']
    symsize = [9, 10, 10, 8, 10, 10, 10]
    symevery = [5, 5, 5, 4, 5, 5, 5]
    labels = ['case A', 'case B', 'case C', 'case D', 'case E']

    jobs_1 = []
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_full_qmd.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_onlyone_rcm.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_onlyone_lines.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_ksp_rcm.hst"))
    jobs_1.append(hst.ConvergenceHistory("hst/a4_medium_ksp_lines.hst"))

    jobs_2 = []
    jobs_2.append(hst.ConvergenceHistory("hst/a4_fine_full_qmd.hst"))
    jobs_2.append(hst.ConvergenceHistory("hst/a4_fine_onlyone_rcm.hst"))
    jobs_2.append(hst.ConvergenceHistory("hst/a4_fine_onlyone_lines.hst"))
    jobs_2.append(hst.ConvergenceHistory("hst/a4_fine_ksp_rcm.hst"))
    jobs_2.append(hst.ConvergenceHistory("hst/a4_fine_ksp_lines.hst"))

    jobs = [jobs_1, jobs_2]
    jtags = ["medium", "fine"]
    lines = []
    
    for idx, (jgroup, jtag) in enumerate(zip(jobs, jtags)):
        for  j, c, l, s, ssize, se in zip(jgroup, colors, labels, symbols, symsize, symevery):
            j.init_axis()

            np.square(j.yaxis_res_nl[1])
            np.square(j.yaxis_res_nl[2])
            np.square(j.yaxis_res_nl[3])
            np.square(j.yaxis_res_nl[4])
            np.square(j.yaxis_res_nl[5])
            j.yaxis_res_nl[0] = j.yaxis_res_nl[1] + j.yaxis_res_nl[2] + \
                j.yaxis_res_nl[3] + j.yaxis_res_nl[4] + j.yaxis_res_nl[5]
            np.sqrt(j.yaxis_res_nl[0])

            ln, = axs[idx].semilogy(j.yaxis_cpu, j.yaxis_res_nl[0], c + s + '-', markevery=se, markersize=ssize) 
            lines.append(ln)
            

    #-------------------  LEGEND
    lgnd=axs[1].legend((lines[0],lines[1],lines[2], lines[3], lines[4]),
                       ('Case A','Case B', 'Case C', 'Case D', 'Case E'),
                       ncol=1, bbox_to_anchor=(1.1,1.), loc="upper right")
    lgnd.set_zorder(500)

    #------------------- OUTER AXIS
    ax.set_ylabel(r'$\| \mathbf{R}(\mathbf{U}_h) \|_2$', fontsize=22)
    ax.set_xlabel(r'Wall time (s)', fontsize=22)
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelsize=30, labelcolor='white', top='off', bottom='off', left='off', right='off')

    #------------------- Small AXIS
    axs[0].tick_params(labelsize=22)
    axs[1].tick_params(labelsize=22)

    axs[0].locator_params(axis='y', numticks=6)
    axs[0].locator_params(axis='x', nbins=5)
    axs[1].locator_params(axis='x', nbins=5)

    
    #------------------- TExt
    # axs[0].text(350, 600, r"$N_{CV}=25K$", fontsize=22)
    # axs[1].text(2500, 600, r"$N_{CV}=100K$", fontsize=22)
    axs[0].text(300, 600, "25K control volumes", fontsize=15)
    axs[1].text(2400, 600, "100K control volumes", fontsize=15)


    fig.savefig('plots/res_comparison_aiaa.eps', bbox_inches='tight')
    plt.show()
