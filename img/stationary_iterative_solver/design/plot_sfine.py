#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 

#plt.rcParams['text.usetex'] = True

def myticks(x,pos):

    if x == 0: return "0"

    exponent = int(np.log10(x))
    coeff = x/10**exponent

    return "$\mathregular{%2.0f \\times 10^{%2d}}$" % (coeff,exponent)


if __name__ == "__main__":

    colors = ['b', 'g', 'r', 'm', 'k', 'c', 'y']
    symbols = ['o', '^', 'v', '<', '>', 'p', 'h']
    symsize = [9, 10, 10, 8, 10, 10, 10]
    symevery = [5, 5, 5, 4, 5, 5, 5]
    labels = ['case A', 'case B', 'case C', 'case D', 'case E']

    j = hst.ConvergenceHistory("hst/a4_sfine_ksp_lines.hst")

    plt.figure(1)
    plt.clf()

    j.init_axis()

    plt.figure(1)

    np.square(j.yaxis_res_nl[1])
    np.square(j.yaxis_res_nl[2])
    np.square(j.yaxis_res_nl[3])
    np.square(j.yaxis_res_nl[4])
    np.square(j.yaxis_res_nl[5])
    j.yaxis_res_nl[0] = j.yaxis_res_nl[1] + j.yaxis_res_nl[2] + \
        j.yaxis_res_nl[
        3] + j.yaxis_res_nl[4] + j.yaxis_res_nl[5]
    np.sqrt(j.yaxis_res_nl[0])

    plt.semilogy(j.yaxis_cpu, j.yaxis_res_nl[0], 'k>-',
                 label='case E$^*$', markevery=1, markersize=8, linewidth=2)

    # -------
    plt.legend(loc='upper right', fontsize=20)
    plt.xlabel('Wall time (s)', fontsize=24)
    plt.ylabel(r'$\| \mathbf{R}(\mathbf{U}_h) \|_2$', fontsize=24)
    plt.tick_params(labelsize=24)

    plt.locator_params(axis='x', nbins=3)
    plt.locator_params(axis='y', numticks=5)
    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(myticks))
    
    plt.savefig('plots/res_sfine.eps', bbox_inches='tight')

    plt.show()
