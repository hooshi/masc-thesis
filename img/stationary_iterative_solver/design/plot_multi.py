#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
fig.set_figwidth(12)

for hsize in [1]:
    
    # ---------------------------------------------------
    # Read Info
    # --------------------------------------------------

    jobsC= []
    jobsC.append(hst.ConvergenceHistory("hst_multi/D%d_p2_lines.hst" % (hsize+1) ))
    jobsC.append(hst.ConvergenceHistory("hst_multi/D%d_p3_lines.hst" % (hsize+1) ))
    jobsC.append(hst.ConvergenceHistory("hst_multi/D%d_p4_lines.hst" % (hsize+1) ))

    jobsD= []
    jobsD.append(hst.ConvergenceHistory("hst_multi/D%d_p2_qmd.hst" % (hsize+1) ))
    jobsD.append(hst.ConvergenceHistory("hst_multi/D%d_p3_qmd.hst" % (hsize+1) ))
    jobsD.append(hst.ConvergenceHistory("hst_multi/D%d_p4_qmd.hst" % (hsize+1) ))

  
    for ij in enumerate(jobsC):
        i = ij[0]
        j = ij[1]
        j.init_axis()
        if(i>0):  j.yaxis_cpu += jobsC[i-1].yaxis_cpu[-1]

    for ij in enumerate(jobsD):
        i = ij[0]
        j = ij[1]
        j.init_axis()
        if(i>0):  j.yaxis_cpu += jobsD[i-1].yaxis_cpu[-1]

    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------

    xvals = np.concatenate([jobsC[0].yaxis_cpu,jobsC[1].yaxis_cpu, jobsC[2].yaxis_cpu])
    yvals = np.concatenate([jobsC[0].yaxis_res_nl[0],jobsC[1].yaxis_res_nl[0], jobsC[2].yaxis_res_nl[0]])

    ax.semilogy (xvals, yvals,
                 'k', linewidth=2,
                 markersize=10, markevery=11)

    # xvals = np.concatenate([jobsD[0].yaxis_cpu,jobsD[1].yaxis_cpu, jobsD[2].yaxis_cpu])
    # yvals = np.concatenate([jobsD[0].yaxis_res_nl[0],jobsD[1].yaxis_res_nl[0], jobsD[2].yaxis_res_nl[0]])

    # ax.semilogy (xvals, yvals,
    #              'k', linewidth=2,
    #              markersize=10, markevery=11)

    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    ax.text(500,  10**4, '$1$-exact', fontsize=20)
    ax.text(1300, 10**4, '$2$-exact', fontsize=20)
    ax.text(1900, 10**4, '$3$-exact', fontsize=20)
    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    plt.xlabel('Wall Time (s)', fontsize=20)
    plt.ylabel(r'$\| \mathbf{R}(\mathbf{U}_h) \|_2$', fontsize=20)
    plt.tick_params(labelsize=20)

    # ax.tick_params(axis='x',which='minor',labelsize=20)
    plt.locator_params(axis='y', numticks=8)

plt.savefig('plots/multi_conv.eps', bbox_inches='tight')        
plt.show()
