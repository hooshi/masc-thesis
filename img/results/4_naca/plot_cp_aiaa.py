#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def read_csv_data(fname):
    array=np.loadtxt(fname, dtype='float', delimiter=',')
    return array[:,6], array[:,3], array[:,2] 

def read_fun3d_cp(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[:,0], array[:,2]


fig = plt.figure(figsize=(8,4))
axtot= fig.add_subplot(1, 2, 1)
ax1= fig.add_subplot(2, 4, 3)
ax2= fig.add_subplot(2, 4, 4)
ax3= fig.add_subplot(2, 4, 7)
ax4= fig.add_subplot(2, 4, 8)
plt.setp(ax1, xticks=[], yticks=[])
plt.setp(ax2, xticks=[], yticks=[])
plt.setp(ax3, xticks=[], yticks=[])
plt.setp(ax4, xticks=[], yticks=[])


fig.set_figheight(7)
fig.subplots_adjust(wspace=0, hspace=0)

## ----------------------------------------
## Plot all values
## ----------------------------------------

#
x, cp, cf =  read_csv_data('hst/hex_cpcv.csv')
ln1, = axtot.plot(x,cp, color='b', linestyle='--', marker='o')
ax1.plot(x,cp, color='b', linestyle='--', marker='o')
ax2.plot(x,cp, color='b', linestyle='--', marker='o')
ax3.plot(x,cp, color='b', linestyle='--', marker='o')
ax4.plot(x,cp, color='b', linestyle='--', marker='o', markevery=4)

#
x, cp, cf =  read_csv_data('hst/mixed_cpcv.csv')
ln2, = axtot.plot(x,cp, color='r', linestyle='--', marker='^')
ax1.plot(x,cp, color='r', linestyle='--', marker='^')
ax2.plot(x,cp, color='r', linestyle='--', marker='^')
ax3.plot(x,cp, color='r', linestyle='--', marker='^')
ax4.plot(x,cp, color='r', linestyle='--', marker='^', markevery=4)

#
x, cp, cf =  read_csv_data('hst/hex_cpcv_low.csv')
ln3, = axtot.plot(x,cp, color='g', linestyle=':', marker='v')
ax1.plot(x,cp, color='g', linestyle=':', marker='v')
ax2.plot(x,cp, color='g', linestyle=':', marker='v')
ax3.plot(x,cp, color='g', linestyle=':', marker='v')
ax4.plot(x,cp, color='g', linestyle=':', marker='v', markevery=4)

#
x, cp, cf =  read_csv_data('hst/mixed_cpcv_low.csv')
ln4, = axtot.plot(x,cp, color='c', linestyle=':', marker='>')
ax1.plot(x,cp, color='c', linestyle=':', marker='>')
ax2.plot(x,cp, color='c', linestyle=':', marker='>')
ax3.plot(x,cp, color='c', linestyle=':', marker='>')
ax4.plot(x,cp, color='c', linestyle=':', marker='>', markevery=4)

#
x, cp =  read_fun3d_cp('hst/fun3d_cp_sa_nopv.dat')
ln5, =  axtot.plot(x,cp, color='k', linestyle='-', linewidth=1)
ax1.plot(x,cp, color='k', linestyle='-', linewidth=1)
ax2.plot(x,cp, color='k', linestyle='-', linewidth=1)
ax3.plot(x,cp, color='k', linestyle='-', linewidth=1)
ax4.plot(x,cp, color='k', linestyle='-', linewidth=1, markevery=4)

## ----------------------------------------
## Set the limit of small axis
## ----------------------------------------

ax1.set_xlim([0, 0.015])
ax1.set_ylim([-5.7, -5])

ax2.set_xlim([0.1, 0.2])
ax2.set_ylim([-1.9, -1.2])

ax3.set_xlim([0.010, 0.030])
ax3.set_ylim([0.7, 1.08])

ax4.set_xlim([0.95, 1])
ax4.set_ylim([0.04, 0.2])

## ----------------------------------------
## Add matplotlib boxes
## ----------------------------------------

i = 2
ax = ax2

ylim = ax.get_ylim()
xlim = ax.get_xlim()

## Draw rectangle
rect = patches.Rectangle(
    ( xlim[0], ylim[0]*1.1),   
    2*(xlim[1]-xlim[0]),          
    2*(ylim[1]-ylim[0]),
    fill=False,
    edgecolor='k',
    linewidth=1
)
rect.set_zorder(100)
axtot.add_patch(rect)

## ----------------------------------------
## Set the text in the big axis
## ----------------------------------------
font_size=11

axtot.text(0.035, -5.3, '(a)', fontsize=font_size)
axtot.text(0.31, -1.35, '(b)', fontsize=font_size)
axtot.text(0.085, 1.25, '(c)', fontsize=font_size)
axtot.text(0.96,  -0.40, '(d)', fontsize=font_size)

## ----------------------------------------
## Set the text in the small axis
## ----------------------------------------

ax1.text(0.5, 0.8, '(a)',transform=ax1.transAxes,fontsize=font_size)
ax2.text(0.5, 0.8, '(b)',transform=ax2.transAxes,fontsize=font_size)
ax3.text(0.5, 0.8, '(c)',transform=ax3.transAxes,fontsize=font_size)
ax4.text(0.5, 0.8, '(d)',transform=ax4.transAxes,fontsize=font_size)

## ----------------------------------------
## Invert the Y axis
## ----------------------------------------

ax1.invert_yaxis()
ax2.invert_yaxis()
ax3.invert_yaxis()
ax4.invert_yaxis()
axtot.invert_yaxis()


## ----------------------------------------
## Set the legend and stuff
## ----------------------------------------
axtot.set_xlabel(r'x', fontsize=font_size)
axtot.set_ylabel(r'$C_p$', fontsize=font_size)

lines = [ ln5, ln1, ln2, ln3, ln4]
titles = ['FUN3D', '$k=3$, hex', '$k=3$, mixed', '$k=1$, hex', '$k=1$, mixed']
axtot.legend(lines,titles,fontsize=10)

plt.show()
fig.savefig('plots/cp_aiaa.eps', bbox_inches='tight')
