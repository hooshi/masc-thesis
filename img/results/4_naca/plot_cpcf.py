#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse


def read_csv_data(fname):
    array=np.loadtxt(fname, dtype='float', delimiter=',')
    return array[:,6], array[:,3], array[:,2] 

def read_fun3d_cp(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[:,0], array[:,2]


x, cp, cf =  read_csv_data('hst/hex_cpcv.csv')
plt.plot(x,cp, color='b', linestyle='--', marker='o')

x, cp, cf =  read_csv_data('hst/mixed_cpcv.csv')
plt.plot(x,cp, color='r', linestyle='--', marker='^')

x, cp, cf =  read_csv_data('hst/hex_cpcv_low.csv')
plt.plot(x,cp, color='g', linestyle=':', marker='v')

x, cp, cf =  read_csv_data('hst/mixed_cpcv_low.csv')
plt.plot(x,cp, color='c', linestyle=':', marker='>')

x, cp =  read_fun3d_cp('hst/fun3d_cp_sa_nopv.dat')
plt.plot(x,cp, color='k', linestyle='-', linewidth=1)

plt.gca().invert_yaxis()
plt.show()
