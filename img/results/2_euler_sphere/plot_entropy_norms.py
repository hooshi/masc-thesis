#!/usr/bin/python

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 

def plot_versus_mesh_size():

    # ---------------------------------------------------
    # MESH SIZES
    # ---------------------------------------------------
    #sizes = [8.,14.,25.,40.];
    #sizes = [np.cbrt(15360), np.cbrt(63540), np.cbrt(322500), np.cbrt(1017600)];
    #hh = np.array([sizes[0]/sizes[0], sizes[1]/sizes[0], sizes[2]/sizes[0], sizes[3]/sizes[0]])

    sizes = [14.,25.,40.];
    #sizes = [np.cbrt(63540), np.cbrt(322500), np.cbrt(1017600)];
    hh = np.array([sizes[0]/sizes[0], sizes[1]/sizes[0], sizes[2]/sizes[0]])
    

    # ---------------------------------------------------
    # Entropy norms
    # ---------------------------------------------------
    S = []
    # a1
    S.append(np.array([2.405681e-02, 1.994755e-02, 1.592839e-02]))
    #S.append(np.array([2.144235e-02, 2.405681e-02, 1.994755e-02, 1.592839e-02]))
    #S.append(np.array([2.144235e-02, 2.405681e-02, 1.994755e-02, 1.592839e-02]))
    
    # a2, curve3, curve, facet
    S.append(np.array([4.636350e-04, 1.281985e-04, 4.683948e-05]))
    # S.append(np.array([2.769819e-03, 4.636350e-04, 1.281985e-04, 4.683948e-05]))a
    # S.append(np.array([2.525616e-03, 4.400782e-04, 1.191065e-04, 4.320160e-05]))

    # a1
    S.append(np.array([6.199170e-05, 1.294311e-05, 4.929109e-06]))
    #S.append(np.array([1.467434e-03, 6.199170e-05, 1.294311e-05, 4.929109e-06]))
    # S.append(np.array([1.532920e-03 , 6.933847e-05, 1.294312e-05, 4.801597e-06]))

    # a1
    S.append(np.array([1.633790e-05, 1.086930e-06, 1.091083e-07]))
    #S.append(np.array([3.421774e-04, 1.633790e-05, 1.086930e-06, 1.091083e-07]))
    # S.append(np.array([4.014887e-04, 1.576764e-05, 1.245873e-06, 1.727088e-07]))

    ppS = []
    bid=0
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[0][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[1][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[2][bid:]), 1) )
    ppS.append( np.polyfit(np.log(hh[bid:]), np.log(S[3][bid:]), 1) )
    print ppS[0]
    print ppS[1]
    print ppS[2]
    print ppS[3]
    
    # ---------------------------------------------------
    # Drag norms
    # ---------------------------------------------------
    # D = []
    # D.append(np.fabs(np.array([4.379070e+00, 3.567711e+00, 2.545257e+00, 1.926052e+00])     ))
    # D.append(np.fabs(np.array([6.335719e-01, 1.632818e-01, 6.214897e-02, 2.922296e-02])     ))
    # D.append(np.fabs(np.array([2.942209e-01, -5.167872e-02, -3.727831e-02, -2.108941e-02])  ))
    # D.append(np.fabs(np.array([8.167000e-02, -4.706528e-02, -8.323122e-03, -1.886976e-03])  ))

    # ppD = []
    # ppD.append( np.polyfit(np.log(hh[1:-1]), np.log(D[0][1:-1]), 1) )
    # ppD.append( np.polyfit(np.log(hh[1:-1]), np.log(D[1][1:-1]), 1) )
    # ppD.append( np.polyfit(np.log(hh[1:-1]), np.log(D[2][1:-1]), 1) )
    # ppD.append( np.polyfit(np.log(hh[1:-1]), np.log(D[3][1:-1]), 1) )
    

    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # ln1, = ax.loglog(hh, S[0], 'c->', linewidth=2, markersize=10)
    ln2, = ax.loglog(hh, S[1], 'b-o', linewidth=2, markersize=10)
    ln3, = ax.loglog(hh, S[2], 'r-^', linewidth=2, markersize=10)
    ln4, = ax.loglog(hh, S[3], 'g-v', linewidth=2, markersize=10)

    # ---------------------------------------------------
    # Annotations on plots themselves
    # ---------------------------------------------------

    xend = hh[-1]
    yend = S[1][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[1][0], xy=(xend, yend),
                xytext=(xend+0.4, yend*2),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=17 )

    xend = hh[-1]
    yend = S[2][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[2][0], xy=(xend, yend),
                xytext=(xend+0.4, yend*2),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=17 )

    xend = hh[-1]
    yend = S[3][-1]
    ax.annotate('ave. slope=%.1f' % -ppS[3][0], xy=(xend, yend),
                xytext=(xend+0.4, yend*2),
                arrowprops=dict(arrowstyle="->", connectionstyle="arc3"),
                fontsize=17 )
    
    # ---------------------------------------------------
    # Plot annotations
    # ---------------------------------------------------
    # xend = 3
    # yl = []
    # yl.append('')
    # yl.append( [S[1][0], S[1][0]/pow(xend,2)] );
    # yl.append( [S[2][0], S[2][0]/pow(xend,3)] );
    # yl.append( [S[3][0], S[3][0]/pow(xend,4)] );
    # ax.loglog([1, xend], yl[1], color='k', linestyle='--', linewidth=0.75)
    # ax.loglog([1, xend], yl[2], color='k', linestyle='--', linewidth=0.75)
    # ax.loglog([1, xend], yl[3], color='k', linestyle='--', linewidth=0.75)

    # ax.annotate('slope=2', xy=(xend, yl[1][1]), xytext=(xend+0.3, yl[1][1]),
    #             arrowprops=dict(arrowstyle="->", connectionstyle="arc3"), fontsize=17 )
    # ax.annotate('slope=3', xy=(xend, yl[2][1]), xytext=(xend+0.3, yl[2][1]),
    #             arrowprops=dict(arrowstyle="->", connectionstyle="arc3"), fontsize=17 )
    # ax.annotate('slope=4', xy=(xend, yl[3][1]), xytext=(xend+0.3, yl[3][1]),
    #             arrowprops=dict(arrowstyle="->", connectionstyle="arc3"), fontsize=17 )


    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    fig.legend([ln2, ln3, ln4],
               ['$k=1$','$k=2$','$k=3$'],
               loc='upper right', fontsize=20)
    
    plt.xlabel('$h_0/h$', fontsize=20)
    plt.ylabel(r'$\| S_h-S_\infty \|_2$', fontsize=20)
    plt.tick_params(labelsize=20)

    ax.tick_params(axis='x',which='minor',labelsize=20)
    ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.0f"))
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.0f"))

    #for label in ax.xaxis.get_ticklabels(1)[1::2]:
    #    label.set_visible(False)

    ax.set_xlim([0, 6])

    # plt.locator_params(axis='x', numticks=4)
    #plt.locator_params(axis='y', numticks=5)
    plt.savefig('plots/entropy.eps', bbox_inches='tight')



def plot_versus_cpu_time():

    # ---------------------------------------------------
    # MESH SIZES
    # ---------------------------------------------------
    sizes = [14.,25.,40.];
    hh = np.array([sizes[0]/sizes[0], sizes[1]/sizes[0], sizes[2]/sizes[0]])
    

    # ---------------------------------------------------
    # Times
    # ---------------------------------------------------
    cpu = []
    cpu.append( [3.4, 20, 114, 427] )
    cpu.append( [21, 107, 579, 1969] )
    cpu.append( [24, 112, 620, 2150] )
    cpu.append( [67, 320, 1879, 5904] )
    
    # ---------------------------------------------------
    # Entropy norms
    # ---------------------------------------------------
    S = []
    S.append( [0.0214424, 2.405681e-02, 1.994755e-02, 1.592839e-02] )    
    S.append( [0.00276982, 4.636350e-04, 1.281985e-04, 4.683948e-05] )
    S.append( [0.00146743, 6.199170e-05, 1.294311e-05, 4.929109e-06] )
    S.append( [0.000342177, 1.633790e-05, 1.086930e-06, 1.091083e-07] )
      

    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(111)

    common_options = dict(linewidth=2, markersize=10)
    ln1, = ax.loglog(cpu[0][1:], S[0][1:], 'c-o', **common_options)
    ln2, = ax.loglog(cpu[1][1:], S[1][1:], 'b-s', **common_options)
    ln3, = ax.loglog(cpu[2][1:], S[2][1:], 'r-^', **common_options)
    ln4, = ax.loglog(cpu[3][1:], S[3][1:], 'g-v', **common_options)

    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    ax.legend([ln1, ln2, ln3, ln4],
               [r'$O(h^1)$', r'$O(h^2)$',r'$O(h^3)$',r'$O(h^4)$'],
               loc='upper right', fontsize=20)
    
    ax.set_xlabel('CPU Time', fontsize=25)
    ax.set_ylabel('Entropy Error', fontsize=25)
    ax.tick_params(labelsize=25)

    ax.tick_params(axis='x',which='minor',labelsize=20)
    plt.savefig('plots/entropy_cpu.pdf', bbox_inches='tight')


if __name__ == "__main__":
    plot_versus_mesh_size()
    plot_versus_cpu_time()
    plt.show()

