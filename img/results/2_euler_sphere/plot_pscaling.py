#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 


if __name__ == "__main__":

    # ---------------------------------------------------
    # Number of processors
    # ---------------------------------------------------
    nproc = np.array( range(1,11) )


    # ---------------------------------------------------
    # Time consumptions
    # ---------------------------------------------------
    totid = 0
    jacid = 1
    kspid = 2
    time= np.array \
          ([ \
             (11952, 6663, 4572, 3498, 2929, 2387, 2047, 1806, 1630, 1467), \
             (10442, 5761, 3932, 2995, 2450, 1979, 1724, 1551, 1352, 1230), \
             (1214 , 743 , 530 , 424 , 410 , 355 , 275 , 212 , 238 , 204) \
          ])
    time = time.astype(float)

    speedup = time
    for jj in reversed(range(time.shape[1])):
        speedup[:,jj] = time[:,0] / time[:,jj]

    plt.plot([1, 10], [1, 10], color='k', linewidth=2, linestyle='--',
             label='Ideal')
    plt.plot(nproc, speedup[0,:], color='b', linewidth=2, marker='o',
             label='Total', markersize=10)
    plt.plot(nproc, speedup[1,:], color='r', linewidth=2, marker='^',
             label='Jac. int.', markersize=10)
    plt.plot(nproc, speedup[2,:], color='g', linewidth=2, marker='v',
             label= 'Lin. solve', markersize=10)

    plt.legend(loc='upper left', fontsize=20)
    plt.xlabel('# of processors', fontsize=20)
    plt.ylabel('Speed-up', fontsize=20)
    plt.tick_params(labelsize=20)


    plt.savefig('plots/scaling.eps', bbox_inches='tight')
    plt.show()
