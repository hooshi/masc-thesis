#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

def read_mut_data(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[:,0], array[:,2], array[:,1], array[:,3]


if __name__ == "__main__":

    fig1, ax1 = plt.subplots(1, 1, sharex=True)
    fig2, ax2 = plt.subplots(1, 1, sharex=True)
    fig3, ax3 = plt.subplots(1, 1, sharex=True)
    fig1.set_figheight(4)
    fig3.set_figheight(4)
    fig2.set_figheight(4)

    # ------------------------------------------
    #       Coarse MESH
    # ------------------------------------------

    nasa=np.loadtxt('hst/mut.txt', dtype='float')
    ln1, = ax1.plot(nasa[:,2], nasa[:,1], color='k',
                    linestyle=':', marker='.',  markerfacecolor='none',
                    markeredgecolor='k', markeredgewidth=2, zorder=0,
                    markevery=2)
    
    yb, ye, mutb, mute = read_mut_data('hst/h1_p2.serial.postproc')
    for i in range(len(yb)):
        ln2, = ax1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='b', linestyle='-', linewidth=2)
    yb, ye, mutb, mute = read_mut_data('hst/h1_p3.serial.postproc')
    for i in range(len(yb)):
        ln3, =  ax1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='r', linestyle='--', linewidth=2)
    yb, ye, mutb, mute = read_mut_data('hst/h1_p4.serial.postproc')
    for i in range(len(yb)):
        ln4, =  ax1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='g', linestyle='-.', linewidth=2)

    # ------------------------------------------
    #       Medium MESH
    # ------------------------------------------
    ax2.plot(nasa[:,2], nasa[:,1], color='k',
             linestyle=':', marker='.',  markerfacecolor='none',
             markeredgecolor='k', markeredgewidth=2, zorder=0,
             markevery=2)
    
    yb, ye, mutb, mute = read_mut_data('hst/h2_p2.serial.postproc')
    for i in range(len(yb)):  ax2.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='b', linestyle='-', linewidth=2)
    yb, ye, mutb, mute = read_mut_data('hst/h2_p3.serial.postproc')
    for i in range(len(yb)):  ax2.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='r', linestyle='--', linewidth=2)
    yb, ye, mutb, mute = read_mut_data('hst/h2_p4.serial.postproc')
    for i in range(len(yb)):  ax2.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='g', linestyle='-.', linewidth=2)


    # ------------------------------------------
    #       Fine MESH
    # ------------------------------------------

    #-------------------- Small axes

    axz1 = plt.axes([.4, .5, .2, .36])
    axz1.set_zorder(600)
    plt.setp(axz1, xticks=[], yticks=[])

    axz1.set_xlim([198, 210])
    axz1.set_ylim([0.0044, 0.0096])

    circle1 = Ellipse(xy=(202, 0.007), width=20, height=0.01, color='k', fill=False, zorder=0)
    ax3.add_artist(circle1)

    # ------------------- PLOT
    
    ax3.plot(nasa[:,2], nasa[:,1], color='k',
             linestyle=':', marker='.',  markerfacecolor='none',
             markeredgecolor='k', markeredgewidth=2, zorder=0,
             markevery=2)
    axz1.plot(nasa[:,2], nasa[:,1], color='k',
              linestyle=':', marker='.',  markerfacecolor='none',
              markeredgecolor='k', markeredgewidth=2, zorder=101, markevery=1)
    
    yb, ye, mutb, mute = read_mut_data('hst/h3_p2.serial.postproc')
    for i in range(len(yb)):
        ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='b', linestyle='-', linewidth=2, zorder=99)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='b', linestyle='-', linewidth=2, zorder=99)
        
    yb, ye, mutb, mute = read_mut_data('hst/h3_p3.serial.postproc')
    for i in range(len(yb)):
        ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='r', linestyle='--', linewidth=2, zorder=101)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='r', linestyle='--', linewidth=2, zorder=101)
        
    yb, ye, mutb, mute = read_mut_data('hst/h3_p4.serial.postproc')
    for i in range(len(yb)):
        ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='g', linestyle='-.', linewidth=2, zorder=102)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], color='g', linestyle='-.', linewidth=2, zorder=102)



    # ------------------------------------------
    #   Beautiful stuff
    # ------------------------------------------
    FSIZE=15
        
    ax1.set_ylim([0, 0.03])
    ax2.set_ylim([0, 0.03])
    ax3.set_ylim([0, 0.03])
    ax1.locator_params(axis='y', nbins=3)
    ax2.locator_params(axis='y', nbins=3)
    ax3.locator_params(axis='y', nbins=3)

    ax1.set_xlabel(r'$\mu_T/\mu$', fontsize=FSIZE+3)
    ax2.set_xlabel(r'$\mu_T/\mu$', fontsize=FSIZE+3)
    ax3.set_xlabel(r'$\mu_T/\mu$', fontsize=FSIZE+3)

    ax1.set_ylabel(r'$x_2$', fontsize=FSIZE+3)
    ax2.set_ylabel(r'$x_2$', fontsize=FSIZE+3)
    ax3.set_ylabel(r'$x_2$', fontsize=FSIZE+3)
    
    ax1.tick_params(labelsize=FSIZE)
    ax2.tick_params(labelsize=FSIZE)
    ax3.tick_params(labelsize=FSIZE)
    

    lgnd=ax1.legend((ln1,ln2,ln3,ln4),
               ('NASA TMR','$k=1$','$k=2$','$k=3$'),
                    ncol=1, bbox_to_anchor=(1.,1.), loc="upper right")
    lgnd.set_zorder(500)
    lgnd=ax2.legend((ln1,ln2,ln3,ln4),
                    ('NASA TMR','$k=1$','$k=2$','$k=3$'),
                    ncol=1, bbox_to_anchor=(1.,1.), loc="upper right")
    lgnd.set_zorder(500)
    lgnd=ax3.legend((ln1,ln2,ln3,ln4),
                   ('NASA TMR','$k=1$','$k=2$','$k=3$'),
                   ncol=1, bbox_to_anchor=(1.,1.), loc="upper right")
    lgnd.set_zorder(500)



    fig1.savefig('plots/mut_1.eps', bbox_inches='tight')
    fig2.savefig('plots/mut_2.eps', bbox_inches='tight')
    fig3.savefig('plots/mut_3.eps', bbox_inches='tight')
    plt.show()



