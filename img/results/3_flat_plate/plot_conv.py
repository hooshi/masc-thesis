#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
fig.set_figwidth(12)

for hsize in [0, 1, 2]:
    
    # ---------------------------------------------------
    # Read Info
    # --------------------------------------------------

    jobsC= []
    jobsC.append(hst.ConvergenceHistory("hst/h%d_p2_lines.hst" % (hsize+1) ))
    jobsC.append(hst.ConvergenceHistory("hst/h%d_p3_lines.hst" % (hsize+1) ))
    jobsC.append(hst.ConvergenceHistory("hst/h%d_p4_lines.hst" % (hsize+1) ))

  
    for ij in enumerate(jobsC):
        i = ij[0]
        j = ij[1]
        j.init_axis()
        if(i>0):  j.xaxis_nl += jobsC[i-1].xaxis_nl[-1]
        
    xvals = np.concatenate([jobsC[0].xaxis_nl,jobsC[1].xaxis_nl, jobsC[2].xaxis_nl])
    yvals = np.concatenate([jobsC[0].yaxis_res_nl[0],jobsC[1].yaxis_res_nl[0], jobsC[2].yaxis_res_nl[0]])

    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------

    if (hsize==0):
        ax.semilogy (xvals, yvals,
                     'b-o', linewidth=2,
                     label='$\mathregular{60 \\times 34 \\times 7}$',
                     markersize=10, markevery=11)
    elif (hsize==1):
        ax.semilogy (xvals, yvals,
                     'r-^', linewidth=2,
                     label='$\mathregular{120 \\times 68 \\times 14}$',
                     markersize=10, markevery=10)
    else:
        ax.semilogy (xvals, yvals,
                     'g-v', linewidth=2,
                     label="$\mathregular{240 \\times 136 \\times 28}$",
                     markersize=10, markevery=9)

    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    if (hsize==0):
        ax.text(10, 10**4, r'$k=1$', fontsize=20)
        ax.text(35, 10**4, r'$k=2$', fontsize=20)
        ax.text(65, 10**4, r'$k=3$', fontsize=20)
    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    plt.legend(loc='upper right', fontsize=20, bbox_to_anchor=(1.2,1.))
    plt.xlabel('PTC iteration', fontsize=20)
    plt.ylabel(r'$\| \mathbf{R}(\mathbf{U}_h) \|_2$', fontsize=20)
    plt.tick_params(labelsize=20)

    # ax.tick_params(axis='x',which='minor',labelsize=20)
    plt.locator_params(axis='y', numticks=8)

plt.savefig('plots/ptc.eps', bbox_inches='tight')        
plt.show()
