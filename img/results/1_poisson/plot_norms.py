#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker 
import matplotlib

def read_norms(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[1] 

matplotlib.rcParams['mathtext.fontset'] = 'stix'
fig, axs = plt.subplots(1, 3, sharey=True, figsize=(15,6))
# ---------------------------------------------
#  Initialize the Sets
# ---------------------------------------------

set_p = [2,3,4]
set_ele =  ['wedge', 'tet', 'hex', 'pyramid']
set_hcons = [1,2,3]
set_hname = [5,10,20,40,80]
lines=[[],[],[]]

for ele in set_ele:

    if (ele=='hex'):
        set_p = [2, 3, 4]
        set_mark = ['o', 'o','o']
        set_color = ['b','b','b']
        set_msize = [8,8,8]
        set_line = ['-','-','-']
        set_linew=[2,2,2]
    elif (ele=='wedge'):
        set_p = [2, 3, 4]
        set_mark = ['^', '^','^']
        set_color = ['r','r','r']
        set_msize = [13,13,8]
        set_line = ['-','-','-']
        set_linew= [2,2,2]
    elif (ele=='pyramid'):
        set_p = [2, 3, 4]
        set_mark = ['v', 'v','v']
        set_color = ['g','g','g']
        set_msize = [9,9,9]
        set_line = ['-','-','-']
        set_linew=[2,2,2]
    elif (ele=='tet'):
        set_p = [2, 3, 4]
        set_mark = ['>', '>', '>']
        set_color = ['c','c', 'c']
        set_msize = [9,9,14]
        set_line = ['-','-','-']
        set_linew= [2,2,2]

    for ip, p in enumerate(set_p):

        # ---------------------------------------------
        #  Read all the norms
        # ---------------------------------------------

        xvals = []
        yvals = []

        for h in set_hcons:
            comname = '%s_h%d_p%d' % (ele, h, p)
            fname = 'hst/%s.postproc' % comname
            xvals.append( set_hname[h] / set_hname[set_hcons[0]] )
            yvals.append( read_norms(fname) )

        # ---------------------------------------------
        #  Plot all the norms
        # ---------------------------------------------
        print xvals, yvals
        ln, = axs[ip].loglog(xvals, yvals,
                   marker=set_mark[ip],
                   markersize=set_msize[ip],
                   color= set_color[ip],
                   linewidth=set_linew[ip],
                   linestyle=set_line[ip])
        lines[ip].append(ln)

# ---------------------------------------------
#  Plot the reference line
# ---------------------------------------------
ln, = axs[0].loglog([1, xvals[-1]], [0.035, 0.035/xvals[-1]**2],
               color='k', linestyle='--', linewidth=1.5)
lines[0].append(ln)

axs[1].loglog([1, xvals[-1]], [0.01, 0.01/xvals[-1]**3],
               color='k', linestyle='--', linewidth=1.5)

axs[2].loglog([1, xvals[-1]], [0.01, 0.01/xvals[-1]**4],
               color='k', linestyle='--', linewidth=1.5)


# ---------------------------------------------
#  Text in plot
# ---------------------------------------------
fsize=24

axs[0].text(0.3, 0.1, r'$k=1$',transform=axs[0].transAxes, fontsize=fsize)
axs[1].text(0.3, 0.1, r'$k=2$',transform=axs[1].transAxes, fontsize=fsize)
axs[2].text(0.3, 0.1, r'$k=3$',transform=axs[2].transAxes, fontsize=fsize)

# ---------------------------------------------
#  Nice plot
# ---------------------------------------------

ax = plt.gca()
for ax in axs:
    ax.set_xlim([0, 4])
    ax.set_ylim([1e-6, 1e-1])
    ax.tick_params(axis='x',which='minor',labelsize=fsize-2)
    ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.0f"))
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.0f"))
    ax.tick_params(labelsize=fsize)
    ax.set_xlabel(r'$h_0/h$', fontsize=fsize)

axs[0].set_ylabel(r'$\|e_h\|_2$', fontsize=fsize)

fig.legend([lines[0][2], lines[2][0], lines[0][3], lines[0][1], lines[0][-1]],
           ['Hex', 'Prism', 'Pyramid+Tet', 'Tet', r'$O(h^{k+1}$)'],
           fontsize=fsize,
           ncol=5,
           loc='upper center')    

fig.savefig('plots/norms.eps', bbox_inches='tight')
plt.show()
