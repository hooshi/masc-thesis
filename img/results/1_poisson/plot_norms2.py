#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib

def read_norms(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[1] 

def myticks(x,pos):

    if x == 0: return "0"

    exponent = int(np.log10(x))
    coeff = x/10**exponent

    return "$\mathregular{%2.0f \\times 10^{%2d}}$" % (coeff,exponent)

matplotlib.rcParams['mathtext.fontset'] = 'stix'
fig, axs = plt.subplots(1, 3, sharey=True, figsize=(15,4))
# ---------------------------------------------
#  Initialize the Sets
# ---------------------------------------------

set_p = [2,3,4]
set_ele =  ['wedge', 'tet', 'hex', 'pyramid']
set_hcons = [1,2,3]
set_hname = [5,10,20,40,80]
lines=[[],[],[]]

for ele in set_ele:

    if (ele=='hex'):
        factor=1
        set_p = [2, 3, 4]
        set_mark = ['o', 'o','o']
        set_color = ['b','b','b']
        set_msize = [8,8,8]
        set_line = ['-','-','-']
        set_linew=[2,2,2]
    elif (ele=='wedge'):
        factor=2
        set_p = [2, 3, 4]
        set_mark = ['^', '^','^']
        set_color = ['r','r','r']
        set_msize = [8,8,8]
        set_line = ['-','-','-']
        set_linew= [2,2,2]
    elif (ele=='pyramid'):
        factor=4
        set_p = [2, 3, 4]
        set_mark = ['v', 'v','v']
        set_color = ['g','g','g']
        set_msize = [9,9,9]
        set_line = ['-','-','-']
        set_linew=[2,2,2]
    elif (ele=='tet'):
        factor=6
        set_p = [2, 3, 4]
        set_mark = ['>', '>', '>']
        set_color = ['c','c', 'c']
        set_msize = [9,9,9]
        set_line = ['-','-','-']
        set_linew= [2,2,2]

    for ip, p in enumerate(set_p):

        # ---------------------------------------------
        #  Read all the norms
        # ---------------------------------------------

        xvals = []
        yvals = []

        for h in set_hcons:
            comname = '%s_h%d_p%d' % (ele, h, p)
            fname = 'hst/%s.postproc' % comname
            xvals.append( set_hname[h]**3 * factor )
            yvals.append( read_norms(fname) )

        # ---------------------------------------------
        #  Plot all the norms
        # ---------------------------------------------
        print xvals, yvals
        ln, = axs[ip].loglog(xvals, yvals,
                   marker=set_mark[ip],
                   markersize=set_msize[ip],
                   color= set_color[ip],
                   linewidth=set_linew[ip],
                   linestyle=set_line[ip])
        lines[ip].append(ln)

# ---------------------------------------------
#  Plot the reference line
# ---------------------------------------------
# ln, = axs[0].loglog([10**3, 10**6], [10**-3, 10**-3],
#                color='k', linestyle='--', linewidth=1.5)
# lines[0].append(ln)

#axs[1].loglog([1, xvals[-1]], [0.01, 0.01/xvals[-1]**3],
#               color='k', linestyle='--', linewidth=1.5)

#axs[2].loglog([1, xvals[-1]], [0.01, 0.01/xvals[-1]**4],
#               color='k', linestyle='--', linewidth=1.5)


# ---------------------------------------------
#  Text in plot
# ---------------------------------------------
fsize=24

axs[0].text(0.3, 0.1, r'$k=1$',transform=axs[0].transAxes, fontsize=fsize)
axs[1].text(0.3, 0.1, r'$k=2$',transform=axs[1].transAxes, fontsize=fsize)
axs[2].text(0.3, 0.1, r'$k=3$',transform=axs[2].transAxes, fontsize=fsize)

# ---------------------------------------------
#  Nice plot
# ---------------------------------------------

ax = plt.gca()
for ax in axs:
#    ax.set_xlim([0, 4])
#    ax.set_ylim([1e-6, 1e-1])
    ax.tick_params(labelsize=fsize-2)
    ax.set_xlabel(r'$N_{DOF}$', fontsize=fsize)

axs[0].set_ylabel(r'$\|e_h\|_2$', fontsize=fsize)

# fig.legend([lines[0][2], lines[2][0], lines[0][3], lines[0][1]],
#           ['Hex', 'Prism', 'Pyramid+Tet', 'Tet'],
#           fontsize=fsize)    

fig.savefig('plots/norms2.eps', bbox_inches='tight')
plt.show()
