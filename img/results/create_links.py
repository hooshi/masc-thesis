#!/usr/bin/python

import os

cases = [
    '',
    '1_poisson/plots/',
    '2_euler_sphere/plots/',
    '3_flat_plate/plots/',
    '4_naca/plots/',
]

def renew_link(casenum, name):
    target = 'results/%s%s' % (cases[casenum], name)
    link = 'result_%d_%s' % (casenum, name)
    
    os.system('cd ..; rm -rf %s' % link);
    os.system('cd ..; ln -s -T %s %s' % (target, link) );
    os.system('echo \'cd ..; ln -s -T %s %s\'' % (target, link) );

def renew_all_links():
    
    renew_link(1, 'mesh_hex.png')
    renew_link(1, 'mesh_prism.png')
    renew_link(1, 'mesh_pyramid.png')
    renew_link(1, 'mesh_tet.png')
    renew_link(1, 'norms.eps')
    renew_link(1, 'norms2.eps')
    renew_link(1, 'bar.png')
    renew_link(1, 'exact.png')


    renew_link(2, 'entropy.eps')
    renew_link(2, 'entropy_cpu.pdf')
    renew_link(2, 'h2p2.eps')
    renew_link(2, 'h2p3.eps')
    renew_link(2, 'h2p4.eps')
    renew_link(2, 'h4p2.eps')
    renew_link(2, 'h4p3.eps')
    renew_link(2, 'h4p4.eps')
    renew_link(2, 'ptc.eps')
    renew_link(2, 'scaling.eps')
    renew_link(2, 'mesh.png')

    renew_link(3, 'scaling.eps')
    renew_link(3, 'ptc.eps')
    renew_link(3, 'mut.eps')
    renew_link(3, 'mut_1.eps')
    renew_link(3, 'mut_2.eps')
    renew_link(3, 'mut_3.eps')
    renew_link(3, 'mesh.eps')
    renew_link(3, 'anslib_nut.png')
    renew_link(3, 'fun3d_nut.png')

    renew_link(4, 'ptc.eps')
    renew_link(4, 'mesh_mix_gmsh.eps')
    renew_link(4, 'mesh_hex_gmsh.eps')
    renew_link(4, 'nut_mix.png')
    renew_link(4, 'nut_hex.png')
    renew_link(4, 'nut_bar.png')
    renew_link(4, 'nut_bar.png')
    renew_link(4, 'cp.eps')
    renew_link(4, 'cp_aiaa.eps')


if (__name__ == "__main__"):    
    renew_all_links()
