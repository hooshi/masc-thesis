# this file is not a function
THIS_IS_NOT_A_FUNCTION=1;
clear THIS_IS_NOT_A_FUNCTION;

# function to append norms from a file
function [N,res] = append_file(file_name)

data = load(file_name);
N = data(:,1);
res = data(:,2);

endfunction

# ---------------------------------------------------------------------------
#                                     2nd order
# ---------------------------------------------------------------------------
# 1-> lagrange 2->hierarchic 3->bernstein
clf, hold on;

# hie
[N,res] = append_file('bump_h_o2.pp');
semilogy(N, res, 'b-', 'linewidth', 3);
semilogy(N(1:3:end), res(1:3:end), 'bo;p=2;', 'markersize', 15,  'linewidth'
         , 3)

[N,res] = append_file('bump_h_o3.pp');
semilogy(N, res, 'r-', 'linewidth', 3);
semilogy(N(1:3:end), res(1:3:end), 'r^;p=3;', 'markersize', 15, 'linewidth'
         , 3)

## ------------------------  beautiful plot
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );

L = get(gca,'XLim');
# set(gca,'XTick',linspace(L(1),L(2),5))
set(gca,'YTick',logspace(-12,0,7))

copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 24 , "location" , "northeast", "linewidth",2);

hx=xlabel("CG iteration");
hy=ylabel("Residual");
set(hx, "fontsize", 24, "linewidth", 2);
set(hy, "fontsize", 24, "linewidth", 2);
set(gca,  "fontsize", 24);

print("bump_convergence.eps","-deps","-FArial", "-color", "-tight");

