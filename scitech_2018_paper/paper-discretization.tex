\section{Discretization Scheme}
\label{ch:disc}

\subsection{The Finite Volume Method}
\label{ch:disc_fvm}

The finite volume method seeks the vector of average solution values
inside every control volume, denoted as $\vvec U_h$. First, a discrete
solution function, $\vvec u_h=\vvec u_h (\xyz, \vvec U_h)$, is
constructed corresponding to every set of average solution values,
which is known as solution reconstruction. Herein, we use the
$k$-exact reconstruction method that will be introduced in
\autoref{ch:disc_kexact}. After employing a solution reconstruction
method, a system of algebraic differential equations is derived for
$\vvec U_h$, by casting equation \eqnref{eq:conserve} into the
integral form, and replacing $\vvec u$ with $\vvec u_h$:
\begin{equation}
  \label{eq:fvm}
  \frac{d\vvec U_{h,\kappa}}{dt}
  + \frac{1}{\Omega_\kappa}
  \int_{\partial \kappa}
  \left(
    \vvec \NumFluxF(\vvec u_h^+,\vvec u_h^-)-
    \vvec \NumFluxQ(
    \vvec u_h^+, \nabla \vvec u_h^+, \vvec u_h^-, \nabla \vvec u_h^-
    ) 
  \right) dS 
  -
  \frac{1}{\Omega_\kappa}
  \int_{\kappa} \vvec S(\vvec u_h, \nabla \vvec u_h) d\Omega
  = 0,
\end{equation}
for every control volume $\kappa$. Here, $\vvec U_{h,\kappa}$ and
$\Omega_\kappa$ are the sub-vector of average solution values and the
volume of $\kappa$, respectively. Although the discrete solution
$\vvec u_h$ and its gradient $\nabla \vvec u_h$ have to be continuous
inside every control volume, they can be discontinuous on the control
volume boundaries $\partial \kappa$. These discontinuous values are
shown using the $(.)^+$ and $(.)^-$ notations. $\NumFluxF$ and
$\NumFluxQ$ represent the numerical flux functions, and are designed
to imitate the product of the original flux matrices and the normal
vector, while taking into account the discontinuity of the discrete
solution.

Looking back at equation \eqnref{eq:fvm}, the following system of ODEs
for control volume averages can be derived:
% 
% Compact form
\begin{equation}
  \label{eq:nonlineareq}
  \frac{d \vvec U_h}{dt} + \vvec R( \vvec U_h) = 0.
\end{equation}
% 
Although only the steady-state solution is of interest in this work,
the unsteady terms can be used to improve the robustness of the solver
with respect to the initial solution guess. This method, known as
pseudo-transient continuation (PTC), will be discussed in
\autoref{ch:solu_PTC}.

\subsection{K-exact Reconstruction}
\label{ch:disc_kexact}

In this work, higher-order accuracy is achieved using the $k$-exact
reconstruction scheme \cite{barth1990}, which results in a nominal
discretization error of order $\BigO(h^{(k+1)})$ \cite{barth2002}.  In
this method, the solution in every control volume is defined as the
superposition of a set of basis functions in the form:
\begin{equation}
  \vvec u_h(\xyz; \vvec U_h)|_{x \in \kappa} =
  \vvec u_{h,\kappa}(\xyz; \vvec U_h) =
  \sum _{i=1}^{\nrec} \vvec a_{\kappa}^i(\vvec U_h) \phi_{\kappa}^i(\xyz),
\end{equation}
where $\phi_\kappa^i(\xyz)$ and $\vvec a_\kappa^i$ represent the $i$th
basis function and reconstruction coefficient for every control volume
$\kappa$, respectively. Commonly, the basis functions are chosen as
monomials in Cartesian coordinates with origin at the control volume's
centroid of volume, $\xyz_\kappa$. Therefore, the set of basis
functions can be expressed as:
\begin{equation}
  \label{eq:cart_mon}
  \left\{ \phi_\kappa^i(\xyz) | i = 1 \ldots \nrec \right\}
  =
  \left\{ \frac{1}{a!b!c!}
    (x_1-x_{\kappa1})^a
    (x_2-x_{\kappa2})^b
    (x_3-x_{\kappa3})^c
    | a+b+c \leq k \right\}.
\end{equation}
This particular choice of basis functions has the advantage that the
discrete solution $\vvec u_h$ will resemble the Taylor expansion of
the exact solution $\vvec u$, which is particularly useful in
theoretical analysis \cite{ollivier2002}. Nevertheless, monomials in
transformed coordinate systems maybe used in the case of highly
anisotropic meshes to prevent numerical issues \cite{jalali2017}.

The $k$-exact reconstruction requires $\vvec u_{h,\kappa}$ to predict
the average values of a specific set of the neighbors of $\kappa$,
$\Stencil(\kappa)$, closely. Moreover, the discrete solution must
satisfy the conservation of the mean constraint. Thus, the
reconstruction coefficients are found from the following constrained
minimization problem:
\begin{equation}
  \label{eq:recon_min}
  \begin{aligned}
    % 
    & \underset{
      \vvec{a}_{\kappa}^1 \ldots \vvec{a}_{\kappa}^{\nrec}}{
      \text{minimize}}
    & & \sum_{\sigma \in \Stencil(\kappa)}
    \left(
      \frac{1}{\Omega_\sigma}\int_{\sigma}
      \vvec u_{h,\kappa}(\xyz) d\Omega -
      \vvec U_{h,\sigma} 
    \right)^2
    \\
    & \text{subject to}
    & & \frac{1}{\Omega_\kappa} \int_{\kappa}
     \vvec u_{h}(\xyz) d\Omega = \vvec U_{h,\kappa},
  \end{aligned}
\end{equation}
which is first transformed into an unconstrained optimization problem
by a change of variables \cite{ollivier2002}, and then solved using
the SVD decomposition algorithm \cite{golub2012}. To construct
$\Stencil(\kappa)$, all the neighbors at a given topological distance
from $\kappa$ are added to the stencil until the number of stencil
members gets bigger than $1.5\nrec$. This strategy ensures that the
minimization problem of Equation \eqnref{eq:recon_min} does not become
undetermined or ill-conditioned.

\subsection{Numerical Flux Functions}
\label{ch:disc_numf}

In this work, the inviscid flux function is evaluated using a
computationally efficient formulation \cite{roe1985} of the
approximate Riemann solver of Roe \cite{roe1981} because of its
effectiveness and simplicity. This formulation has the form of:
\begin{equation}
  \vvec \NumFluxF (\vvec u_h^+,\vvec u_h^-) =
  \frac{1}{2}
  \left(
    \mmat F(\vvec u_h^+) \nvec n +
    \mmat F(\vvec u_h^-) \nvec n -
    \vvec D(\vvec u_h^+, \vvec u_h^-)
  \right),
\end{equation}
where $\vvec D$ represents the diffusion vector given by the formula:
\begin{equation}
  \label{eq:roediff}
  \vvec D(\vvec u_h^+, \vvec u_h^-) =
  \begin{bmatrix}
    |\tilde \lambda_2| \Delta \rho
    + \delta_1\\
    |\tilde \lambda_2| \Delta (\rho \vel)
    + \delta_1 \tilde \vel + \delta_2 \nvec n \\
    |\tilde \lambda_2| \Delta E  + \delta_1 \tilde H
    + \delta_2 \tilde \vel \cdot \nvec n \\
    |\tilde \lambda_2| \Delta (\rho \nut)  + \delta_1 \tilde \nut \\
  \end{bmatrix}.
\end{equation}
The symbol $\Delta(.)$ stands for $(.)_h^+ - (.)_h^-$, while
$\lambda_1 = \vel \cdot \nvec n - c$,
$\lambda_2 = \ldots = \lambda_5 = \vel \cdot \nvec n$, and
$\lambda_6 = \vel \cdot \nvec n + c$ represent the six eigenvalues of
the Jacobian matrix
$\frac{\partial \mmat F(\vvec u) \nvec n}{ \partial \vvec u}$.
Furthermore, $c=\sqrt { \frac{\gamma P}{\rho} }$ is the speed of
sound.  In Equation \eqnref{eq:roediff} the symbol $\widetilde{(.)}$
represents the Roe average state, evaluated as:
\begin{equation}
    \widetilde{(.)} =
    \begin{cases}
      \sqrt{(.)_h^- (.)_h^+} & (.)=\rho \\
      \frac{ \sqrt{\rho_h^-} (.)_h^- + \sqrt{\rho_h^+} (.)_h^+}{\sqrt{\rho_h^+}+\sqrt{\rho_h^+}} &(.)=\vel, \nut, H
  \end{cases},
\end{equation}
where $H=\frac{P+E}{\rho}$ is the enthalpy.  The variables $\delta_1$
and $\delta_2$ in Equation \eqnref{eq:roediff} are given as:
\begin{equation}
  \begin{gathered}
    \delta_1 =
    \frac{\Delta P}{\tilde c^2}
    \left(
      -|\tilde \lambda_2| + \frac{|\tilde \lambda_1| + |\tilde \lambda_6|}{2} 
    \right) +
    \frac{\tilde \rho  }{2 \tilde c^2} \Delta (\vel \cdot \nvec n)
    \left(
      |\tilde \lambda_6| - |\tilde \lambda_1| 
    \right)  
    \\
    \delta_2 =
    \frac{\Delta P}{2 \tilde c^2}
    \left(
      |\tilde \lambda_6| - |\tilde \lambda_1| 
    \right) +
    \tilde \rho \Delta (\vel \cdot \nvec n)
    \left(
      -|\tilde \lambda_2| + \frac{|\tilde \lambda_1| + |\tilde \lambda_6|}{2} 
    \right).    
  \end{gathered}
\end{equation}
Note that $\tilde \vel$ and $\tilde \nut$ are the velocity and the S-A
working variable at the Roe average state, respectively.

The viscous flux functions are evaluated using an intermediate state
$\vvec u_h^*$, such that:
$\vvec \NumFluxQ=\mmat Q(\vvec u_h^*, \nabla \vvec u_h^*)\nvec n$.
Finding the intermediate state as the numerical average of the left
and right states,
$ \vvec u_h^* = \frac{1}{2} \left( \vvec u_h^+ + \vvec u_h^- \right)$,
results in a sufficiently accurate solution \cite{nishikawa2011}. When
evaluating the intermediate state gradient, however, simple averaging
may lead to spurious solutions and instabilities. Nishikawa
\cite{nishikawa2010} suggested the following modified formula:
\begin{equation}
  \nabla \vvec u_h^* =
  \frac{1}{2} \left( \nabla \vvec u_h^+ + \nabla \vvec u_h^- \right) +
  \eta \left(
    \frac{\vvec u_h^+ - \vvec u_h^-}{ \|\xyz_{\kappa+} - \xyz_{\kappa-} \|_2  }
  \right)   \nvec n,
\end{equation}
where $\xyz_{\kappa-}$ and $\xyz_{\kappa+}$ are the reference
locations of the adjacent control volumes, respectively, and $\eta$ is
a heuristic damping factor, known as the jump term. Jalali \etal{}
\cite{jalali2014} have numerically tested different values of $\eta$
in high-order finite volume simulations, and have recommended a value
of $\eta=1$, which is also used in this work.

%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
