#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt

if __name__ == "__main__":

    # ADD the subplots
    plt.close('all')
    fig, (ax) = plt.subplots(1, 1, sharey=True)

    set_job = []
    set_job.append(hst.ConvergenceHistory("hst/a4_medium_full_qmd.hst"))
    set_job.append(hst.ConvergenceHistory("hst/a4_medium_onlyone_rcm.hst"))
    set_job.append(hst.ConvergenceHistory("hst/a4_medium_ksp_rcm.hst"))

    lines = []
    set_linetype = ['-','--','-.'];
    #-------------------  PLOT
    for ijob, job in enumerate(set_job):
        job.init_axis()
        ltype = set_linetype[ijob]
        xvals =job.yaxis_cpu
        yvals = job.yaxis_res_nl[0]
        ln, = ax.semilogy( xvals, yvals,
                           linestyle=ltype, linewidth=2,
                           color='k') 
        lines.append(ln)


    #-------------------  FONT SIZE    
    fsize=17
    #-------------------  LEGEND
    lgnd=ax.legend((lines[0],lines[1],lines[2]),
                   ('Case A','Case B', 'Case C'),
                   loc="upper right",fontsize=fsize)

    #------------------- OUTER AXIS
    ax.set_ylabel(r'Residual', fontsize=fsize)
    ax.set_xlabel(r'Wall time, Sec.', fontsize=fsize)

    #------------------- Small AXIS
    ax.tick_params(labelsize=fsize)
    ax.locator_params(axis='y', numticks=6)
    ax.locator_params(axis='x', nbins=5)


    fig.savefig('plots/res_comparison.eps', bbox_inches='tight')
    plt.show()
