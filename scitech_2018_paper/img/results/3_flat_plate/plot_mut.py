#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

def read_mut_data(fname):
    array=np.loadtxt(fname, dtype='float')
    return array[:,0], array[:,2], array[:,1], array[:,3]


if __name__ == "__main__":

    fig, (ax3) = plt.subplots(1, 1, sharex=True)
    ax3.set_zorder(100)
    nasa=np.loadtxt('hst/mut.txt', dtype='float')


    # ------------------------------------------
    #       Fine MESH
    # ------------------------------------------

    #-------------------- Small axes

    axz1 = plt.axes([.55, .45, .3, .4])
    axz1.set_zorder(600)
    plt.setp(axz1, xticks=[], yticks=[])

    axz1.set_xlim([198, 210])
    axz1.set_ylim([0.0046, 0.0094])

    circle1 = Ellipse(xy=(202, 0.007), width=40, height=0.01, color='k', fill=False, zorder=0)
    ax3.add_artist(circle1)

    # ------------------- PLOT
    kwargs_nasa=dict(color='k',
                     linestyle=':', marker='.',  markerfacecolor='none',
                     markeredgecolor='k', markeredgewidth=2,
                     zorder=101, markevery=1)
    kwargs_k1 = dict(color='k', linestyle='-', linewidth=2, zorder=99)
    kwargs_k2 = dict(color='k', linestyle='--', linewidth=2, zorder=101)
    kwargs_k3 = dict(color='k', linestyle='-.', linewidth=2, zorder=102)
    
    ln1, = ax3.plot(nasa[:,2], nasa[:,1], **kwargs_nasa)
    axz1.plot(nasa[:,2], nasa[:,1], **kwargs_nasa)
    
    yb, ye, mutb, mute = read_mut_data('hst/h3_p2.serial.postproc')
    for i in range(len(yb)):
        ln2, = ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]], **kwargs_k1)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], **kwargs_k1)
        
    yb, ye, mutb, mute = read_mut_data('hst/h3_p3.serial.postproc')
    for i in range(len(yb)):
        ln3, = ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]],  **kwargs_k2)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], **kwargs_k2)
        
    yb, ye, mutb, mute = read_mut_data('hst/h3_p4.serial.postproc')
    for i in range(len(yb)):
        ln4, = ax3.plot([mutb[i], mute[i]], [yb[i], ye[i]], **kwargs_k3)
        axz1.plot([mutb[i], mute[i]], [yb[i], ye[i]], **kwargs_k3)


    # ------------------------------------------
    #   Beautiful stuff
    # ------------------------------------------
    FONT_SIZE=17   
    ax3.set_ylim([0, 0.03])
    ax3.locator_params(axis='y', nbins=3)


    ax3.set_xlabel(r'Nondimensional eddy viscosity', fontsize=FONT_SIZE)
    ax3.set_ylabel('Y', fontsize=FONT_SIZE)
    ax3.tick_params(labelsize=FONT_SIZE)
    

    lgnd=ax3.legend((ln1,ln2,ln3,ln4),
                    ('NASA TMR','$k=1$','$k=2$','$k=3$'),
                    ncol=1, loc="upper left",
                    fontsize=FONT_SIZE) #bbox_to_anchor=(0,0)
    lgnd.set_zorder(500)


    fig.savefig('plots/mut.eps', bbox_inches='tight')
    plt.show()



