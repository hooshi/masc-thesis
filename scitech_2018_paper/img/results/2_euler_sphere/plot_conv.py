#!/usr/bin/python

import os
import sys
import inspect
import numpy as np
sys.path.insert(0, '../')

import history as hst
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

if __name__ == "__main__":

    
    # ---------------------------------------------------
    # Read Info
    # --------------------------------------------------
    
    jobsA= []
    jobsA.append(hst.ConvergenceHistory("hst/h2_p2_curve.hst"))
    jobsA.append(hst.ConvergenceHistory("hst/h2_p3_curve.hst"))
    jobsA.append(hst.ConvergenceHistory("hst/h2_p4_curve.hst"))

    jobsB= []
    jobsB.append(hst.ConvergenceHistory("hst/h3_p2_curve.hst"))
    jobsB.append(hst.ConvergenceHistory("hst/h3_p3_curve.hst"))
    jobsB.append(hst.ConvergenceHistory("hst/h3_p4_curve.hst"))

    jobsC= []
    jobsC.append(hst.ConvergenceHistory("hst/h4_p2_curve.hst"))
    jobsC.append(hst.ConvergenceHistory("hst/h4_p3_curve.hst"))
    jobsC.append(hst.ConvergenceHistory("hst/h4_p4_curve.hst"))

    for j in jobsA: j.init_axis()
    for j in jobsB: j.init_axis()
    for j in jobsC: j.init_axis()


    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
    ax = fig.add_subplot(111)
    ax1.set_zorder(100)
    ax2.set_zorder(100)
    ax3.set_zorder(100)
    ax.set_zorder(0)
    fig.set_figwidth(14)
    
    # ---------------------------------------------------
    # Plot norms
    # ---------------------------------------------------
    
    ln1, = ax1.semilogy (jobsA[0].xaxis_nl, jobsA[0].yaxis_res_nl[0], 'b-o', linewidth=2, markersize=10, markevery=3)
    ln2, = ax1.semilogy (jobsB[0].xaxis_nl, jobsB[0].yaxis_res_nl[0], 'r-^', linewidth=2, markersize=10, markevery=3)
    ln3, = ax1.semilogy (jobsC[0].xaxis_nl, jobsC[0].yaxis_res_nl[0], 'g-v', linewidth=2, markersize=10, markevery=3)

    ax2.semilogy (jobsA[1].xaxis_nl, jobsA[1].yaxis_res_nl[0], 'b-o', linewidth=2, markersize=10, markevery=3)
    ax2.semilogy (jobsB[1].xaxis_nl, jobsB[1].yaxis_res_nl[0], 'r-^', linewidth=2, markersize=10, markevery=3)
    ax2.semilogy (jobsC[1].xaxis_nl, jobsC[1].yaxis_res_nl[0], 'g-v', linewidth=2, markersize=10, markevery=3)

    ax3.semilogy (jobsA[2].xaxis_nl, jobsA[2].yaxis_res_nl[0], 'b-o', linewidth=2, markersize=10, markevery=3)
    ax3.semilogy (jobsB[2].xaxis_nl, jobsB[2].yaxis_res_nl[0], 'r-^', linewidth=2, markersize=10, markevery=3)
    ax3.semilogy (jobsC[2].xaxis_nl, jobsC[2].yaxis_res_nl[0], 'g-v', linewidth=2, markersize=10, markevery=3)

    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    ax1.text(7, 50, '$k=1$', fontsize=20)
    ax2.text(7, 50, '$k=2$', fontsize=20)
    ax3.text(7, 50, '$k=3$', fontsize=20)
    
    # ---------------------------------------------------
    # Details
    # ---------------------------------------------------
    
    ax1.locator_params(axis='y', numticks=8)
    ax1.locator_params(axis='x', nbins=3)
    ax2.locator_params(axis='x', nbins=3)
    ax3.locator_params(axis='x', nbins=3)

    ax1.set_ylabel(r'$\| \mathbf{R}(\mathbf{U}_h) \|_2$', fontsize=20)
    ax.set_xlabel('PTC iteration', fontsize=20)
    
    ax1.tick_params(labelsize=20)
    ax2.tick_params(labelsize=20)
    ax3.tick_params(labelsize=20)

    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelsize=20, labelcolor='white', top='off', bottom='off', left='off', right='off')

    lgnd=ax3.legend((ln1,ln2,ln3),
               ('#CV = 64K','#CV = 322K', '#CV = 1M'),
               ncol=1, bbox_to_anchor=(1.6,1.), loc="upper right")
    lgnd.set_zorder(500)


    figsize = fig.get_size_inches()
    print "Size in Inches", figsize
    fig.savefig('plots/ptc.eps',
                bbox_inches='tight')
        
    plt.show()




    # ---------------------------------------------------
    # Garbage Can
    # ---------------------------------------------------
    
    # idx = 10;
    # ax.annotate('#CV=1M',
    #             xy=(jobsF[2].xaxis_nl[idx]+.1, jobsF[2].yaxis_res_nl[0][idx]),
    #             xytext=(jobsF[2].xaxis_nl[idx]+2,
    #                     jobsF[2].yaxis_res_nl[0][idx]),
    #             arrowprops=dict(arrowstyle="->", connectionstyle="arc",
    #                          linewidth=2),
    #             fontsize=17 )

    # idx = 8;
    # ax.annotate('#CV=16K',
    #             xy=(jobsC[2].xaxis_nl[idx]-.1, jobsC[2].yaxis_res_nl[0][idx]),
    #             xytext=(jobsC[2].xaxis_nl[idx]-5,
    #                     jobsC[2].yaxis_res_nl[0][idx]),
    #             arrowprops=dict(arrowstyle="->",
    #                             connectionstyle="arc",
    #                             linewidth=2),
    #             fontsize=17, )
