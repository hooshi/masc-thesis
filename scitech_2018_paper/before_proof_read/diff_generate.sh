#!/bin/bash

## Create the folder if it is not there
mkdir -p difftex

## Produce the diff files

# DIFF='/home/hooshi/bin/latexdiff -c diff_config.cfg --exclude-textcmd="section,subsection,subsubsection" '
DIFF='/home/hooshi/bin/latexdiff --exclude-textcmd="subsubsection" '
echo "${DIFF}"

## OLD NEW


FILE=paper-introduction.tex
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}


FILE=paper-equations.tex          
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

FILE=paper-discretization.tex     
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

FILE=paper-solver.tex     
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

FILE=paper-results.tex    
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

FILE=paper-conclusion.tex 
echo "${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}"
${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

