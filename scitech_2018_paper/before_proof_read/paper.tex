\documentclass[conf]{new-aiaa}

%% -------------------------------------------------------
%% PACKAGES
%% -------------------------------------------------------

\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{diagbox}
\usepackage{xcolor}
\usepackage{multirow}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{graphicx}
\usepackage{xpatch} %% Convert section 1 hyperref to section VI.A.1

%% For draft
% \usepackage [pagebackref=true,
% colorlinks,
% linkcolor=blue,
% citecolor=magenta,
% urlcolor=cyan]{hyperref}

% For publication
\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.8 0.8 0.8},%
linktocpage%
]{hyperref}


%% -------------------------------------------------------
%% MACROS
%% -------------------------------------------------------

%%
%% Symbols and abbreviations
%%

\newcommand{\vvec}[1]{\mathbf{#1}}
\newcommand{\nvec}[1]{\mathbf{#1}}
\newcommand{\mmat}[1]{\bm{#1}}

\newcommand{\SetCV}{\mathcal T_h}
\newcommand{\BdryCond}{\mathcal B}
\newcommand{\NumFluxF}{\mathcal F}
\newcommand{\NumFluxQ}{\mathcal Q}
\newcommand{\Stencil}{\operatorname{\mathit{Stencil}}}
\newcommand{\BdGauss}{\operatorname{\mathit{BdryQuad}}}
\newcommand{\MinNeigh}{\operatorname{\mathit{MinNeigh}}}
\newcommand{\Proj}{\operatorname{\mathit{Proj}}}
\newcommand{\CFL}{\operatorname{CFL}}
\newcommand{\kappals}{\kappa_{\operatorname{\mathit{LS}}}}
\newcommand{\IL}{\tilde{\mmat L}}
\newcommand{\IU}{\tilde{\mmat U}}

\newcommand{\vel}{\vvec{v}}
\newcommand{\nut}{\tilde{\nu}}
\newcommand{\Reyn}{\operatorname{\mathit{R\kern-.04em e}}}
\newcommand{\Pran}{\operatorname{\mathit{P\kern-.04em r}}}
\newcommand{\Mach}{\operatorname{\mathit{M\kern-.04em a}}}
\newcommand{\Diff}{\operatorname{\mathit{Diff}}}
\newcommand{\Prod}{\operatorname{\mathit{Prod}}}
\newcommand{\Dist}{\operatorname{\mathit{Dest}}}
\newcommand{\Trip}{\operatorname{\mathit{Trip}}}
\newcommand{\atol}{\operatorname{\mathit{atol}}}
\newcommand{\rtol}{\operatorname{\mathit{rtol}}}

\newcommand{\xyz}{\vvec{x}}
\newcommand{\trace}{\operatorname{\mathit{Trace}}}
\newcommand{\BigO}{O}

\newcommand{\anslib}{ANSLib}
\newcommand{\petsc}{PETSc}
\newcommand{\dof}{DOF}
\newcommand{\ndof}{{N_\text{DOF}}}
\newcommand{\ndim}{{N_\text{dim}}}
\newcommand{\nunk}{{N_\text{unk}}}
\newcommand{\nrec}{{N_\text{rec}}}
\newcommand{\nlag}{{N_\text{lag}}}
\newcommand{\ncv}{{N_\text{CV}}}
\newcommand{\nqua}{{N_\text{qua}}}
\newcommand{\nacaoolz}{NACA 0012}

%% not in math mode
\newcommand{\nptc   }{{N-PTC}}
\newcommand{\ngmres }{{N-GMRES}}
\newcommand{\tct }{{TST}}
\newcommand{\lst }{{LST}}


\newcommand{\NA}{\textsc{n/a}}	% for "not applicable"
\newcommand{\eg}{e.g.,\ }	% proper form of examples (\eg a, b, c)
\newcommand{\ie}{i.e.,\ }	% proper form for that is (\ie a, b, c)
\newcommand{\etal}{\emph{et al}}

%%
%% Horizontal and vertical captions in images
%%
\newlength{\tempheight}
\newlength{\tempwidth}
\newcommand{\rowname}[1]% #1 = text
{\rotatebox{90}{\makebox[\tempheight][c]{#1}}}
\newcommand{\columnname}[1]% #1 = text
{\makebox[\tempwidth][c]{#1}}

%%
%% Algorithm configuration
%%
\makeatletter
\newcommand{\algmargin}{\the\ALG@thistlm}
\makeatother
\algnewcommand{\parState}[1]{\State%
  \parbox[t]{\dimexpr\linewidth-\algmargin}{\strut #1\strut}}


%%
%% Hyperref, autoref, and titlesec configuration
%%
\renewcommand{\thesection}{\Roman{section}}
\renewcommand{\thesubsection}{\Alph{subsection}}
\renewcommand{\thesubsubsection}{\arabic{subsubsection}}
\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}
\renewcommand{\subsubsectionautorefname}{Section}
\newcommand{\algorithmautorefname}{Algorithm}
\makeatletter
\AtBeginDocument{%
  \xpretocmd{\p@section}{}{}{}
  \xpretocmd{\p@subsection}{\thesection.}{}{}
  \xpretocmd{\p@subsubsection}{\thesection.\thesubsection.}{}{}
}
\makeatother


%% -------------------------------------------------------
%% METADATA
%% -------------------------------------------------------

\title{A Higher-Order Unstructured Finite Volume Solver for
  Three-Dimensional Compressible Flows}

\author{ Shayan Hoshyari %
  \footnote{M.Sc. Student, Department of Computer Science, 
    hoshyari@cs.ubc.ca, Member AIAA}
  and Carl Ollivier-Gooch %
  \footnote{Professor, Department of Mechanical Engineering, 
    cfog@mech.ubc.ca, Associate Fellow AIAA} 
}
\affil{The University of British Columbia, 
Vancouver, BC, V6T 1Z4, Canada }

 % Define commands to assure consistent treatment throughout document
 \newcommand{\eqnref}[1]{(\ref{#1})}
 \newcommand{\class}[1]{\texttt{#1}}
 \newcommand{\package}[1]{\texttt{#1}}
 \newcommand{\file}[1]{\texttt{#1}}
 \newcommand{\BibTeX}{\textsc{Bib}\TeX}

 %% ----------------------------------------------------------------
%% LATEX DIFF STUFF
%% ----------------------------------------------------------------

 % Redefine these commands for chapter/sections and change below
\DeclareRobustCommand{\hsout}[1]{\texorpdfstring{\sout{#1}}{#1}}
\DeclareRobustCommand{\hwave}[1]{\texorpdfstring{\uwave{#1}}{#1}}

%DIF PREAMBLE EXTENSION ADDED BY LATEXDIFF
%DIF UNDERLINE PREAMBLE %DIF PREAMBLE
\RequirePackage[normalem]{ulem}% DIF PREAMBLE
\RequirePackage{color}\definecolor{RED}{rgb}{1,0,0}\definecolor{BLUE}{rgb}{0,0,1}%DIF PREAMBLE
\providecommand{\DIFadd}[1]{{\protect\textcolor{blue}{\hwave{#1}}}}% DIF PREAMBLE
\providecommand{\DIFdel}[1]{{\protect\textcolor{red}{\hsout{#1}}}}% DIF PREAMBLE


% DIF PREAMBLE
%DIF SAFE PREAMBLE %DIF PREAMBLE
\providecommand{\DIFaddbegin}{} %DIF PREAMBLE
\providecommand{\DIFaddend}{} %DIF PREAMBLE
\providecommand{\DIFdelbegin}{} %DIF PREAMBLE
\providecommand{\DIFdelend}{} %DIF PREAMBLE
%DIF FLOATSAFE PREAMBLE %DIF PREAMBLE
\providecommand{\DIFaddFL}[1]{\DIFadd{#1}} %DIF PREAMBLE
\providecommand{\DIFdelFL}[1]{\DIFdel{#1}} %DIF PREAMBLE
\providecommand{\DIFaddbeginFL}{} %DIF PREAMBLE
\providecommand{\DIFaddendFL}{} %DIF PREAMBLE
\providecommand{\DIFdelbeginFL}{} %DIF PREAMBLE
\providecommand{\DIFdelendFL}{} %DIF PREAMBLE
% DIF END PREAMBLE EXTENSION ADDED BY LATEXDIFF

%% -------------------------------------------------------
%% -------------------------------------------------------
%% BEGIN DOCUMENTS
%% -------------------------------------------------------
%% ------------------------------------------------------- 
\begin{document}

\maketitle

%% -------------------------------------------------------
%% -------------------------------------------------------
%% ABSTRACT
%% -------------------------------------------------------
%% -------------------------------------------------------

\begin{abstract}
  We present a three-dimensional higher-order-accurate finite volume
  algorithm for the solution of steady-state compressible flow
  problems. Higher-order accuracy is achieved by constructing a
  piecewise continuous representation of the average solution values
  using the $k$-exact reconstruction scheme. The pseudo-transient
  continuation method is employed to reduce the solution of the
  discretized system of nonlinear equations into the solution of a
  series of linear systems, which are subsequently solved using the
  GMRES method. We propose a preconditioning algorithm based on inner
  GMRES iterations and lines of strong coupling between unknowns, and
  show that it can enhance the speed and reduce the memory cost of the
  solver. Finally, we verify the developed finite volume algorithm by
  solving a set of test problems, where we attain optimal solution
  convergence with mesh refinement.
\end{abstract}

\input{difftex/paper-introduction}
\input{difftex/paper-equations}
\input{difftex/paper-discretization}
\input{difftex/paper-solver}
\input{difftex/paper-results}
\input{difftex/paper-conclusion}


%% -------------------------------------------------------
%% -------------------------------------------------------
%%        BIBLIOGRAPHY
%% -------------------------------------------------------
%% -------------------------------------------------------
\bibliography{paper}
%%\bibliographystyle{new-aiaa}

\end{document}

%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
