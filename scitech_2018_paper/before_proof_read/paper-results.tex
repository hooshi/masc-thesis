\section{Results}
\label{ch:results}

The performance of the GMRES-LO-ILU preconditioning method is
demonstrated in computing the flow around a \nacaoolz{} airfoil, where
the computations are carried out by a single core of an Intel i7-4790
($3.60$ GHz) CPU. Next, the inviscid flow around a sphere is
considered. Finally, our finite volume solution is verified for a
fully turbulent flow over a three-dimensional flat plate, and an
extruded \nacaoolz{} airfoil. All the three-dimensional flow
simulations were performed using the Grex cluster from the WestGrid
computing facilities \cite{westgrid}. Each node of the cluster
consists of two 6-core Intel Xeon X5650 2.66GHz processors, and has a
minimum of 48GB memory. The wall times and memory costs are reported
using 8 processors from 8 separate nodes.

%% -------------------------------------------------------
%% 1- Precon, NACA
%% -------------------------------------------------------
\subsection{Numerical Comparisons}
\label{ch:result_naca}

The performance of the proposed preconditioning algorithm is studied
by considering the turbulent flow around a \nacaoolz{} airfoil, with
$\Mach=0.15$, $\Reyn=6 \times 10^6$, and an angle of attack of 10
degrees. Three nested meshes with approximately $25$K, $100$K, and
$400$K control volumes ($\ncv=25$K,$100$K, and $400$K) are considered,
where the coarsest mesh and its corresponding lines of strong unknown
coupling are shown in \autoref{fig:naca_lines}. The farfield
boundaries are located almost $500$ chords away from the airfoil, and
the chord length has a nondimensional value of one.  This problem is
taken from the NASA Turbulence Modeling Resource (TMR) website
\cite{nasatmr}, and has been previously studied by Jalali and
Ollivier-Gooch \cite{jalali2017}, where they used a HO-ILU$3$
preconditioner with QMD reordering. Only the highest-order
discretization scheme of our solver ($3$-exact) is considered here,
since it results in the stiffest linear systems that are most
difficult to solve. Also, the initial conditions are taken from the
steady-state solution of the lower-order $2$-exact scheme.

\begin{figure}
  \centering
    \includegraphics[width=0.45\linewidth]{thesis-img/sts_view_whole.eps}
    \includegraphics[width=0.45\linewidth]{thesis-img/sts_lines_whole.eps} \\
    \includegraphics[width=0.45\linewidth]{thesis-img/sts_view_close.eps}
    \includegraphics[width=0.45\linewidth]{thesis-img/sts_lines_close.eps} \\
  \caption{Mesh and lines of strong unknown coupling for the geometry of
    a \nacaoolz{} airfoil}
  \label{fig:naca_lines}
\end{figure}

The ILU based preconditioning methods tested are shown in
\autoref{tab:naca_pname}. In all cases, the outer GMRES solver stops
when the linear residual of Equation~\eqnref{eq:PTC} is reduced by a
factor of $10^{-3}$, or a maximum number of $500$ iterations is
performed. The maximum Krylov subspace size is $100$ for the outer
GMRES solver, and the initial $\CFL$ number is set to
$10^{-2}$. Furthermore, the simulation ends when the norm of the
residual vector $\| \vvec R(\vvec U_h) \|_2$ is reduced by a factor of
$10^{-8}$.  Note that other combinations of preconditioner and
reordering schemes that are not considered in \autoref{tab:naca_pname},
did not result in convergence even for the coarse mesh.

\begin{table} \centering
  \caption{Preconditioning methods considered}
  \label{tab:naca_pname}
  \begin{tabular}{ccc c}
    \hline
    Case &Preconditioning &Reordering & Number of inner          \\
    name &method   &algorithm     &GMRES iterations             \\
    \hline
    A         &HO-ILU$3$        &QMD
    &---                                          \\
    B         &LO-ILU$0$        &RCM
    &---                                          \\
    C         &LO-ILU$0$        &lines
    &---                                          \\
    D         &GMRES-LO-ILU$0$  &RCM
    &$10$                                         \\
    E         &GMRES-LO-ILU$0$  &lines   
    &$10$                                         \\
    \hline
  \end{tabular}
\end{table}

The residual history of the solver for each preconditioning algorithm
is shown in \autoref{fig:naca_hiscoarse}. Our proposed preconditioning
algorithm (case E) outperforms all the other methods on both mesh
sizes, and takes only half the time of the HO-ILU$3$ algorithm (case
A) to find the steady-state solution. Furthermore, the line reordering
algorithm shows its prominence on the $100$K control volume mesh,
where the RCM reordering algorithm fails.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{thesis-img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{thesis-img/sts_res_comparison_aiaa.eps}
  \caption{Comparison of residual histories for the \nacaoolz{} problem
    obtained using different preconditioning algorithms}
  \label{fig:naca_hiscoarse}
\end{figure}


\autoref{tab:naca_pperf} shows the number of PTC iterations (\nptc{}),
the number of outer GMRES iterations (\ngmres{}), total memory
consumption, CPU time spent on the linear solver (\lst{}), and total
computational time (\tct{}). The proposed preconditioning scheme (case
E) speeds up the linear solve process by a factor of three, and
results in a twofold reduction in memory consumption, compared to the
HO-ILU$3$ method. The shortcomings of the LO-ILU methods (cases B and
C) are evident because of their large number of PTC iterations and
failure in convergence, on the coarse and medium meshes,
respectively. Also, the memory consumption scales linearly with
respect to the problem size for the proposed preconditioning scheme,
whereas the linear solve time seems to be increasing at a much more
rapid rate. 

\begin{table}
  \centering
  \caption[Performance comparison for different preconditioning
  schemes]{Performance comparison for different preconditioning
    schemes. Cases with entries marked by ``$-$'' did not converge
    to the steady state solution.}
  \label{tab:naca_pperf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    Preconditioner &\nptc   &\ngmres
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=25$K} \\
    \hline
          A  &$37$   &$956$      &$1.4$      &$416$   &$635$  \\
          B  &$44$   &$10,893$   &$0.7$      &$264$   &$572$  \\
          C  &$51$   &$13,692$   &$0.7$      &$328$   &$705$  \\
          D  &$34$   &$1,856$    &$0.7$      &$134$   &$379$   \\
          E  &$34$   &$1,787$    &$0.7$      &$118$   &$361$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=100$K} \\
    \hline
          A  &$39$  &$4,640$   &$5.9$      &$3,122$  &$4,068$     \\
          B  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          C  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          D  &$-$   &$-$       &$3.2$      &$-$      &$-$         \\
          E  &$36$  &$4,396$   &$3.2$      &$1,308$  &$2,348$      \\
    \hline
  \end{tabular}
\end{table}

%% -------------------------------------------------------
%% 2- Euler, Sphere
%% -------------------------------------------------------
\subsection{Inviscid Flow Over a Sphere}
\label{ch:result_sphere}

The second test problem is the inviscid flow around a sphere with unit
radius. The Mach number is equal to $\Mach=0.38$, and the free-stream
flow is in the $x_1$ direction. This problem is chosen as a
three-dimensional extension of the inviscid flow around a circle, the
higher-order solution of which was studied by Bassi and Rebay
\cite{bassi1997}. Three prismatic grids (64K, 322K, and 1M control
volumes) are used, where the farfield is located at a distance of 100
dimensionless units from the sphere center. A symmetric cut of the
coarsest mesh near the sphere surface is shown in
\autoref{fig:c5_2_mesh}.  The faces and control volumes adjacent to
the wall boundaries are curved using second-order Lagrange
polynomials, as quadratic functions are sufficient to represent a
spherical surface. To ensure that numerical quadrature does not
introduce additional error in the solution, a quadrature scheme
accurate up to order $k+1$ is employed.


\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{thesis-img/result_2_mesh.png}
  \caption{Symmetric cut of the coarsest mesh near the sphere
    surface.}
  \label{fig:c5_2_mesh}
\end{figure}

As the flow does not contain any discontinuities, the entropy must be
constant throughout the domain. Therefore, the $L_2$ norm of the
entropy relative to the free-stream conditions, $\| S-S_\infty \|_2$,
has an exact value of zero. The convergence of this quantity versus
mesh refinement is shown in \autoref{fig:c5_2_entnorm}, where the mesh
length scale is defined as $h=(\ncv)^{1/3}$, and $\ncv$ is the number
of control volumes. The $k=1$, and $3$ solutions converge even faster
than their expected ratios of $2$ and $4$, respectively. The
convergence ratio for the $k=2$ solution, however, is half an order
smaller than its expected value of $3$.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{thesis-img/result_2_entropy.eps}
  \caption{Relative entropy norm versus mesh size for the sphere
    problem.}
  \label{fig:c5_2_entnorm}
\end{figure}

The effect of the reconstruction order $k$ on solution accuracy is
also evident from the qualitative Mach contours on the $x_3=0$
symmetry plane, which are plotted in \autoref{fig:c5_2_mach} for the
finest mesh. Not surprisingly, only the $k=3$ solution has been able
to capture the symmetry of the flow. Furthermore, the artificial wake
behind the sphere seems smaller for the $k=2$ solution compared to
that of $k=1$.

\begin{figure}
  \setlength{\tempwidth}{0.31\linewidth}
  \centering
  \includegraphics[width=\tempwidth]{thesis-img/result_2_h4p2.eps}\hfil
  \includegraphics[width=\tempwidth]{thesis-img/result_2_h4p3.eps}\hfil
  \includegraphics[width=\tempwidth]{thesis-img/result_2_h4p4.eps} \\
  %%\hspace{\baselineskip}
  \makebox[\tempwidth][c]{(a) $k=1$} \hfil
  \makebox[\tempwidth][c]{(b) $k=2$} \hfil
  \makebox[\tempwidth][c]{(c) $k=3$} \\
  \caption{Computed Mach contours on the $x_3=0$ symmetry plane for
    the sphere problem on the $1$M control volume mesh.}
  \label{fig:c5_2_mach}
\end{figure}


%% -------------------------------------------------------
%% 3- Turbulent, Flat plate
%% -------------------------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------
% FLAT PLATE                    -------------------------
% -------------------------------------------------------
% -------------------------------------------------------

\subsection{Turbulent Flow Over a Flat Plate}
\label{ch:result_flatplate}

%% PROBLEM, GEOMETRY
For this test case, we consider a three-dimensional extension of the
flat plate verification case from the NASA TMR website. The
computational domain is $\Omega=[-0.33, 2]\times[0,1]\times[0,1]$, the
nondimensional parameters are $\Reyn=5 \times 10^6$, $\Mach=0.2$, and
the flow is in the $x_3$ direction. The flat plate is located on the
${ (0 \leq x_1 \leq 2) \wedge (x_2 = 0)}$ boundary, where adiabatic
solid wall conditions are imposed. Symmetry boundary conditions are
imposed on the $(x_3=0)$, $(x_3=1)$, and
${ (-0.33 \leq x_1 \leq 0) \wedge (x_2 = 0) }$ boundaries, while other
boundaries are considered as farfield. As symmetry boundary conditions
are imposed on all the boundaries normal to the $x_3$-axis, the exact
solution of this problem must be constant in the $x_3$
direction. Therefore, the solutions obtained in this section can be
compared to the two-dimensional results from the NASA TMR website,
which are obtained by the CFL3D solver \cite{nasacfl3d} on a
$544 \times 384$ grid.

% MESHES, INITIAL COND, PRECON
The two-dimensional meshes provided by the NASA TMR website are
extruded in the $x_3$ direction to construct a series of nested
three-dimensional grids with dimensions: $60\times34\times7$,
$120\times68\times14$, and $240\times136\times28$. The coarsest mesh
is depicted in \autoref{fig:c5_3_mesh}. An anisotropic mesh is
necessary to correctly capture the flow pattern in the boundary layer
and at the plate leading edge. On each mesh, the problem is solved for
$k=1$, then for $k=2$, and finally for $k=3$. The free-stream
conditions are used as the initial conditions for $k=1$, while the
initial solution for each $k\geq 2$ is taken from the converged
solution of the $(k-1)$-exact scheme. The GMRES-LO-ILU2 method is used
as the preconditioner, with 10 inner GMRES iterations, and the RCM
reordering algorithm.  Moreover, the ILU factorization is performed
only on the diagonal block of each processor. Other solver parameters
are the same as \autoref{ch:result_sphere}.

\begin{figure}
  \centering
  \includegraphics[width=0.3\linewidth]{thesis-img/result_3_mesh.eps}
  \caption{Coarsest mesh for the flat plate problem.}
  \label{fig:c5_3_mesh}
\end{figure}

% TALK ABOUT THE NUT PLOTS
To schematically demonstrate the correctness of the solution, the
distribution of the turbulence working variable obtained from $k=3$ on
the finest mesh is plotted on the $x_3 = 0.5$ plane, and shown in
\autoref{fig:flatplate_nut}a.  The results for a $544 \times 384$ grid
provided by the NASA TMR are also shown in
\autoref{fig:flatplate_nut}b. There is a close agreement between the
reference solution of NASA TMR and the solution of this work,
although a few undershoots of small magnitude are present in the
latter.
\begin{figure}
  \centering
  \setlength{\tempwidth}{0.45\linewidth}
  \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{thesis-img/result_3_fun3d_nut.png}}
  \includegraphics[height=\tempheight]{thesis-img/result_3_anslib_nut.png} \hfil
  \includegraphics[height=\tempheight]{thesis-img/result_3_fun3d_nut.png} \\
  \columnname{ (a) $3$-exact: $\min:7\times10^{-3}$, $\max:379$ } \hfil
  \columnname{ (b) NASA TMR: $\min:0$, $\max:378$ } \\
  \caption{Distribution of the turbulence working variable on the
    plane $x_3=0.5$ for the flat plate problem}
  \label{fig:flatplate_nut}
\end{figure}
The next quantity of interest is the distribution of the
nondimensional eddy viscosity $\frac{\mu_T}{\mu}$ on the line
$(x_1=0.97)\wedge(x_3=0.5)$, which is depicted in
\autoref{fig:c5_3_mut}, along with the reference solution from the
NASA TMR website. An acceptable agreement is observable between our
computed results and the reference values. Also, it is evident that a
higher-order reconstruction scheme leads to a more accurate estimate
of the eddy viscosity.
\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{thesis-img/result_3_mut_3.eps}
  \caption{Distribution of the nondimensional eddy viscosity on the
    line $(x_1=0.97)\wedge(x_3=0.5)$ for the flat plate problem on the
    $240\times 136\times28$ grid}
  \label{fig:c5_3_mut}
\end{figure}

% TALK ABOUT C_D AND C_F
To further verify the numerical solution, the convergence of the drag
coefficient $C_D$ and the skin friction coefficient $C_f$ at the point
$\vvec x = (0.97,0,0.5)$ are studied with mesh
refinement. \autoref{tab:c5_3_cpf} shows the computed values on
different meshes.  The computed values converge faster for a
higher-order reconstruction scheme, such that only $k=3$ offers an
accurate solution on the medium mesh.  The reference values from the
NASA TMR library are also shown in the same table.  As desired, there
is a good agreement between the computed values using the method of
this work and the reference values of the NASA TMR website. The
convergence orders of $C_D$ and $C_f$ are computed using the procedure
of Celik \etal{} \cite{celik2008}, and are also shown in
\autoref{tab:c5_3_cpf}.  All the reconstruction schemes have attained
optimal convergence rates. Nevertheless, care must be taken when
interpreting the estimated convergence rates because the procedure of
Celik \etal{} is most reliable when the error has
already achieved an asymptotic behavior on the coarsest mesh, which
might not necessarily be the case here.

% is accurate enough on
% the finest mesh. The $240\times 136\times28$ mesh seems to be fine
% enough for the $k=2$ and $3$ schemes, as they have both resulted in
% the same values for $C_D$ and $C_f$. The $k=1$ solution, however,
% seems to be in need of another level of mesh refinement. Thus, the
% computed convergence order of $k=1$ is less reliable than that of the
% other two schemes.

\begin{table}
  \centering
  \caption{Computed value and convergence order of the drag coefficient
    and the skin friction coefficient at the point
    $\vvec x =(0.97,0,0.5)$ for the flat plate problem}
  \label{tab:c5_3_cpf}
  \begin{tabular}{|c|ccc|ccc|}
    \cline{2-7}
    \multicolumn{1}{c|}{} &\multicolumn{3}{c|}{$C_D$} &\multicolumn{3}{c|}{$C_f$} \\
    \cline{2-7}
    %%%%%%%%%%
    \hline
    NASA TMR    &\multicolumn{3}{c|}{$0.00286$} &\multicolumn{3}{c|}{$0.00271$} \\
    \hline
    %%%%%%%%%%% 
    \hline
    \diagbox{Mesh}{$k$} &$1$ &$2$ &$3$ &$1$ &$2$ &$3$ \\
    \hline
    $60\times34\times7$  &$0.00396$ &$0.00233$ &$0.00233$
                             &$0.00350$ &$0.00228$ &$0.00222$ \\
    $120\times68\times14$  &$0.00301$ &$0.00281$ &$0.00285$
                             &$0.00283$ &$0.00268$ &$0.00271$ \\
    $240\times136\times28$  &$0.00287$ &$0.00286$ &$0.00286$
                             &$0.00274$ &$0.00273$ &$0.00273$ \\
    \hline
    %%%%%%%%%%%%
    \hline
    Convergence order    &$2.8$ &$3.3$ &$5.4$
                             &$3$ &$3$ &$5.1$ \\
    \hline
  \end{tabular}
\end{table} 

% TALK ABOUT THE CONVERGENCE
The norm of the residual vector per PTC iteration is shown in
\autoref{fig:c5_3_PTC}. Convergence is not affected by the
reconstruction order $k$, but is slightly degraded as the number of
degrees of freedom increases.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.4\linewidth]{thesis-img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{thesis-img/result_3_ptc.eps}
  \caption{Norm of the residual vector per PTC iteration for the flat
    plate problem}
  \label{fig:c5_3_PTC}
\end{figure}
% TALK ABOUT RESOURCE CONSUMPTION, SAME PARAGRAPH
The number of iterations and the resource consumption of the flow
solver are listed in \autoref{tab:c5_3_perf}. The majority of the
total time is spent on linear solves, and memory consumption increases
linearly with mesh refinement. Most importantly, the benefit of
higher-order methods can be observed: While the $k=3$ solution on the
medium mesh offers the same level of accuracy as the $k=1$ solution on
the fine mesh, the computational time of the former is smaller than
that of the latter by a factor of four.

\begin{table}
  \centering
  \caption{ Number of iterations and the resource consumption of the
    flow solver for the flat plate problem}
  \label{tab:c5_3_perf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$60\times34\times7$ mesh} \\
    \hline
    $1$  &$26$   &$844$      &$0.42$     &$31$   &$55$    \\
    $2$  &$26$   &$1,009$     &$1.35$     &$42$   &$124$   \\
    $3$  &$26$   &$1,071$     &$2.10$     &$57$   &$202$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$120\times68\times14$ mesh} \\
    \hline
    $1$  &$28$   &$1,436$   &$5.24$      &$510$   &$713$     \\
    $2$  &$29$   &$1,864$   &$8.30$      &$742$   &$1,489$     \\
    $3$  &$29$   &$2,041$   &$14.63$     &$825$   &$2,124$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$240\times136\times28$ mesh} \\
    \hline
    $1$  &$29$   &$2,492$      &$38.77$     &$6,222$   &$7,884$  \\
    $2$  &$27$   &$3,305$      &$60.70$     &$12,353$   &$18,137$  \\
    $3$  &$27$   &$2,906$      &$121.81$    &$23,141$   &$35,412$  \\
    \hline
  \end{tabular}
\end{table}



%% -------------------------------------------------------
%% 4- Turbulent, Extruded Airfoil
%% -------------------------------------------------------

\subsection{Turbulent Flow Over an Extruded Airfoil}
\label{ch:result_naca_extruded}

%% PROBLEM, GEOMETRY
For the last test case, we consider a three-dimensional extension of
the flow around the \nacaoolz{} airfoil. The computational domain of
\autoref{ch:result_naca} is extruded in the $x_3$ direction
with an extrusion length of one nondimensional unit. Symmetry boundary
conditions are imposed on the two boundaries normal to the $x_3$-axis,
while other boundaries retain their conditions from
\autoref{ch:result_naca}.  The nondimensional parameters are:
$\Reyn=6 \times 10^6$, $\Mach=0.15$, and the angles of attack and side
slip are equal to 10 and zero degrees, respectively. As the exact
solution must be constant in the $x_3$ direction, the solutions are
compared to the reference values provided by NASA TMR, which are
obtained by the FUN3D \cite{nasafun3d} solver on a $7169 \times 2049$
grid.


% MESHES, INITIAL COND, PRECON
Two meshes are employed: a hexahedral mesh with $\ncv = 100$K, and a
mixed prismatic-hexahedral mesh with $\ncv = 176$K. In both cases,
quadrature points are obtained from the tensor product of
one-dimensional quadrature formulas and the quadrature points of the
curved two-dimensional meshes. A portion of the meshes is depicted in
\autoref{fig:c5_4_mesh}.  In both cases, there are $7$ layers of
extruded control volumes in the $x_3$ direction.  The solver
parameters, including the order ramping procedure for solution
initialization, are the same as that of \autoref{ch:result_flatplate}.


\begin{figure}
  \centering
  \setlength{\tempwidth}{0.45\linewidth}
  \settoheight{\tempheight}{\includegraphics[width=0.45\linewidth]{thesis-img/result_4_mesh_hex.png}}
  \includegraphics[width=0.45\linewidth]{thesis-img/result_4_mesh_hex_gmsh.eps}
  \includegraphics[width=0.45\linewidth]{thesis-img/result_4_mesh_mix_gmsh.eps}\\
  \columnname{(a) Hexahedral mesh}
  \columnname{(b) Mixed prismatic-hexahedral mesh}\\
  \caption{Meshes for the extruded \nacaoolz{} problem}
  \label{fig:c5_4_mesh}
\end{figure}


%% CP PLOT
The distribution of the surface pressure coefficient on the
intersection of the airfoil and the $x_3=0.5$ plane is computed using
the $k=1$ and $3$ solutions, and depicted in
\autoref{fig:c5_4_cp}. Generally, the computed distributions are
consistent with the reference values obtained by FUN3D. Closer views
of the plots are shown for four distinct points to better compare the
results. As expected, the $k=3$ scheme predicts the most accurate
values, particularly on the mixed mesh. For example, the FUN3D results
show that the pressure coefficient on the lower surface becomes bigger
than that of the upper surface, near the trailing edge of the
airfoil. This phenomenon is captured by the $k=3$ solutions, but not
observed by those of $k=1$.

\begin{figure}
  \centering
  \includegraphics[width=0.75\linewidth]{thesis-img/result_4_cp_aiaa.eps}
  \caption{Distribution of surface pressure coefficient on the
    intersection of the extruded \nacaoolz{} airfoil and the $x_3=0.5$
    plane.}
  \label{fig:c5_4_cp}
\end{figure}


%% CP and CF values
The computed and the reference lift and drag coefficients are shown in
\autoref{tab:c5_cpf}. It seems that the hexahedral mesh is too coarse,
as none of the schemes can obtain accurate enough solutions. For the
mixed mesh, the $k=3$ scheme gives satisfactory results with less than
$10\%$ error for all the coefficients. The solution of the other
schemes, however, is quite off, particularly for the pressure drag
coefficient $C_{Dp}$. Note that the reference results are obtained
using a $7169\times 2049$ mesh, which is more than a thousand times
finer than the meshes employed in this work.

\begin{table}
  \centering
  \caption{Computed drag and lift coefficients for the extruded
    \nacaoolz{} airfoil problem}
  \label{tab:c5_cpf}
  \begin{tabular}{*{4}{c}}
    \hline
    $k$    &$C_{Dp}$ &$C_{Dv}$ &$C_L$ \\
    \hline
    \multicolumn{4}{c}{NASA TMR} \\
    \hline
    $-$      &$0.00607$  &$0.00621$ &$1.0910$      \\
    \hline
    \multicolumn{4}{c}{Hex mesh} \\
    \hline
    $1$     &$0.01703$  &$0.00582$ &$1.0619$   \\
    $2$     &$0.01702$  &$0.00497$ &$1.0507$   \\
    $3$     &$0.00301$  &$0.00472$ &$1.0417$   \\
    \hline
    \multicolumn{4}{c}{Mixed mesh} \\
    \hline
    $1$     &$0.01129$  &$0.00574$ &$1.0735$   \\
    $2$     &$0.00365$  &$0.00565$ &$1.0776$   \\
    $3$     &$0.00550$  &$0.00536$ &$1.0869$   \\
    \hline
    % \multicolumn{4}{c}{$2$-D Mixed mesh} \\
    % \hline
    % $1$     &$0.01095$  &$0.00704$  &$1.0778$  \\
    % $2$     &$0.00380$  &$0.00556$  &$1.0817$  \\
    % $3$     &$0.00545$  &$0.00583$  &$1.0951$  \\
    % \hline
  \end{tabular}
\end{table}

%% CONVERGENCE WITH PTC ITERATION
The norm of the residual vector per PTC iteration is shown in
\autoref{fig:c5_4_PTC}. Convergence is marginally affected by either
the mesh type or the reconstruction order $k$. The latter is a
desirable result of using the steady-state solution of a $k$-exact
scheme as the initial guess for the $(k+1)$-exact solution.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.4\linewidth]{thesis-img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{thesis-img/result_4_ptc.eps}
  \caption{Norm of the residual vector per PTC iteration for the
    extruded airfoil problem}
  \label{fig:c5_4_PTC}
\end{figure}


%% RESOURCE CONSUMPTION
Finally, parameters related to the iterative convergence of the solver
are listed in \autoref{tab:c5_4_perf}. The linear solve time makes up
around $30\%$ of the total computation time. The total computation
time is strongly dependent on $k$, but does not differ considerably
between the two meshes for the same reconstruction order. Moreover,
the memory consumption seems to be linearly increasing with both $k$
and $\ncv$.


\begin{table}
  \centering
  \caption{Resource consumption of the flow solver for the extruded
    \nacaoolz{} problem}
  \label{tab:c5_4_perf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)                       \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Hex mesh, $\ncv=100$K}                       \\
    \hline
    $1$  &$33$   &$1,154$      &$4.77$      &$317$      &$744$      \\
    $2$  &$31$   &$1,788$      &$6.82$      &$730$      &$2,097$    \\
    $3$  &$31$   &$2,415$      &$12.23$      &$1,057$    &$3,215$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Mixed mesh, $\ncv=176$K}                 \\
    \hline
    $1$  &$34$   &$1,132$   &$8.70$      &$458$     &$1,164$    \\
    $2$  &$32$   &$1,769$     &$10.87$   &$800$     &$2,427$    \\
    $3$  &$31$   &$2,185$     &$26.47$   &$1,311$   &$4,666$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
  \end{tabular}
\end{table}

 
%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
