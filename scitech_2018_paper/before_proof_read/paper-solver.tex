\section{Solving the Discretized System of Equations}
\label{ch:solu}

\subsection{Pseudo-Transient Continuation}
\label{ch:solu_PTC}

In this work, the Pseudo-Transient Continuation method
\cite{ceze2015} is used to find
the steady-state solution of Equation \eqnref{eq:nonlineareq}. The PTC
method starts from an initial guess, which is usually taken from the
free-stream conditions or the converged solution of a lower-order
accurate scheme. Then, the solution is updated iteratively according
to the formula:
\begin{equation}
  \label{eq:PTC}
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right)
  \delta \vvec U_h =
  -\vvec R ( \vvec U_h ), 
\end{equation}
where $\delta \vvec U_h$ is the change in the solution average values
between the current and next iterations, and $\CFL$ is the
Courant-Friedrichs-Lewy number. Moreover,
$\frac{\partial \vvec R}{\partial \vvec U_h}$ is the residual Jacobian
matrix, and is evaluated using the algorithm proposed by Michalak and
Ollivier-Gooch \cite{michalak2010}, while the diagonal matrix
$\mmat{V}$ scales the time-step for each control volume. The entries
of $\mmat{V}$ corresponding to a control volume $\kappa$ are:
\begin{equation}
  V_\kappa = \frac{\lambda_{\max,\kappa}}{h_\kappa},
\end{equation}
where $h_\kappa$ is the hydraulic diameter of $\kappa$, and
$\lambda_{\max,\kappa}$ is the maximum eigenvalue of the inviscid flux
Jacobian over all the surface quadrature points of $\kappa$.  After
$\delta \vvec U_h$ is solved for, the solution is updated according to
the line search algorithm:
\begin{equation}
  \vvec U_h \gets \vvec U_h + \omega \delta \vvec U_h,
\end{equation}
where $\omega \in (0\quad 1]$ is the line search parameter, which must
satisfy:
\begin{equation}
  \label{eq:lsearch}
  \left\| \frac{\omega \mmat V \delta \vvec U_h}{\CFL} +
  \vvec R ( \vvec U_h + \omega \delta \vvec U_h) \right\|_2
  \leq
  \kappals \| \vvec R ( \vvec U_h ) \|_2.
\end{equation}
Here, $\kappals$ controls the strictness of the line search algorithm,
and $\kappals=1.2$ performs well for both viscous and inviscid flows
\cite{ceze2015}. In addition, the vector entries corresponding to the
turbulence working variable are omitted in the evaluation of the norms
in Equation \eqnref{eq:lsearch} to enhance convergence
\cite{wallraff2015}. Finally, the CFL number is updated at each
iteration according to the value of $\omega$:
\begin{equation}
\label{eq:cflramp}
  \CFL \gets
  \begin{cases}
    1.5 \CFL    &  \omega = 1 \\
    \CFL        &   0.1 < \omega < 1 \\
    0.1 \CFL    &  \omega < 0.1 
  \end{cases}.
\end{equation}
At the beginning of the solution process, the approximate solution is
away from its steady-state value, and a small $\CFL$ number prevents
divergence by making the approximate solution follow a physical
transient path. On the other hand, when the approximate solution gets
close to its steady-state value, Equation \eqnref{eq:cflramp} will
increase the $\CFL$ number. Therefore, the effect of the term
$\frac{\mmat V}{\CFL}$ in Equation \eqnref{eq:PTC} will be reduced, and
Newton iterations would be recovered. Thus, as better solution
approximations are obtained, the convergence rate will get closer to
the optimum behavior of Newton's iterations.

\subsection{Linear Solver}
\label{ch:solu_lin}

Every PTC iteration requires the solution of the linear system
$\mmat A \vvec x = \vvec b$, where:
\begin{equation}
  \mmat A =
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right) \quad
  \vvec x = \delta \vvec U_h \quad
  \vvec b = -\vvec R ( \vvec U_h ).
\end{equation}
We use the GMRES iterative method \cite{saad2003} for this purpose
because of the successful history of GMRES in solving various
aerodynamic problems \cite{pueyo1998, luo1998, wong2008, ceze2015},
and the fact that it is readily available as a black box solver in
many numerical computation libraries, such as our library of choice:
\petsc{} \cite{balay2012}. The behavior of the GMRES method is
strongly dependent on the eigenvalue spectrum of $\mmat A$. The
smaller the eigenvalue spectrum, the fewer iterations required for
convergence. As the LHS matrices arising from a $k$-exact finite
volume discretization usually lack a nice eigenvalue distribution, the
performance of GMRES can be greatly improved by means of
preconditioning.

Right preconditioning is used in this work, where a matrix $\mmat P$
is constructed, such that the condition number of the product matrix
$\mmat{AP}$ is smaller than that of $\mmat A$. Then, the original
equation is changed to:
\begin{equation}
  \mmat{AP} \vvec y = \mmat b \quad
  \vvec x = \mmat P \vvec y,
\end{equation}
where the vector $\vvec y$ is first solved for, and then it is used to
find $\vvec x$. The preconditioning matrix can be constructed either
directly from $\mmat A$, or the LHS matrix resulting from a
lower-order discretization scheme.  The matrix from which the
preconditioner is constructed will be denoted as $\mmat A^*$. Also,
note that the GMRES solver only requires the matrix-by-vector product
of $\mmat P$, and does not need its explicit form.

The Point Gauss-Seidel (PGS) iterative solver is a strong
preconditioner for solving linear systems arising from discretization
on isotropic meshes \cite{mavriplis1999}.  Therefore, PGS is used for
the solution of Euler equations in this work, for which an isotropic
mesh correctly captures the solution. Nevertheless, resolving viscous
flow problems requires anisotropic meshes, and is a more challenging
problem. In this work, we use the incomplete lower-upper factorization
of fill level $p$ (ILU$p$) for the solution of viscous turbulent
problems. In this method, a lower triangular matrix, $\IL{}$, and an
upper triangular matrix, $\IU{}$, are constructed that have a small
nonzero structure, and satisfy $\mmat A^* \approx \IL \IU$
\cite{saad2003}. Then, the preconditioner matrix is set to
$\mmat P = (\IL{}\IU{})^{-1}$. A larger fill level results in $\IL{}$
and $\IU{}$ matrices with bigger nonzero structure, but is likely to
construct a more effective preconditioner. Since the ILU
preconditioner is based on matrix factoring, its performance is
dramatically affected by the ordering of the unknowns in the matrix
$\mmat A^*$. Quotient minimum degree (QMD) and reverse Cuthill-McKee
(RCM) are among the reordering algorithms that are offered in \petsc{}
\cite{balay2012}, and are considered in this work. The former reduces
the fill of the factored matrices, while the latter reduces the fill
of matrix $\mmat A^*$ itself. In addition, it will be demonstrated in
\autoref{ch:result_naca} that lines of strong unknown coupling can
make up an effective ILU reordering method.

Jalali and Ollivier-Gooch \cite{jalali2017} observed that a fill level
of three or larger is required for preconditioning of two-dimensional
viscous turbulent flow problems if the $\IL{}$ and $\IU{}$ matrices
are factored from the higher-order LHS matrix. While this method is a
practical preconditioner for two-dimensional problems, its memory cost
soars drastically in three dimensions, hindering its implementation
for such problems. An alternative is to factor the $\IL{}$ and $\IU{}$
matrices from the LHS matrix of the $0$-exact discretization scheme
\cite{pueyo1998,luo1998,wong2008}. Despite being conservative on the
memory, this method can behave poorly for high-order reconstruction
schemes \cite{nejat2009}. Hereinafter, the ILU preconditioner of fill
level $p$ factored from the $0$-exact and full-order LHS matrices will
be denoted as LO-ILU$p$ and HO-ILU$p$, respectively.

\subsection{Improved Preconditioning Algorithms}

To improve the poor performance of the ILU preconditioning method for
large higher-order problems, two strategies will be introduced: a
preconditioning method based on inner GMRES iterations, and an ILU
reordering method based on lines of strong unknown coupling.

\subsubsection{Inner GMRES Iterations}

Some researchers have observed that the lower-order LHS matrix can
construct a more effective preconditioner compared to its higher-order
counterpart because the structure of the lower-order LHS matrix only
includes the immediate neighbors of every control volume
\cite{wong2008, luo1998, pueyo1998}. Nevertheless, as will be shown in
\autoref{ch:result_naca}, the LO-ILU method can behave poorly for
high-order reconstruction schemes applied to viscous flow
problems. Our conjecture is that using the exact inverse of the
lower-order LHS matrix as the preconditioner:
$\mmat{P}=(\mmat{A^*})^{-1}$, rather than the product of the ILU
matrices: $\mmat{P}=(\IL\IU)^{-1}$, can mitigate the mentioned
issue. In this case, the matrix-by-vector product
$\vvec{z}=\mmat{P}\vvec{v}$ can be found by solving the system:
\begin{equation}
  \label{eq:losystem}
  \mmat{A}^* \vvec{z}=\vvec{v}.
\end{equation}
Nevertheless, the linear system of Equation~\eqref{eq:losystem} can be
as large as the original linear system $\mmat A \vvec x = \vvec b$,
and its solution cannot be carried out exactly. Instead of seeking the
exact solution, the proposal is to use further inner ILU
preconditioned GMRES iterations to find an approximate solution for
Equation~\eqref{eq:losystem}. The resulting preconditioner will be
referred to as GMRES-LO-ILU in this work, and its performance will be
examined in \autoref{ch:result_naca}. It must be noted that when a
nonlinear operator such as an inner GMRES solver is used as the
preconditioner, the outer solver has to be replaced with a Flexible
GMRES (FGMRES) solver \cite{saad1993}.

\subsubsection{Lines of Strong Coupling Between Unknowns}

Forming non-overlapping lines that contain control volumes with
strongly coupled degrees of freedom is beneficial in constructing
effective preconditioners. Mavriplis \cite{mavriplis1998} introduced
the concept of lines in anisotropic unstructured meshes to enhance the
performance of multigrid solvers via implicit smoothing. His
approximate algorithm for finding such lines was purely
geometric-based, and only considered control volume aspect
ratios. Thus, his method only captured coupling through
diffusion. Okusanya \etal{} \cite{okusanya2004} later developed an
algorithm that considered both the advection and diffusion
phenomena. Fidkowski \etal{} \cite{fidkowski2005} proved that such an
algorithm results in a set of unique lines, and used the lines for
nonlinear $p$-multigrid solution of Euler equations. Diosady and
Darmofal \cite{diosady2009} showed that reordering the unknowns, such
that members of a line have consecutive numbers, is an effective
reordering strategy for the ILU preconditioner. In this work, the line
reordering algorithm of Diosady and Darmofal is implemented to improve
the speed of the linear solver.

In the first step of the line creation algorithm, a weight is assigned
to every face inside the mesh. This weight is derived from the
Jacobian of the $0$-exact discretization of the advection-diffusion
equation,
\begin{equation}
  \nabla \cdot (\vel u - \mu_L \nabla u) = 0,
\end{equation}
where $u$ is the unknown variable, $\vel$ is the velocity taken from
the initial conditions, and $\mu_L$ is an input parameter that
controls the sensitivity of the lines to mesh anisotropy. The weight
of a face $f$ is then defined as:
\begin{equation}
  \label{eq:edge_lw}
  W(f) = \max
  \left(
    \frac{\partial R_\sigma }{\partial u_\tau},
    \frac{\partial R_\tau }{\partial u_\sigma}
  \right ),
\end{equation}
where $W$ is the weight of the face, $\sigma$ and $\tau$ are the
adjacent control volumes of the face, and $\vvec{R}$ the residual
vector. If a face is located on the boundary,
Equation~\eqref{eq:edge_lw} will not directly be applicable to
it. Thus, a mirrored ghost control volume is placed adjacent to every
boundary face. After finding the face weights, calling
\autoref{alg:line} will construct the lines, where $F(\tau)$ is the
set of faces of control volume $\tau$, and $\sigma=C(\tau, f)$ is the
control volume which has the face $f$ in common with control volume
$\tau$. This algorithm ensures that two adjacent control volumes are
connected to each other if and only if they have their first or second
highly weighted face lying between them \cite{fidkowski2005}.

\begin{algorithm}
  \caption{Line creation}
  \label{alg:line}
  \begin{algorithmic}
    \Function{CreateLines}{}
    \Comment{Calling this function creates all lines.}
    \State Unmark all control volumes.
    \While{there a exists an unmarked control volume $\tau$}
    \State \Call{MakePath}{$\tau$}
    \Comment{Forward path creation}
    \State \Call{MakePath}{$\tau$}
    \Comment{Backward path creation}
    \EndWhile
    \EndFunction
    \vspace{0.3in}
    \Function{MakePath}{$\tau$}
    \Comment{Helper function.}
    \Repeat
    % 
    \parState{ For control volume $\tau$, pick the face
      $f \in F (\tau)$ with the highest weight, such that control
      volume $ \sigma= C(\tau,f)$ is not part of the current line.}
    %
    \If{ $f$ is a boundary face}
    \State Terminate
    \ElsIf{$\sigma$ is marked}
    \State Terminate
    \ElsIf{$f$ is not the first or second ranked face of $\sigma$ in weight}
    \State Terminate
    \EndIf
    %
    \State Mark $\sigma$.
    \State Add $\sigma$ to the current line.
    \State $\tau \gets  \sigma$
    \Until{Termination}
    \EndFunction
  \end{algorithmic}
\end{algorithm}

To illustrate the performance of the algorithm, an anisotropic mesh
for the geometry of a \nacaoolz{} airfoil is considered, as shown in
\autoref{fig:naca_lines}. The velocity has been taken from the $2$-exact
solution of the flow resulting from a Mach number of $0.15$, a
Reynolds number of $6\times10^6$, and an angle of attack of
$10^\circ$. The parameter $\mu_L$ is set to the heuristic value of
$10^{-5}$. As desired, the lines follow the direction of mesh
anisotropy near wall boundaries, while their pattern changes, and
follows the flow direction in other parts of the geometry.

%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
