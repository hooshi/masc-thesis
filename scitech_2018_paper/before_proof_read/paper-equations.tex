\section{Governing Equations}
\label{ch:govern}

The compressible RANS equations can be written in conservative form:
\begin{equation}
  \label{eq:conserve}
  \frac{ \partial \vvec{u} }{ \partial t}
  + \nabla \cdot
  \left(
    \mmat{F}(\vvec u) - \mmat{Q}(\vvec u, \nabla \vvec{u}) 
  \right)
  =
  \vvec{S}(\vvec u, \nabla \vvec{u}),
\end{equation}
where $\vvec u$ is the solution vector, $\mmat F$ is the inviscid flux
matrix, $\mmat Q$ is the viscous flux matrix, and $\vvec S$ is the
source term vector. In this work, the negative Spalart-Allmaras (S-A
neg) turbulence model \cite{spalart2} is used in conjunction with the
RANS equations.

The solution vector and the flux matrices for the RANS + S-A neg
equations are given as:
\begin{equation}
  \label{eq:sa_flux}
  \vvec u = 
  \begin{bmatrix} 
    \rho       \\
    \rho \vel  \\
    E                 \\
    \rho \nut           
  \end{bmatrix} \quad
  \mmat F = 
  \begin{bmatrix} 
    \rho \vel^T                  \\
    \rho \vel \vel^T + P \mmat I \\
    (E+P) \vel^T                 \\
    \nut \rho \vel^T
  \end{bmatrix} \quad
  \mmat Q =
  \begin{bmatrix} 
    0                  \\
    \mmat \tau         \\
    (E+P) \mmat \tau \vel + \frac{R \gamma}{\gamma - 1}
    \left( \frac{\mu}{\Pran} + \frac{\mu_T}{\Pran_T} \right) \nabla T \\
    -\frac{1}{\sigma} ( \mu + \mu_T ) \nabla \nut
  \end{bmatrix},
\end{equation}
where $\rho$ is the fluid density, $\vel$ is the velocity vector,
$\nut$ is the S-A turbulence working variable, $E$ is the total
energy, $P$ is the fluid pressure, $T$ is the fluid temperature,
$(.)^T$ denotes matrix transpose, $\mmat \tau$ is the viscous stress
tensor, $\Pran$ and $\Pran_T$ are the Prandtl and turbulent Prandtl
numbers, respectively, $\mmat I$ is the identity matrix, $R$ is the
universal gas constant, and $\gamma$ is the specific heat ratio. The
emphasis is on air as the working fluid, so the values $\gamma = 1.4$,
$\Pran = 0.72$, and $\Pran_T = 0.9$ are used.  The pressure is related
to the other variables via the formula:
\begin{equation}
  P = (\gamma-1)
  \left(
    E - \frac{1}{2}\rho(\vel \cdot \vel)
  \right).
\end{equation}
Similarly, temperature is related to pressure and density in the form:
\begin{equation}
  T = \frac{\gamma P}{\rho}.
\end{equation}
In Equation \eqnref{eq:sa_flux}, $\mu$ is the viscosity, and is found
from Sutherland's law while $\mu_T$ is the eddy viscosity, and is
found from the S-A neg model \cite{spalart2}. Finally, the stress
tensor $\mmat \tau$ is found via the equation:
\begin{equation}
  \label{eq:sa_tau}
  \mmat \tau =
  2 ( \mu + \mu_T)
  \left(
    \frac{1}{2} \left( \nabla \vel + ( \nabla \vel)^T \right) -
    \frac{1}{3} \trace(\nabla \vel) \mmat I
  \right).
\end{equation}

The source term vector for the RANS + S-A neg equations is:
\begin{equation}
  \vvec S =
  \begin{bmatrix} 
    0             \\
    0             \\
    0             \\
    \Diff + \rho (\Prod-\Dist+\Trip)
  \end{bmatrix},
\end{equation}
where $\Diff$, $\Prod$, $\Dist$, and $\Trip$ represent diffusion,
production, destruction, and trip terms, respectively, and are found
from the S-A neg model \cite{spalart2}.
 
By neglecting the turbulence model equation, the viscous flux matrix,
and the source term vector in Equation \eqnref{eq:sa_flux}, the Euler
equations are recovered. The Euler equations govern the flow of
inviscid fluids, and are characterized by:
\begin{equation}
  \vvec u = 
  \begin{bmatrix} 
    \rho       \\
    \rho \vel  \\
    E               
  \end{bmatrix} \quad
  \mmat F = 
  \begin{bmatrix} 
    \rho \vel^T                  \\
    \rho \vel \vel^T + P \mmat I \\
    (E+P) \vel^T 
  \end{bmatrix} \quad
  \mmat Q = \mmat{0} \quad
  \vvec S = \vvec{0}.
\end{equation}

%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
