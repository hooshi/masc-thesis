\chapter{Three-Dimensional Mesh Processing}
\label{ch:c3_threed}

% -------------------------------------------------------
% -------------------------------------------------------
% Chapter beginning -------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------
The finite volume method requires the subdivision of the domain into a
set of control volumes, which are obtained from a background
mesh. There are two different approaches for constructing the control
volumes: cell-centered, and vertex-centered (cell-vertex). In the
cell-centered approach, every cell of the mesh is considered as a
control volume, as shown in Figure \ref{fig:c3_meshdual1}. Conversely,
the vertex-centered approach associates a control volume to each
vertex of the mesh, and is dependent on the definition of the control
volume faces. Figure \ref{fig:c3_meshdual2} shows an example of
vertex-centered control volumes, which are created by connecting the
barycenter of each triangle to the midpoints of its edges. This method
for constructing the vertex-centered control volumes is known as the
median-dual approach.

The only data that the solver requires about the geometry of the
control volumes is their adjacency and quadrature information. Thus,
relevant data structures and algorithms have to be implemented in a
numerical solver package, to offer access to such information, with
reasonable time and memory cost. Although the cell-centered and
cell-vertex methods are both straightforward to implement in two
dimensions, three-dimensional implementation of the latter is more
difficult to code, and requires more quadrature points for integration
up to a certain order of accuracy. As a result, the cell-centered
method has been chosen for the purpose of this thesis, and is
implemented in \anslib{}.  In this chapter, the different
preprocessing steps involved in undertaking such a task are discussed.

\begin{figure}
  \centering
  \begin{subfigure}[t]{0.4\textwidth}
    \includegraphics[width=.99\linewidth]{img/mesh_primal.eps}
    \caption{}
    \label{fig:c3_meshdual1}
  \end{subfigure}
  \begin{subfigure}[t]{0.4\textwidth}
    \includegraphics[width=.99\linewidth]{img/mesh_dual.eps}
    \caption{}
    \label{fig:c3_meshdual2}
  \end{subfigure}  
  \caption[Control volumes for cell-centered and cell-vertex
  methods]{Schematic of control volumes: (a) cell-centered (b)
    median-dual cell-vertex }
  \label{fig:c3_meshdual}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% SECTION: Mesh Connectivity ---------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Element Mapping and Quadrature}

Hexahedral and quadrilateral meshes are known to have superior
properties, in terms of solution accuracy in three and two dimensions,
respectively. Although structured mesh generation techniques can
create such grids, they can be expensive in terms of time resources,
since applying these algorithms to complex geometries requires a
considerable amount of human input.  Conversely, unstructured mesh
generation techniques are rather automatic, but are not able to handle
complex geometries without resorting to robust algorithms that only
create simplex cells, such as the Delaunay mesh generation methods
\cite{cheng2012}. Therefore, simulation grids are usually composed of
a mixture of different cell types. The purpose of this section is to
introduce the connectivity and quadrature formulas for the control
volumes and their faces in a computational mesh. Hereinafter, both the
control volumes and their faces will be referred to as elements.

The process starts with mapping each element $E$ to its reference
version $\hat E$, which resides in a nondimensional space.  The
mapping is a polynomial of degree $l$, that must guarantee geometric
continuity across neighboring elements. Thus, it is usually
constructed from Lagrangian basis functions (polynomials). Using the
$\hat{()}$ notation for the reference space, the mapping can be
written as:
\begin{equation}
  \label{eq:lag_int}
  \xyz(\hat \xyz) = \sum_{i=1}^\nlag \yyz_i \hat\psi_i(\hat \xyz)
  \quad \hat \xyz \in \hat E,
\end{equation}
where $\hat \psi_i$ and $\yyz_i$ are the Lagrange basis functions and
nodes, respectively, and $\nlag$ is the number of Lagrange points.
Hexahedra, prisms and tetrahedra are the most commonly used
three-dimensional elements in both finite element and finite volume
simulations. Pyramids, on the other hand, are not very popular, and
are used for making transitions between different element types in a
mixed mesh. Quadrilateral and triangular elements are also used to
represent the control volume faces. Figure \ref{fig:c3_refel} shows
the first order ($l=1$) versions of these elements in the reference
space. A well defined mesh must provide the location of every
node. Furthermore, it must store the local to global node numbering
for every element, along with its type and interpolation order.

For a reference element, a $q$th-order accurate quadrature rule,
consists of $\nqua$ point locations $\hat\zzz_i$, and weights
$\hat w_i$, such that for every polynomial $\hat P(\hat\xyz)$ of degree
less than or equal to $q-1$:
\begin{equation}
  \int_{\hat E} \hat P(\hat\xyz) d\hat E =
  \sum_{i=1}^\nqua \hat P(\hat \zzz_i) \hat w_i.
\end{equation}
These quadrature rules are tabulated for different orders and types of
elements in the literature, such as the work of Solin \etal{}
\cite{solin2003}. By a change of variables, quadrature rules can be
constructed for elements in the physical space. For a
three-dimensional element, we can write:
\begin{equation}
  \label{eq:quad_phys}
  \int_E f(\xyz) dE =
  \int_{\hat E} f (\xyz(\hat \xyz))
  \det(\mmat J(\hat\xyz)) d\hat E
  \approx
  \sum_{i=1}^\nqua
  f(\xyz(\hat\zzz_i)) \det(\mmat J(\hat\zzz_i)) \hat w_i.
\end{equation}
Here, $\mmat J =\frac{\partial\xyz}{\partial\hat\xyz}$ represents the
Jacobian of the transformation given in Equation \eqref{eq:lag_int}. A
similar statement can be made for two-dimensional elements, giving way
to the following procedure for finding quadrature rules in the
physical space:
\begin{equation}
  \begin{aligned}
    % 
    &\text{2D Element:} 
    & & w_i =
    \| \partial_{\hat x_1} \vvec x \times \partial_{\hat x_2} \vvec x \|_2
    \hat w_i
    & \zzz_i = \xyz(\hat\zzz_i),
    \\
    & \text{3D Element:}
    & & w_i = \det(\mmat J(\hat\zzz_i)) \hat w_i
    & \zzz_i = \xyz(\hat\zzz_i).
  \end{aligned}
\end{equation}
It is noteworthy to mention that the higher the interpolation order of
an element, $l$, the higher the values of $q$ are required to evaluate
an integral up to a certain order of accuracy. This is due to the
presence of the term $\det(\mmat J)$ in Equation \eqref{eq:quad_phys}
that prevents a quadrature rule to maintain its nominal order of
accuracy in the physical space. Furthermore, for integrals on the
faces that involve the normal vector, the following formula can be
used:
\begin{equation}
  \nvec n  =
  \frac{
    \partial_{\hat x_1} \vvec x \times \partial_{\hat x_2} \vvec x
  }
  {
    \| \partial_{\hat x_1} \vvec x \times \partial_{\hat x_2} \vvec x \|_2
  }.
\end{equation}
In this thesis we have used the Lagrange polynomials and reference
element quadrature rules that are provided by the libMesh finite
element library \cite{kirk2006}.

\begin{figure}
  % 
  \input{img/hex.tex} 
  \vspace{.2in}
  \input{img/prism.tex}
  \vspace{.1in}
  \input{img/pyramid.tex}
  \vspace{.25in}
  \input{img/tet.tex}
  \vspace{.35in}
  \input{img/quad.tex}
  \vspace{.3in}
  \input{img/tri.tex}
  \caption[First order reference Lagrange elements]{First order
    reference Lagrange elements and polynomials. From top to bottom:
    hexahedron, prism, pyramid, tetrahedron, quadrilateral, and
    triangle.}
  \label{fig:c3_refel}
  % 
\end{figure}

% \end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% SECTION: Creating curved anisotropic meshes -----------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Creating Curved Anisotropic Meshes}

High-order numerical discretization schemes must correctly account for
the curvature of domain boundaries to maintain their nominal order of
accuracy \cite{bassi1997}. As conventional mesh generators create only
meshes with planar faces, modification schemes are required before
such meshes can be used for a high-order numerical simulation. This
modification can be as simple as just replacing the boundary faces
with higher-order representations, as in isotropic meshes. Successful
resolution of quantities of interest in turbulent flows, however,
requires highly anisotropic meshes that have big spacing in the wall
tangent direction compared to small spacing in the wall normal
direction. Replacing only the boundary faces in this case would create
self intersecting invalid domain subdivisions. Thus, the curvature of
the boundary faces must somehow get propagated throughout the domain
to prevent mesh tangling.

For structured meshes, a simple remedy is to agglomerate a fine mesh
and use the extra points to define the higher order representation of
the cells and their faces. Although this method is greatly effective,
its limit in only working with structured meshes confines its use to
verification and benchmark problems. A more general approach is to use
a solid mechanics analogy, and model the faceted mesh as an elastic
solid. Then, the boundary of the solid can be deformed to match its
curved representation, leading to an internal deformation which is
consequently used to define the curvature of the inner faces and
cells. Hartmann and Leicht \cite{hartmann2016} gave a detailed summary
of this family of methods. They also introduced modifications that
greatly reduce the computational cost, while increasing the quality of
the final mesh produced. Jalali and Ollivier-Gooch
\cite{jalali_thesis} have developed a simplified two-dimensional
linear elasticity solver, which is currently used in \anslib{} to
generate high-order curved meshes for two-dimensional turbulent flow
simulations. In this section, their approach will be extended to work
with three-dimensional unstructured meshes.

A linear elastic solid is governed by the Navier equations, which are
derived from the conservation of linear momentum, and expressed as
\cite{lai}:
\begin{equation}
  \label{eq:navier_1}
  - \nabla \cdot \mmat \sigma = 0,
\end{equation}
where $\mmat \sigma$ is the stress tensor, and is defined as:
\begin{equation}
  \label{eq:navier_2}
\mmat \sigma = 
\frac{\mu}{2} \left(
\nabla \vvec u + \nabla \vvec u^T
\right)
-
\frac{\lambda}{3} \trace (\nabla \vvec u) \mmat I,
  % \sigma_{ij} =
  % \frac{\mu}{2}
  % (
  % \frac{\partial u_i}{\partial x_j} +
  % \frac{\partial u_j}{\partial x_i}
  % )
  % -
  % \frac{\lambda}{3} \delta_{ij}
  % \frac{\partial u_k}{\partial x_k},
\end{equation}
where $\vvec u = [u_1,u_2,u_3]^T$ is the displacement vector, and the
variables $\mu$ and $\lambda$ are the Lame's coefficients. The elastic
solid properties are usually reported in terms of the Young's modulus,
$E$, and Poisson's ratio, $\nu$, which are related to the Lame's
coefficients as:
\begin{equation}
  \label{eq:lame}
  \mu = \frac{E}{ 2 (1+\nu)}, \quad
  \lambda = \frac{\nu E}{(1+\nu)(1-2 \nu)}.
\end{equation}
On the boundaries, either the displacement, the normal force, or a
linear relation between both of them has to be specified, which
results in Dirichlet, Neumann, or Robin boundary conditions,
respectively. These conditions can all be expressed through a single
equation:
\begin{equation}
  \label{eq:bdry_1}
  \mmat{Q}(\xyz) \vvec{u} + \mmat{\sigma} \nvec{n} = \vvec{f}(\xyz)
  \quad \xyz \in \partial \Omega,
\end{equation}
where $\vvec f$ is the normal force, and $\mmat Q$ is a generalized
spring constant. In the curved mesh generation process, only the
Dirichlet boundary conditions are of interest, which can be recovered
by setting $\mmat Q=\frac{1}{\epsilon}\mmat I$ and
$\vvec f(\xyz)= \frac{1}{\epsilon} \vvec u_b(\xyz)$ in Equation
\eqref{eq:bdry_1}. Here, $\vvec u_b(\xyz)$ is the prescribed boundary
displacement, and $\epsilon$ is a small number such as $10^{-10}$.

The solution method for the Navier equations and their boundary
conditions will now be presented. To ease the notation, the Einstein
summation convention is used for indices $i$, $j$, $k$, and $l$, which
are reserved only for the geometric dimensions. In the first solution
step, Equations \eqref{eq:navier_1} and \eqref{eq:navier_2} are
combined to yield:
\begin{equation}
  \label{eq:navier_3}
  - \frac{ \partial}{\partial x_k}
  \left(
    K_{ijkl}
    \frac{ \partial u_j}{\partial x_l}
  \right)
  = 0
  \qquad i=1 \cdots \ndim.
\end{equation}
where $K_{ijkl}$ are the components of the fourth-order stiffness
tensor:
\begin{equation}
  K_{ijkl} = \lambda \delta_{ik} \delta_{jl} + 
  \mu ( \delta_{ij}\delta_{kl} + \delta_{il}\delta_{jk} ).
\end{equation} 
The next step involves the use of the continuous Galerkin finite
element method to solve the governing equations. The numerical
solution $\vvec u_h(\xyz)$ will be defined as the linear superposition
of a set of basis functions,
\begin{equation}
  \vvec u_h(\xyz) = \sum_{r=1}^{\ndof/\ndim} \vvec U_{h,r} \psi_r(\xyz),
\end{equation} 
where $\psi_r$ is the $r$th finite element basis function. Note that
the finite element basis functions are not necessarily the same as
Lagrange polynomials that are used to represent the curved geometry in
Equation \eqref{eq:lag_int}. Moreover, $\vvec U_{h,r}$ is the $r$th
sub-component of the vector of degrees of freedom, $\vvec U_h$, and is
expressed as:
\begin{equation}
  \vvec U_{h,r}=[(U_{h,r})_1, (U_{h,r})_2, (U_{h,r})_3]^T.
\end{equation}
Subsequently, $\vvec u$ is replaced with $\vvec u_h$ in Equations
\eqref{eq:navier_3}, and the result is multiplied by every basis
function $\psi_r$, which when integrated over the whole domain gives:
\begin{equation*}
  -\int_\Omega
  \frac{ \partial}{\partial x_k}
  \left(
    K_{ijkl}
    \frac{ \partial u_{h,j}}{\partial x_l}
  \right)
  \psi_r
  d\Omega
  = 0
  \quad i=1 \cdots \ndim \quad r=1 \cdots \ndof.
\end{equation*}
Then, using the integration by parts theorem and Equation
\eqref{eq:navier_2}, we arrive at the discretized weak form:
\begin{equation}
  \label{eq:elfem}
  \sum_{s=1}^\ndof 
  \left( 
    \int_\Omega
    \frac{\partial \psi_r }{\partial x_k}
    K_{ijkl}
    \frac{\partial \psi_s }{\partial x_l} d \Omega
    +
    \int_{\partial \Omega} \psi_r Q_{ij}  \psi_s dS
  \right) (U_{h,s})_{j}  = 
  \int_{\partial \Omega}  f_i \psi_r dS,
\end{equation}
for every $i \leq \ndim$ and $r \leq \ndof$, which is a linear system
of equations for finding $\vvec U_h$.

The described solution scheme is implemented using the libMesh finite
element library \cite{kirk2006} in this thesis. The hierarchical
polynomials \cite{flaherty} of order $p$ are chosen as the basis
functions, where $p$ is an input parameter. LibMesh supports
quadrature rules of arbitrary orders of accuracy for assembling the
discretized system of equations. Therefore, a quadrature scheme
accurate up to order $(2p + 1)$ is used, which ensures exact
evaluation of all integrals, so that we do not have to worry about the
effect of quadrature error on the final solution.


To illustrate the performance of the implemented method, a model
problem is presented, with a simple geometry resembling a
three-dimensional airfoil. This geometry along with its anisotropic
mesh are shown in Figure \ref{fig:c3_bumpgeo}. The mesh is constituted
of $1288$ hexahedra. The curved part of the boundary is analytically
parameterized as:
\begin{equation}
  \begin{aligned}
    \label{eq:manuair}
    & \vvec{y}(u,v) =
    \left[ u+(R_0 - bu)\sin(v),
    cu,
    (R_0 - bu)\cos(v) - H_0 \left( 1-\frac{bu}{R_0} \right)
    \right]^T \\
    % 
    & 0<u<1.25 \quad -\frac{\pi}{6}<v<\frac{\pi}{6} \\
    % 
    & R_0=1 \quad H_0=\cos(\pi/6) \quad
    b=2 \quad c=4,
  \end{aligned}
\end{equation}
where $u$ and $v$ are parameterization variables, and $\vvec y$ is a
point on the surface. To correctly capture the curvature of the
boundary, we need to project every point $\vvec x$ on the boundary
of the faceted mesh to the surface of the original geometry. Then, the
boundary condition simply becomes that of Dirichlet with
$\vvec u_b(\xyz) = \Proj(\vvec x)-\vvec x$. The projection operator
would be identity on the planar parts of the boundary. On the curved
part, however, we seek to find the point $\Proj(\xyz)=\vvec y(u,v)$,
such that the line $\vvec x \vvec y$ is perpendicular to the surface,
\ie{} we seek the unknowns $[u,v,d]^T$ that satisfy:
\begin{equation}
  \vvec x - \vvec y(u,v) - d\nvec n(u,v) = 0,
\end{equation}
where $d$ is the distance between the points $\vvec x$ and $\vvec y$,
and $\nvec n$ is the surface normal vector,
\begin{equation}
  \nvec{n}(u,v) =
  \frac{
    \partial_u \vvec{y} \times \partial_v \vvec{y}
  }{
    \| \partial_u \vvec{y}  \times \partial_v \vvec{y} \|_2
  }.
\end{equation}
The projection problem can be solved using the Newton's method,
globalized by line search, which fully defines the boundary
conditions. Finally, the values $E=1$ and $\nu=0.3$ have been used in
the Navier equations. Note that due to the use of only Dirichlet
boundary conditions, the value of $E$ cancels out and
is insignificant as long as it is uniform throughout the domain. However,
changing $\nu$ in the admissible range $(0, 0.5)$ will result in
slightly different displacement fields.

To solve the presented model problem, we have used basis functions of
orders $p=2,3$, and a Conjugate Gradient (CG) solver, to set up and
solve Equation \eqref{eq:elfem}, respectively. The problem size and
solution time are listed in Table \ref{tab:bump}. Due to the nature of
higher-order finite element methods, it is not surprising that the
number of \dof{} and computational time have increased substantially
for the $p=3$ scheme. Figure \ref{fig:c3_bumpdis} shows the magnitude
of the displacement field obtained using quadratic hierarchical basis
functions.  As expected, the displacement field has the largest
magnitude near the curved wall, yet vanishes as one moves towards the
other planar parts of the boundary. Moreover, there is no displacement
at the vertices of the mesh, as they are already on the true curved
boundary. On the other hand, there is a big displacement in the middle
of the high aspect ratio cells that are near the curved wall, \ie{}
where the difference between the true boundary and its planar
approximation is the greatest. The residual of the linear system
\eqref{eq:elfem} per CG iteration, is shown in Figure
\ref{fig:c3_bumpres}. Convergence is rather smooth, and the residual
drops below $10^{-11}$ in $18$, and $25$ iterations for the quadratic
and cubic basis functions, respectively. The increase in number of
linear iterations for the $p=3$ scheme is due to its stiffer linear
system and higher degrees of freedom.

\begin{table}
  \center
  \caption{Performance of the linear elasticity solver}
  \label{tab:bump}
  \begin{tabular}{ c  c  c  c  c}
    \hline
    $p$ &$\ndof$ &Assembly Time(s) &Linear Solve Time(s) &Total Time(s) \\
    \hline
    2   &$36801$  &$4.1$           &$7.8$                &$12.5$        \\
    3   &$117390$ &$52.4$          &$98.0$               &$153.6$      \\
    \hline
  \end{tabular}
\end{table}

\begin{figure}
  \includegraphics[width=.6\linewidth,center]{img/bump_9.eps}
  \caption{Geometry of the curved mesh generation test case}
  \label{fig:c3_bumpgeo}
  \vspace{0.3in}
  % 
  \includegraphics[width=.4\linewidth]{img/bump_z000.png}
  \hspace{0.2in}
  \includegraphics[width=.4\linewidth]{img/bump_y025.png}
  \hspace{0.2in} 
  \includegraphics[width=.08\linewidth]{img/bump_legend.png}
  \caption[Curved mesh displacement magnitude]{Curved mesh
    displacement magnitude: The left and right figures show the $z=0$
    view and $y=0.25$ cross section, respectively.}
  \label{fig:c3_bumpdis}
  \vspace{0.3in}
  % 
  \includegraphics[width=.4\linewidth,center]{img/bump_convergence.eps}
  \caption{Residual per iteration for the linear elasticity solver}
  \label{fig:c3_bumpres}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% SECTION: Modified Basis Functions for Highly Anisotropic Meshes
% --------------------------------------------------------
% -------------------------------------------------------
\section{Modified Basis Functions for Highly Anisotropic Meshes}

If Cartesian coordinate monomials, $\phi^i(\xyz)$, are used as basis
functions for the $k$-exact reconstruction scheme, numerical
difficulties arise when dealing with anisotropic meshes. The problem
shows itself in two situations. First, the condition number of the
left hand side matrix of Equation \eqref{eq:recon_min_mat} soars
drastically, even up to $\BigO(\frac{1}{\epsilon})$, where $\epsilon$
is the machine zero. This can in turn introduce an error of order
$\BigO(1)$ in the reconstruction coefficients, and deteriorate the
accuracy of $\vvec u_h$.  Secondly, the $k$-exactness of the
reconstruction scheme is lost, \ie{} for a function $v$ and its
projection on the discrete solution space $v_h$, the equality
$\|v-v_h\|=\BigO(h^{k+1})$ might not hold anymore. In this section, we
will introduce the remedies proposed by Jalali and Ollivier-Gooch
\cite{jalali2017} to mitigate these difficulties, along with their
generalization to three dimensions.

The key is to use two new sets of reconstruction basis functions that
have better conditioning and interpolation properties for anisotropic
meshes. The first group is the set of principal coordinate basis
functions $\varphi^{+i}(\xyz)$, which are defined as:
\begin{equation}
  \label{eq:basis_princ}
  \Set{ \varphi_\tau^{+i}(\xyz) | i = 1 \ldots \nrec }
  =
  \Set{ \frac{1}{a!b!c!}
    (w_{\tau1}(\xyz))^a
    (w_{\tau2}(\xyz))^b
    (w_{\tau3}(\xyz))^c
    | a+b+c \leq k }.
\end{equation}
In this equation, $\vvec w_\tau(\xyz)$ is the principal coordinate of
control volume $\tau$ evaluated at point $\xyz$. To find this
coordinate transformation, the moment of inertia tensor $\mmat I_\tau$
has to be found,
\begin{equation}
(I_\tau)_{ij} = \int_\tau (x_i - x_{\tau i}) (x_j - x_{\tau j})
d\Omega.
\end{equation}
As this tensor is symmetric, it can be diagonalized,
$\mmat I_\tau = \mmat Q_\tau \mmat \Lambda_\tau \mmat Q_\tau^T$, where
$\mmat Q$ and $\mmat \Lambda$ are its matrix of right eigenvectors,
and diagonal matrix of eigenvalues, respectively. Then, the principal
coordinates are evaluated from:
$\vvec w_\tau(\xyz) = \mmat Q_\tau (\vvec x - \vvec x_\tau)$. Although
these basis functions span the same polynomial space as the
Cartesian monomials, $\phi^i(\xyz)$, they greatly reduce the condition
number of the left hand side matrix in Equation
\eqref{eq:recon_min_mat}. Our heuristic approach is to use these basis
functions for control volumes which have a high aspect ratio, yet
their faces are almost planar.
% A control volume $\tau$ is marked as high aspect ratio if
% $\|\mmat I_\tau\|_2 > 10$.

For control volumes that are close to wall boundaries and have high
distortion, a more complicated approach must be adopted. As a starting
point, let us assume that for every control volume $\tau$ with such
property, we somehow can find a vector function
$(d_\tau,t_{\tau1},t_{\tau2})$, such that at every point $\xyz$ in the
vicinity of the control volume, $\nabla d_\tau(\xyz)$ is perpendicular
to the closest wall boundary. Furthermore, $\nabla t_{\tau1}(\xyz)$
and $\nabla t_{\tau2}(\xyz)$ must be perpendicular to
$\nabla d_\tau(\xyz)$, and form an orthogonal basis for $\mathbb R^3$.
The vector $(d_\tau,t_{\tau1},t_{\tau1})$ is then denoted as the wall
coordinates of control volume $\tau$. In two dimensions, the wall
coordinate vector shrinks to $(d_\tau,t_\tau)$, so a rather simple
strategy can be used to find it. The normal coordinate,
$d_\tau(\xyz)$, is set to the distance to wall of point $\xyz$ minus
that of the reference point of $\tau$. For the tangential coordinate
$t_\tau$, however, a compact analytic relation does not exist. Thus,
the approximate value $\tilde t_\tau$ is used,
\begin{equation}
  \tilde t_\tau(\xyz) =
  \|
  (\xyz - \xyz_\tau) -
  ((\xyz - \xyz_\tau) \cdot \nabla d_\tau(\xyz_\tau) ) \nabla d_\tau(\xyz_\tau)
  \|_2,
\end{equation}
which is the length of the projection of $\xyz - \xyz_\tau$ onto the
perpendicular plane of $\nabla d_\tau(\xyz_\tau)$.  Figure
\ref{fig:c3_curvilin} schematically shows the construction of the
approximate wall coordinates for a sample control volume.
\begin{figure} 
  \input{img/curvilinear.tex} 
  \caption[Construction of approximate wall coordinates for a
  two-dimensional control volume] {Construction of approximate wall
    coordinates for a two-dimensional control volume $\tau$}
  \label{fig:c3_curvilin}
\end{figure}
Although it is possible to find these coordinates for every quadrature
point in the vicinity of the control volume of interest, finding their
derivatives can be complicated, or even impossible. Thus, yet another
coordinate transformation is introduced, namely the curvilinear
coordinates $\vvec \xi_\tau$, which closely approximates
$(d_\tau, \tilde t_\tau)$, while having a nice polynomial relationship
in terms of $\xyz$,
\begin{equation}
  \label{eq:curvi_coord}
  \vvec \xi_\tau =
  \sum _{i=1}^{\nrec} \vvec b_{\tau}^i \varphi_{\tau}^{+i}(\xyz),
\end{equation}
where $\varphi_{\tau}^{+i}$ are the same principal coordinate monomials
introduced in Equation \eqref{eq:basis_princ}. The coefficients
$\vvec b_{\tau}$, are subsequently found by requiring $\vvec \xi_\tau$
to be as close as possible to $(d_\tau, \tilde t_\tau)$, at the
reference point of all the control volumes in
$\Stencil(\tau)$. Mathematically, the following minimization problem
must be solved:
\begin{equation}
  \begin{aligned}
    & \underset{\vvec b_{\tau}^1 \ldots \vvec b_{\tau}^{\nrec}}{\text{minimize}}
    & & \sum_{\sigma \in \Stencil(\tau) \cup \Set{\tau}}
    (h(\xyz_\sigma)-\xi_{\tau1}(\xyz_\sigma))^2 +
    (\tilde t(\xyz_\sigma) - \xi_{\tau2}(\xyz_\sigma))^2
    \\
    & \text{subject to}
    & & \vvec \xi_\tau(\xyz_\tau) = 0,
  \end{aligned}
\end{equation}
which has an identical solution process to that of Equation
\eqref{eq:recon_min_mat}. Note that it is theoretically possible to
use the Cartesian monomials $\phi_\tau^i(\xyz)$ in the definition of
the curvilinear coordinates in Equation \eqref{eq:curvi_coord}, but
this results in a poor-conditioned least square problem in finding the
$\vvec b_\tau^i$ coefficients, and thus is avoided. In this thesis,
the emphasis is on anisotropic three-dimensional meshes that are
symmetric in the $x_3$ direction. Taking advantage of the symmetry, we
are able to define $\xi_{\tau1}$ and $\xi_{\tau2}$ using the
two-dimensional method, and simply set $\xi_{\tau3} = x_3-x_{\tau3}$.

Now that the curvilinear coordinates are introduced, their
corresponding basis functions, $\varphi_\tau^{*i}(\xyz)$, can be
defined as:
\begin{equation}
  \Set{ \varphi_\tau^{*i}(\xyz) | i = 1 \ldots \nrec }
  =
  \Set{ \frac{1}{a!b!c!}
    (\xi_{\tau1}(\xyz))^a
    (\xi_{\tau2}(\xyz))^b
    (\xi_{\tau3}(\xyz))^c
    | a+b+c \leq k }.
\end{equation}
Numerical experiments have shown that using these basis functions for
near-wall high aspect ratio elements restores the $k$-exactness of the
reconstruction scheme \cite{jalali2017}.




%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
\endinput
