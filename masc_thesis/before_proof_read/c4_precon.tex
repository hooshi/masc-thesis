\chapter{Solving the Discretized System of Equations}
\label{ch:c4_precon}


% -------------------------------------------------------
% -------------------------------------------------------
% Chapter beginning -------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------

This chapter begins by introducing the Pseudo Transient Continuation
(PTC) method that finds the solution of Equation
\eqref{eq:nonlineareq} via solving a series of systems of linear
equations (SLE). Available methods for the solution of these SLE are
introduced, and their shortcoming in handling three-dimensional FVM
problems is addressed. Finally, a new solution scheme is proposed, and
its performance is compared to other previously available methods in
\anslib{}.

% -------------------------------------------------------
% -------------------------------------------------------
% Pseudo-Transient Continuation -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Pseudo Transient Continuation}


The PTC method \cite{jalali2017, ceze2015, michalak2010, kelley1998}
starts from an initial guess for the steady-state solution of Equation
\eqref{eq:nonlineareq}. The initial guess is usually taken from the
free-stream conditions or the converged solution of a lower-order
accurate scheme. Then, the solution is updated iteratively until the
norm of the residual vector, $\|\vvec R ( \vvec U_h )\|_2$, falls
below a desired threshold.  Consider the backward Euler time advance
scheme:
\begin{equation}
  \label{eq:BEULER}
  \frac{  \vvec U_h^+ - \vvec U^{}_h }{\Delta t} +
  \vvec R ( \vvec U_h ) = 0,
\end{equation}
where $\Delta t$ is the time step size, and $\vvec U_h^+$ is the the
\dof{} vector at the next time level. Linearizing Equation
\eqref{eq:BEULER} gives:
\begin{equation}
  \label{eq:BEULERlinear}
  \left(
    \frac{\mmat I}{\Delta t} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right)
  \delta \vvec U_h =
  -\vvec R ( \vvec U_h ), 
\end{equation}
where $\delta \vvec U_h$ is the change in the \dof{} vector between
the current and next time levels. Furthermore,
$\frac{\partial \vvec R}{\partial \vvec U_h}$ is the residual Jacobian
matrix, and is evaluated using the algorithm proposed by Michalak and
Ollivier-Gooch \cite{michalak2010}.

The PTC method is derived from the backward Euler scheme by making a
few modifications. First, a nonuniform time step is used for every
control volume. Thus, Equation \eqref{eq:BEULERlinear} is changed to:
\begin{equation}
  \label{eq:PTC}
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right)
  \delta \vvec U_h =
  -\vvec R ( \vvec U_h ), 
\end{equation}
where $\CFL$ is the Courant-Friedrichs-Lewy number, and the diagonal
matrix $\mmat V$ is the time step scaling matrix. The entries of
$\mmat V$ corresponding to control volume $\tau$ are denoted as
$V_\tau$, and found from the equation:
\begin{equation}
  V_\tau = \frac{\lambda_{\max,\tau}}{h_\tau},
\end{equation}
where $h_\tau$ is the hydraulic diameter of the control volume, and
$\lambda_{\max,\tau}$ is the maximum eigenvalue of the inviscid flux
Jacobian over all the control volume surface quadrature points.
% The eigenvalues of the inviscid flux Jacobian are given in Equation
% \eqref{eq:wavespeed}.
Secondly, after $\delta \vvec U_h$ is solved for, the solution is
updated according to the line search algorithm:
\begin{equation}
  \vvec U_h \gets \vvec U_h + \omega \delta \vvec U_h,
\end{equation}
where $\omega \in (0\quad 1]$ is the line search parameter, which must
satisfy:
\begin{equation}
  \label{eq:lsearch}
  \| \frac{\omega \mmat V \delta \vvec U_h}{\CFL} +
  \vvec R ( \vvec U_h + \omega \delta \vvec U_h) \|_2
  \leq
  \kappa \| \vvec R ( \vvec U_h ) \|_2.
\end{equation}
Here, $\kappa$ controls the strictness of the line search algorithm,
and $\kappa=1.2$ performs well for both viscous and inviscid flows
\cite{ceze2015}. In addition, the vector entries corresponding to the
turbulence working variable are omitted in the evaluation of the norms
in Equation \eqref{eq:lsearch} to enhance convergence
\cite{wallraff2015}. Finally, the CFL number is updated at each
iteration according to the value of $\omega$:
\begin{equation}
\label{eq:cflramp}
  \CFL \gets
  \begin{cases}
    1.5 \CFL    &  \omega = 1 \\
    \CFL        &   0.1 < \omega < 1 \\
    0.1 \CFL    &  \omega < 0.1 
  \end{cases}.
\end{equation}
At the beginning of the solution process, the approximate solution is
away from its steady-state value, and a small $\CFL$ number prevents
divergence by making the approximate solution follow a physical
transient path. On the other hand, when the approximate solution gets
close to its steady-state value, Equation \eqref{eq:cflramp} will
increase the $\CFL$ number. Therefore, the effect of the term
$\frac{\mmat V}{\CFL}$ in Equation \eqref{eq:PTC} will be reduced, and
Newton iterations would be recovered. Thus, as better solution
approximations are obtained, the convergence rate will get closer to
the optimum behavior of Newton's iterations.

% -------------------------------------------------------
% -------------------------------------------------------
% GMES Method                   -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Linear Solvers}

Every PTC iteration requires the solution of the SLE
$\mmat A \vvec x = \vvec b$, where:
\begin{equation}
  \mmat A =
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right) \quad
  \vvec x = \delta \vvec U_h \quad
  \vvec b = -\vvec R ( \vvec U_h ).
\end{equation}
The exact solution of the SLE is carried out by combinatorial
algorithms that typically calculate a lower-upper (LU) factorization
of the LHS matrix. Although there has been great progress in the
development of such methods, \eg{} MUMPS linear solver library
\cite{amestoy1998}, they still suffer from huge consumption of memory
and time resources, when applied to sufficiently large
problems. Furthermore, these methods do not take advantage of any
initial guess for the solution, nor the fact that only an approximate
solution is of interest. As a result, scientists in the CFD community
have resorted to the iterative methods for the solution of huge SLE.

Iterative methods start from an initial guess $\vvec x^{(0)}$, and
then generate a sequence of improving approximate solutions.
Iterative methods are either categorized as stationary, or a member of
the Krylov Subspace (KSP) family. In a stationary method the
approximate solution at step $k+1$, $\vvec x^{(k+1)}$, is only
dependent on the residual vector of the previous iteration,
$\vvec r^{(k)}$, which is defined as:
\begin{equation}
  \vvec r^{(k)} =  \vvec b - \mmat A \vvec x^{(k)}.
\end{equation}
Conversely, a KSP method updates the approximate solution based on all
or some of the previous residual vectors. \anslib{} uses the KSP
method of GMRES \cite{saad2003} as its linear solver because of its
successful history in solving aerodynamic problems \cite{pueyo1998,
  luo1998, wong2008, nejat2008, ceze2015}, and the fact that it is
readily available as a black box solver in many numerical compuation
libraries, such as \petsc{} \cite{balay2012}.

The GMRES method iteratively constructs an orthogonal basis for the
search space spanned by the vectors $\vvec r^{(0)}$,
$\mmat A \vvec r^{(0)}$, \ldots, $\mmat A^{k-1} \vvec r^{(0)}$. Then,
it seeks the next approximate solution vector, $\vvec x^{(k)}$, as the
member of the search space that minimizes $\| \vvec r^{(k)} \|_2$.
Furthermore, the search space is usually restarted after a certain
number of iterations to prevent it from becoming too large. The
behavior of the GMRES method is strongly dependent on the eigenvalue
structure of matrix $\mmat A$. The smaller the eigenvalue spectrum,
the fewer iterations required for convergence. As the LHS matrices
arising from a $k$-exact FVM discretization usually lack a nice
eigenvalue distribution, the performance of GMRES must be improved by
means of preconditioning, which is introduced in the following
section.

% -------------------------------------------------------
% -------------------------------------------------------
% Preconditioning               -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Preconditioning}

Preconditioning is the transformation of the original linear system
into one which has the same solution, but is easier to solve using
iterative methods. Preconditioning is usually done by first finding a
matrix $\mmat P$, such that the condition number of the product matrix
$\mmat{AP}$ is smaller than that of $\mmat A$. Then, the original
equation is changed to:
% \begin{equation}
%   \mmat{AP} \vvec y = \mmat b \quad
%   \vvec x = \mmat P \vvec y,
% \end{equation}
where the vector $\vvec y$ is first solved for, and then it is used to
find $\vvec x$. The preconditioning matrix can be constructed either
directly from $\mmat A$, or the LHS matrix resulting from a
lower-order discretization scheme. The matrix from which the
preconditioner is constructed will be denoted as $\mmat A^*$.
Furthermore, note that the GMRES solver only requires the
matrix-by-vector product of $\mmat P$, and does not need its explicit
form.

In this section, the preconditioners available in \anslib{}, namely
Point Gauss-Seidel, Block Jacobi, and ILU will be introduced, and their
shortcoming in solving large three-dimensional problems will be
discussed.  More complicated preconditioners, such as multigrid and
domain decomposition have not been considered in this thesis because
of the inconsistent results available in the literature regarding
their performance. For example, Shahbazi \etal{} \cite{shahbazi2009}
reported that the linear multigrid method can be ten times faster than
conventional single grid algorithms. On the other hand, Diosady and
Darmofal \cite{diosady2009}, and Wallraff \etal{} \cite{wallraff2015}
reported cases for which they were not able to achieve a significant
speedup factor.


% ----------------------------------------------------------------------
% SECTION: Jacobi Preconditioner
% ----------------------------------------------------------------------
\subsection{Point Guass-Seidel}

The Point Gauss-Seidel (PGS) method is a stationary iterative solver
that can also be used as a preconditioner for the GMRES method. First,
$\mmat A^*$ is decomposed as:
\begin{equation}
  \mmat A^* = \mmat L + \mmat D + \mmat U,
\end{equation}
where $\mmat D$ is the control volume diagonal part of $\mmat A$,
$\mmat L$ is the strict lower part, and $\mmat U$ is the strict upper
part. Then, the matrix-by-vector product $\mmat P \vvec v$ of the PGS
preconditioner is defined as:
\begin{equation}
  \begin{aligned}
  &\vvec z^{(0)} = 0 \\
  &\vvec z^{(1)} =
  (\mmat D + \mmat L)^{-1}(\vvec v - \mmat A^* \vvec z^{(0)}) \\
  & \vdots \\
  &\vvec z^{(n)} =
  (\mmat D + \mmat L)^{-1}(\vvec v - \mmat A^* \vvec z^{(n-1)}) \\
  & \mmat P \vvec v = \vvec z^{(n)},
  \end{aligned}
\end{equation}
where $n$ is the number of PGS iterations. Although PGS is a strong
preconditioner for solving linear systems arising from discretization
on isotropic meshes, it performs poorly when dealing with anisotropic
meshes \cite{mavriplis1999}.  Therefore, PGS is only used for the
solution of Poisson's and Euler equations in this thesis, for which an
isotropic mesh correctly captures the solution.

% ----------------------------------------------------------------------
% SECTION: ILU Preconditioner
% ----------------------------------------------------------------------
\subsection{Block Jacobi}

The Block Jacobi (BJ) preconditioner also uses a decomposition of
matrix $\mmat A^*$, with the difference that the diagonal blocks of
matrix $\mmat D$ must be composed of all the degrees of freedom
belonging to the same processor. \autoref{fig:c4_bjacobi}a shows a
partitioned mesh, and \autoref{fig:c4_bjacobi}b shows the
corresponding structure and decomposition of the LHS matrix.
\begin{figure}
  \centering
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/naca0012_trunc_proc.eps}
  \end{subfigure}
  % PROC
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.7\linewidth]{img/naca0012_trunc_proc-figure0.eps}
  \end{subfigure} \\
  (a) \hspace{0.23\linewidth} \hfil (b)
  % 
  \caption[Schematic of Block Jacobi decomposition]{Schematic of Block
    Jacobi decomposition: (a) The mesh, and the partition belonging to
    each processor (b) The lower, upper, and block diagonal matrices }
  \label{fig:c4_bjacobi}
\end{figure}
The matrix-by-vector product for this preconditioner is denoted as:
\begin{equation}
  \begin{aligned}
    &\vvec z^{(0)} = 0 \\
    &\mmat D \vvec z^{(1)} =
    \vvec v - \mmat A^* \vvec z^{(0)} \\
    & \vdots \\
    &\mmat D \vvec z^{(n)} =
    \vvec v - \mmat A^* \vvec z^{(n-1)} \\
    & \mmat P \vvec v = \vvec z^{(n)},
  \end{aligned}
\end{equation}
where $n$ is the number of BJ iterations. Note that the block diagonal
matrix $\mmat D$ cannot be inverted exactly anymore, and each
intermediate vector $\vvec z^{(k)}$ must be solved for by using an
inner iterative solver. Nevertheless, since every block of $\mmat D$
only belongs to a single processor, the inner iterative solver can be
serial. Thus, BJ is a means of parallelizing serial preconditioners,
and is used for this purpose in \anslib{}.

% ----------------------------------------------------------------------
% SECTION: ILU Preconditioner
% ----------------------------------------------------------------------
\subsection{ILU}

The incomplete lower-upper factorization with fill level $p$ (ILU$p$)
produces an approximation of the LU factorization of $\mmat A^*$, such
that the approximate matrices have a much smaller nonzero structure
than the exact LU factorization. The factored matrices are computed by
performing Gaussian elimination on $\mmat{A}^*$, but ignoring certain
matrix entries. Larger fill levels result in factored matrices with
bigger nonzero structure, but are likely to construct more effective
preconditioners \cite{saad2003}. Nevertheless, the ILU method is
inherently a serial algorithm, so it is implemented in conjunction with
the BJ method for parallel simulations.

Since the ILU preconditioner is based on matrix factoring, its
performance is dramatically affected by the ordering of \dof s in the
matrix $\mmat A^*$. Quotient minimum degree (QMD) and reverse
Cuthill-McKee (RCM) are among the reordering algorithms that are
offered in \petsc{} \cite{balay2012}, and are used in \anslib{}. The
former reduces the fill of the factored matrices, while the latter
reduces the fill of matrix $\mmat A^*$ itself.

In the course of development of \anslib{}, Nejat and Ollivier-Gooch
\cite{nejat2008} used an ILU preconditioner factored from the LHS
matrix of the $0$-exact scheme to solve the Euler equations. They
showed the good performance of their preconditioning method for 1- and
2-exact FVM schemes, but concluded that the 3-exact scheme requires a
more effective preconditioner. Later, Michalak and Ollivier-Gooch
\cite{michalak2010} demonstrated that incomplete factorization of the
higher-order LHS matrix results in faster convergence compared to
factoring the $0$-exact LHS matrix for inviscid problems. They further
conjectured that factoring the LHS matrix to the full order using a
fill level of $3$ has a feasible memory consumption, and can be a
practical preconditioner for three-dimensional flow
problems. Hereinafter, the ILU preconditioner of fill level $p$
factored from the $0$-exact and full-order LHS matrices will be
denoted as LO-ILU$p$ and HO-ILU$p$, respectively.

Viscous flow problems are more challenging than their inviscid
counterparts. Jalali and Ollivier-Gooch \cite{jalali2017} observed
that a fill level of three or larger is required for HO-ILU
preconditioning of viscous turbulent flow problems. The HO-ILU$3$
method is a practical preconditioner for two-dimensional problems, but
its memory cost sours drastically in three-dimensions, hindering its
implementation for such problems. To mitigate this issue, a new ILU
based algorithm will be introduced in the next section that not only
has a less memory consumption than HO-ILU$3$, but also does not suffer
from poor performance of LO-ILU when used with the $3$-exact
reconstruction scheme.

% -------------------------------------------------------
% -------------------------------------------------------
% Proposed Algorithm            -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Improved Preconditioning Algorithms}

In this section, an effective preconditioning algorithm is proposed
based on inner GMRES iterations. Furthermore, the lines of strong
unknown coupling are presented for reordering of the LHS matrix. As it
will be shown in \autoref{ch:c4_cmp}, this reordering method can
improve the speed of the solver considerably.

% ----------------------------------------------------------------------
% SECTION: Inner GMRES Iterations
% ----------------------------------------------------------------------
\subsection{Inner GMRES Iterations}

Some researchers have observed that the lower-order LHS matrix can
construct a more effective preconditioner compared to its higher-order
counterpart because the structure of the lower-order LHS matrix only
includes the immediate neighbors of every control volume
\cite{wong2008, luo1998, pueyo1998}. Nevertheless, as it will be shown
in \autoref{ch:c4_cmp}, the LO-ILU method can behave poorly for
high-order reconstruction schemes applied to viscous flow problems. To
overcome this difficulty, the use of inner GMRES iterations is
proposed, where the resulting preconditioner is denoted as
GMRES-LO-ILU. In this preconditioner, the input matrix $\mmat A^*$ is
still the LHS arising from a $0$-exact discretization scheme, but the
matrix-by-vector product is defined as:
\begin{itemize}
\item[-] Given vector $\vvec v$,
\item[-] Solve $\mmat A^* \vvec z = \vvec v$ using the GMRES method.
\item[-] Precondition the inner GMRES solver using the ILU method
  factored from $\mmat A^*$.
\item[-] Stop the inner GMRES method after $n_i$ iterations.
\item[-] Set $\mmat P \vvec v = \vvec z$.
\end{itemize}
The inner iterations are inspired by the use of the exact inverse of
the lower-order LHS matrix as the preconditioner
\cite{nejat2009}. Since exact inversion is a memory consuming process,
the inner GMRES iterations are proposed as an effective
alternative. Note that since GMRES-LO-ILU includes nonlinear
operations, it must be used with a Flexible GMRES (FGMRES) outer
linear solver \cite{saad1993}.

%----------------------------------------------------------------------
% SECTION: Lines of Strong Unknown Coupling
% ----------------------------------------------------------------------
\subsection{Lines of Strong Unknown Coupling}
\label{c4:lines}

Forming non-overlapping lines that contain control volumes with
strongly coupled degrees of freedom is beneficial in constructing
effective preconditioners. Mavriplis \cite{mavriplis1998} introduced
the concept of lines in anisotropic unstructured meshes to enhance the
performance of multigrid solvers via implicit smoothing. His
approximate algorithm for finding such lines was purely
geometric-based, and only considered control volume aspect
ratios. Thus, his method only captured coupling through
diffusion. Okusanya \etal{} \cite{okusanya2004} later developed an
algorithm that considered both the advection and diffusion
phenomena. Fidkowski \etal{} \cite{fidkowski2005} proved that such an
algorithm results in a set of unique lines, and used the lines for
nonlinear $p$-multigrid solution of Euler equations. Diosady and
Darmofal \cite{diosady2009} showed that reordering the unknowns, such
that members of a line have consecutive numbers, is an effective
reordering strategy for the ILU preconditioner. In this thesis, the
line reordering algorithm of \cite{diosady2009} is implemented to
improve the speed of the linear solver.

In the first step of the line creation algorithm, a weight is assigned
to every face inside the mesh. This weight is derived from the
Jacobian of the $0$-exact discretization of the advection-diffusion
equation,
\begin{equation}
  \nabla \cdot (\vel u - \mu_L \nabla u) = 0,
\end{equation}
where $u$ is the unknown variable, $\vel$ is the velocity taken from
the initial conditions, and $\mu_L$ is an input parameter that
controls the sensitivity of the lines to mesh anisotropy. The weight
of a face $f$ is then defined as:
\begin{equation}
  \label{eq:edge_lw}
  W(f) = \max
  \left(
    \frac{\partial R_\sigma }{\partial u_\tau},
    \frac{\partial R_\tau }{\partial u_\sigma}
  \right ),
\end{equation}
where $W$ is the weight of the face, $\sigma$ and $\tau$ are the
adjacent control volumes of the face, and $\vvec{R}$ the residual
vector. If a face is located on the boundary, Equation
\eqref{eq:edge_lw} will not directly be applicable to it. Thus, a
mirrored ghost control volume is placed adjacent to every boundary
face. After finding the face weights, calling \autoref{alg:line}
will construct the lines, where $F(\tau)$ is the set of faces of
control volume $\tau$, and $\sigma=C(\tau, f)$ is the control volume
which has the face $f$ in common with control volume $\tau$. This
algorithm ensures that two adjacent control volumes are connected to
each other if and only if they have their first or second highly
weighted face lying between them \cite{fidkowski2005}.

\begin{algorithm}
  \caption{Line creation}
  \label{alg:line}
  \begin{algorithmic}
    \Function{CreateLines}{}
    \Comment{Calling this function creates all lines.}
    \State Unmark all control volumes.
    \While{there a exists an unmarked control volume $\tau$}
    \State \Call{MakePath}{$\tau$}
    \Comment{Forward path creation}
    \State \Call{MakePath}{$\tau$}
    \Comment{Backward path creation}
    \EndWhile
    \EndFunction
    \vspace{0.3in}
    \Function{MakePath}{$\tau$}
    \Comment{Helper function.}
    \Repeat
    % 
    \parState{ For control volume $\tau$, pick the face
      $f \in F (\tau)$ with the highest weight, such that control
      volume $ \sigma= C(\tau,f)$ is not part of the current line.}
    %
    \If{ $f$ is a boundary face}
    \State Terminate
    \ElsIf{$\sigma$ is marked}
    \State Terminate
    \ElsIf{$f$ is not the first or second ranked face of $\sigma$ in weight}
    \State Terminate
    \EndIf
    %
    \State Mark $\sigma$.
    \State Add $\sigma$ to the current line.
    \State $\tau \gets  \sigma$
    \Until{Termination}
    \EndFunction
  \end{algorithmic}
\end{algorithm}

To illustrate the performance of the algorithm, an anisotropic mesh
for the geometry of a NACA0012 airfoil is considered, as shown in
\autoref{fig:c4_lines}. The velocity has been taken from the $2$-exact
solution of the flow resulting from a Mach number of $0.15$, a
Reynolds number of $6\times10^6$, and an angle of attack of
$10^\circ$. The parameter $\mu_L$ is set to the heuristic value of
$10^{-5}$. As desired, the lines follow the direction of mesh
anisotropy near wall boundaries, while their pattern changes, and
follows the flow direction in other parts of the geometry.

\begin{figure}
  \centering
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_view_whole.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_lines_whole.eps}
  \end{subfigure}

  \vspace{0.5in}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_view_close.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_lines_close.eps}
  \end{subfigure}
  \vspace{0.5in}

  \caption{Mesh and lines of strong unknown coupling for the geometry of
    a NACA0012 airfoil}
  \label{fig:c4_lines}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% Numerical Comparisons         -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Numerical Comparisons}
\label{ch:c4_cmp}

In this section, the performance of the proposed preconditioning
algorithm is studied by considering the turbulent flow around a
NACA0012 airfoil, with $\alpha=10^\circ$, $\Mach=0.15$, and
$\Reyn=6 \times 10^6$. Three nested meshes with approximately $25$K,
$100$K, and $400$K control volumes are considered, where the coarsest
mesh and its corresponding lines of strong unknown coupling are shown
in \autoref{fig:c4_lines}. The farfield boundaries are located almost
$500$ chords away from the airfoil, and the chord length has a
nondimensional value of one.  This problem is taken from the NASA
Turbulence Modeling Resource (TMR) website \cite{nasatmr}, and has
been previously studied by Jalali and Ollivier-Gooch
\cite{jalali2017}, where they used a HO-ILU$3$ preconditioner with QMD
reordering. Only the highest-order discretization scheme of \anslib{}
($3$-exact) is considered here, since it results in the stiffest
linear systems that are most difficult to solve. Also, the initial
conditions are taken from the steady-state solution of the lower-order
$2$-exact scheme.

%% OLD NOTES: before results talk about superiority
% Firstly, the superiority of the proposed preconditioning algorithm is
% demonstrated by comparing it to other ILU based methods for two meshes
% with approximately $25$K and $100$K control volumes. The coarsest mesh
% and its corresponding lines of strong unknown coupling are shown in
% \autoref{fig:c4_lines}. Then, it is shown that the proposed
% preconditioning algorithm can handle a fine mesh with $400$K control
% volumes, which was not considered in the previous work of Jalali and
% Ollivier-Gooch \cite{jalali2017}. The steady-state drag and lift
% coefficients are also presented for verification purposes.

The ILU based preconditioning methods tested are shown in
\autoref{tab:c4_pname}. In all cases, the outer GMRES solver stops
when the linear residual of Equation \eqref{eq:PTC} is reduced by a
factor of $10^{-3}$, or a maximum number of $500$ iterations is
performed. The maximum Krylov subspace size is $100$ for the outer
GMRES solver, and the initial $\CFL$ number is set to
$10^{-2}$. Furthermore, the simulation ends when the norm of the
residual vector $\| \vvec R(\vvec U_h) \|_2$ is reduced by a factor of
$10^{-8}$. The command-line options supplied to the \anslib{}
executable for each case are listed in \autoref{ch:app1_comm}. Cases
A-E are compared to each other on the $25$K and $100$K meshes, while
case E$^*$ is used to solve the problem on the finest mesh. Note that
other combinations of preconditioner and reordering schemes that are
not considered in \autoref{tab:c4_pname}, did not result in
convergence even for the coarse mesh. Moreover, an extensive search
for finding the optimum number of inner iterations has not been
carried out, and the large number of inner iterations for case E$^*$
is chosen as a safe measure to ensure convergence on the finest mesh.


\begin{table} \centering
  \caption{Preconditioning methods considered}
  \label{tab:c4_pname}
  \begin{tabular}{cccc}
    \hline
    Case &Preconditioning &Reordering &Number of inner          \\
    name &method          &algorithm  &GMRES iterations         \\
    \hline
    A         &HO-ILU$3$        &QMD      &---                  \\
    B         &LO-ILU$0$        &RCM      &---                  \\
    C         &LO-ILU$0$        &lines    &---                  \\
    D         &GMRES-LO-ILU$0$  &RCM      &10                   \\
    E         &GMRES-LO-ILU$0$  &lines    &10                   \\
    E$^*$
    &GMRES-LO-ILU$0$  &lines    &60                             \\    
    \hline
  \end{tabular}
\end{table}


The residual history of the solver for each preconditioning algorithm is
shown in \autoref{fig:c4_hiscoarse}, where the wall time is obtained
from a single core of an Intel i7-4790 ($3.60$ GHz) CPU. Our proposed
preconditioning algorithm (case E) outperforms all the other methods
on both mesh sizes, and takes only half the time of the HO-ILU$3$
algorithm (case A) to find the steady-state solution. Furthermore, the
line reordering algorithm shows its prominence on the $100$K control
volume mesh, where the RCM reordering algorithm fails. The
effectiveness of the new preconditioning algorithm is further
demonstrated by solving the problem on a $400$K control volume
mesh. The residual history for this case is shown in
\autoref{fig:c4_hisfine}.


\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{img/sts_res_comparison.eps}
  \caption{Comparison of residual histories for the NACA0012 problem
    obtained using different preconditioning algorithms}
  \label{fig:c4_hiscoarse}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{img/sts_res_sfine.eps}
  \caption{Residual history for the NACA0012 problem on the finest
    mesh of $400$K control volumes}
  \label{fig:c4_hisfine}
\end{figure}

\autoref{tab:c4_pperf} shows the number of PTC iterations (\#PTC), the
number of outer GMRES iterations (\#OGMRES), total memory consumption,
CPU time spent on the linear solver (LST), and total computational
time (TCT). The proposed preconditioning scheme (case E) speeds up the
linear solve process by a factor of three, and results in a twofold
reduction in memory consumption, compared to the HO-ILU$3$ method. The
shortcomings of the LO-ILU methods (cases B and C) are evident because
of their large number of PTC iterations and failure in convergence, on
the coarse and medium meshes, respectively. Also, the memory
consumption scales linearly with respect to the problem size for the
proposed preconditioning scheme, whereas the linear solve time seems
to be increasing at a much more rapid rate.

The viscous drag ($C_{Dv}$), the pressure drag ($C_{Dp}$), and the
lift ($C_{L}$) coefficients computed from the numerical solution along
with their reference values are listed in \autoref{tab:c4_coeffs}. The
reference values are taken from the NASA TMR website, and are obtained
by the FUN3D \cite{nasafun3d} solver running on a very fine mesh
(approximately 2M quadrilaterals). As expected, the change in the
coefficients between the fine and medium meshes is much smaller than
that of the medium and coarse meshes. Furthermore, the gap between the
coefficients and their reference values gets smaller with mesh
refinement, with the exception of $C_L$. Solution singularities,
different boundary condition implementations between \anslib{} and
FUN3D, or inexact evaluation of the distance from wall function may
partly be accountable for the non-ideal behavior of the lift
coefficient. Nevertheless, the purpose of this section was the
solution of the discretized system of equations, which according to
\autoref{fig:c4_hisfine} has been performed correctly.


\begin{table}[p]
  \centering
  \caption[Performance comparison for different preconditioning
  schemes]{Performance comparison for different preconditioning
    schemes. Cases with entries marked by ``$-$'' did not converge
    to the steady state solution.}
  \label{tab:c4_pperf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    Preconditioner &\#PTC   &\#OGMRES
    &Memory(GB)    &LST(s)   &TCT(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{\#CV = $25$K} \\
    \hline
          A  &$37$   &$956$      &$1.4$      &$416$   &$635$  \\
          B  &$44$   &$10,893$   &$0.7$      &$264$   &$572$  \\
          C  &$51$   &$13,692$   &$0.7$      &$328$   &$705$  \\
          D  &$34$   &$1,856$    &$0.7$      &$134$   &$379$   \\
          E  &$34$   &$1,787$    &$0.7$      &$118$   &$361$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{\#CV = $100$K} \\
    \hline
          A  &$39$  &$4,640$   &$5.9$      &$3,122$  &$4,068$     \\
          B  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          C  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          D  &$-$   &$-$       &$3.2$      &$-$      &$-$         \\
          E  &$36$  &$4,396$   &$3.2$      &$1,308$  &$2,348$      \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{\#CV = $400$K} \\
    \hline
    E$^*$  &$38$   &$6,869$    &$13.1$      &$87,312$ &$91,502$ \\
    \hline
  \end{tabular}
  \vspace{1in}

  \centering
  \caption{Computed drag and lift coefficients for the NACA0012
    airfoil}
  \label{tab:c4_coeffs}
  \begin{tabular}{*{4}{c}}
    \hline
    \#CV    &$C_{Dp}$ &$C_{Dv}$ &$C_L$ \\
    \hline
    25K     &$0.00545$ &$0.00583$ &$1.0951$ \\
    100K    &$0.00615$ &$0.00623$ &$1.0905$ \\
    400K    &$0.00609$ &$0.00619$ &$1.0922$   \\
    \hline
    NASA TMR  &$0.00607$ &$0.00621$ &$1.0910$ \\
    \hline
  \end{tabular}
\end{table}


\endinput

% \subsection{30P30N Multi-Element Airfoil}

% \begin{figure}
%   \centering
%   \includegraphics[width=0.7\linewidth]{img/sts_multi_mesh.eps}\vspace{0.3in}
  
%   \includegraphics[width=0.7\linewidth]{img/sts_multi_lines.png}\vspace{0.3in}

%   \includegraphics[width=0.7\linewidth]{img/sts_multi_part.png}
%   \caption{}
%   \label{fig:c4_multifigs}
% \end{figure}

% \begin{figure}
%     \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
%   \centering
%   \includegraphics[height=\tempheight]{img/sts_multi_conv.eps}
%   \caption{}
%   \label{fig:c4_multifigs}
% \end{figure}

%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
