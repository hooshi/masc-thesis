\chapter{Background}
\label{ch:c2_background}

% ------------------------------------------------------------------------------
% Chapter beginning
% ------------------------------------------------------------------------------

This chapter presents the fundamentals upon which this research has
been founded. As the algorithms involved in this thesis have been
implemented as part of our in-house solver, \anslib{}, it is essential
to present the working mechanism of this solver.  Thus, this chapter
starts with a description of the Finite Volume Method (FVM) and
$k$-exact reconstruction, which are the numerical solution schemes in
\anslib{}, and then moves on to the model equations of interest.

% As the algorithms developed has been implemented It starts with a
% description of the Finite Volume Method (FVM) and $k$-exact
% reconstruction, which constitute the numerical solution scheme of
% our in-house flow solver, \anslib. and then moves on to the model
% equations of interest.

% ------------------------------------------------------------------------------
% SECTION: Fundamentals of Mesh Based Numerical Methods
% ------------------------------------------------------------------------------
\section{The Finite Volume Method}

% \nunk -> number of unknows
% \ndof -> number of degrees of freedom.
% \nrec   -> recon coefficients

A mesh-based numerical discretization scheme seeks to approximate the
solution to a PDE $\mathcal L \vvec u(\xyz) = 0$ inside a domain
$\Omega \subset \mathbb{R}^\ndim$, where $\mathcal L$ is a
differential operator, $\vvec u(\xyz) \in \mathbb{R}^{\nunk}$ is the
unknown exact solution, and no time dependence has been assumed for
simplicity. In the first step, the domain is subdivided (meshed) into
a set of non-overlapping sub-volumes $\mathcal T_h$, followed by
defining a discrete function in the form of
$\vvec u_h(\vvec x;\vvec U_h)$. Here, the term discrete means that
this function will be uniquely defined if its degrees of freedom,
$\vvec U_h \in \mathbb{R}^{\ndof} $, are all specified. Furthermore,
the subscript $h$, which represents the smallest length scale of the
subdivision, emphasizes the dependency of the corresponding quantities
on the mesh. Finally, the goal is to find $\vvec U_h$, such that
$\vvec u_h$ closely approximates the exact solution $\vvec u$, as the
mesh gets smaller. The rigorous definition of ``to closely
approximate'' is that the discretization error,
$\vvec e_h = \vvec u_h -\vvec u$, must have an asymptotic behavior,
such that: $ \| \vvec e_h \| = \BigO(h^p) $. The discretization scheme
is then said to be $p$th-order accurate.


% TALK ABOUT PROJECTION and PROJECTION error

% ------------------------------------------------------------------------------
% SECTION: Finite Volume Method
% ------------------------------------------------------------------------------

The finite volume method is among the most popular mesh-based
discretization schemes for the solution of fluid flow equations. This
method is applied to equations which can be expressed in the
conservative form:
% 
% General PDE in conservative form.
\begin{equation}
  \label{eq:conserve}
  \frac{ \partial \vvec{u} }{ \partial t}
  + \nabla \cdot
  \left(
    \mmat{F}(\vvec u) - \mmat{Q}(\vvec u, \nabla \vvec{u}) 
  \right)
  =
  \vvec{S}(\vvec u, \nabla \vvec{u}),
\end{equation}
% 
where $\mmat F \in \mathbb{R}^{\nunk \times \ndim}$ is the inviscid
flux matrix, $\mmat Q \in \mathbb{R}^{\nunk \times \ndim}$ is the
viscous flux matrix, and $\vvec S \in \mathbb{R}^{\nunk}$ is the
source term vector. In this method, the degrees of freedom are the
same as the average value of the discrete solution inside every
control volume. This fundamental constraint, known as the conservation
of the mean, can be written as:
% 
% Conservation of the mean
\begin{equation}
  \label{eq:cons_mean}
  \frac{1}{\Omega_\tau} \int_{\tau} \vvec u_{h}(\xyz) d\Omega = \vvec U_{h,\tau}
  \qquad \tau \in \SetCV,
\end{equation}
% 
where $\Omega_\tau$ and $\vvec U_{h,\tau} \in \mathbb{R}^{\nunk}$
represent the volume and local \dof{} vector for control volume
$\tau$, respectively. Establishing a relation between the discrete
solution $\vvec u_h$, and the degrees of freedom $\vvec U_h$, when
satisfying the conservation of the mean constraint, is called
reconstruction in the FVM framework. To accomplish this, \anslib{}
uses the $k$-exact reconstruction scheme, which will be introduced in
Section \ref{ch:c2_k_recon}.


The FVM uses the divergence theorem to discretize Equation
\eqref{eq:conserve}. Consider a control volume $\tau \in \mathcal T_h$
as depicted in Figure \ref{fig:c2_fvm}.  Integrating Equation
\eqref{eq:conserve} inside the control volume, and using the
divergence theorem gives:
%% TALK about u- and u+ here.
% 
% Non smooth finite volume.
\begin{multline}
  \label{eq:fvm}
  \frac{d\vvec U_{h,\tau}}{dt}
  + \frac{1}{\Omega_\tau}
  \int_{\partial \tau \setminus \partial \Omega }
  \left(
    \vvec \NumFluxF_I(\vvec u_h^+,\vvec u_h^-)-
    \vvec \NumFluxQ_I(
    \vvec u_h^+, \nabla \vvec u_h^+, \vvec u_h^-, \nabla \vvec u_h^-
    ) 
  \right) dS \\
  + \frac{1}{\Omega_\tau}
  \int_{\partial \tau \cap \partial \Omega}
  \left(
    \vvec \NumFluxF_B(\vvec u_h, \BdryCond)-
    \vvec \NumFluxQ_B(\vvec u_h, \nabla \vvec u_h, \BdryCond)
  \right)dS  
  -
  \frac{1}{\Omega_\tau}
  \int_{\tau} \vvec S(\vvec u_h, \nabla \vvec u_h) d\Omega
  = 0,
\end{multline}
%
where $\BdryCond$ represents the boundary conditions, in the case
where the control volume has faces lying on the boundary. Although the
discrete solution $\vvec u_h$, and its gradient $\nabla \vvec u_h$,
are continuous inside every control volume, they can be discontinuous
on the control volume boundaries $\partial \tau$. These discontinuous
values are shown using the $()^+$ and $()^-$ notations. $\vvec F_I$
and $\vvec Q_I$ represent the interior numerical flux functions, while
$\vvec F_B$ and $\vvec Q_B$ represent their boundary
counterparts. Numerical flux functions are designed to mimic the
product of the original flux matrices and the normal vector, while
taking into account the discontinuity of the discrete solution and the
boundary conditions. The flux functions used in this thesis will be
introduced in Section \ref{ch:c2_flux}.

Looking back at Equation \eqref{eq:fvm}, the following system of ODEs
for control volume averages can be derived:
% 
% Compact form
\begin{equation}
  \label{eq:nonlineareq}
  \frac{d \vvec U_h}{dt} + \vvec R( \vvec U_h) = 0,
\end{equation}
% 
which can be solved with a variety of time advance schemes when a
time-accurate solution is of interest. Butcher presents a detailed
study of these methods \cite{butcher2016}. Although only the steady
state solution is of interest in this thesis, the unsteady terms can
be used to improve the robustness of the solver with respect to the
initial solution guess. This method, known as pseudo transient
continuation (PTC) \cite{kelley1998}, will be introduced in Chapter
\ref{ch:c4_precon}.

% ------------------------------FIGURE: FVM FORMULATION
\begin{figure}
  \includegraphics[width=.5\linewidth,center]{img/flux_integral.pdf}
  \caption{Illustration of terminology in finite volume
    discretization}
  \label{fig:c2_fvm}
\end{figure}

% ------------------------------------------------------------------------------
% SECTION: K-exact recon
% ------------------------------------------------------------------------------
%% Talk about accuracy

\section{K-exact Reconstruction}
\label{ch:c2_k_recon}

% --------------- basis functions
The design of reconstruction schemes in the finite volume framework
started with Van Leer's MUSCL scheme \cite{vanleer1977,vanleer1997}.
His scheme took advantage of the uniform pattern of control volumes in
structured meshes and was not directly applicable to unstructured
grids. Later on, the $k$-exact reconstruction \cite{barth1990} and the
WENO/ENO \cite{shu1999} family of methods were designed, and did not
suffer from MUSCL's shortcoming in handling unstructured meshes. The
latter, however, has poor steady state convergence properties
\cite{rogerson1990}. Targeted to solve aerodynamic steady state
problems, \anslib{} uses the $k$-exact reconstruction method in its
finite volume formulation.

Suppose a polynomial function of order smaller or equal to $k$,
$v(\xyz)$, is integrated in every control volume to find the control
volume averages $\vvec V_h$. If these average values are fed to a
$k$-exact reconstruction scheme to construct the function $v_h(x)$,
the identity $v_h(\xyz)=v(\xyz)$ must hold, which is where the name
$k$-exact comes from.  A $k$-exact scheme is also called
$(k+1)$th-order accurate, since it results in a nominal discretization
error of order $\BigO(h^{(k+1)})$ \cite{barth2002}. For simplicity,
let us consider this method when there is only one unknown variable,
\ie{} the vector $\vvec u$ reduces to a scalar $u$. Generalization to
multiple unknown variables will then be straightforward. In this case,
the solution in every control volume is defined as the superposition
of a set of basis functions in the form:
% 
% reconstruction
\begin{equation}
  u_h(\xyz; \vvec U_h, \BdryCond)|_{x \in \tau} =
  u_{h,\tau}(\xyz; \vvec U_h, \BdryCond) =
  \sum _{i=1}^{\nrec} a_{\tau}^i(\vvec U_h, \BdryCond) \phi_{\tau}^i(\xyz)
  \quad \tau \in \mathcal{T}_h.
\end{equation}
% 
Where $\phi_\tau^i(\xyz)$ and $a_\tau^i$ represent the $i$th basis
function, and reconstruction coefficient for control volume $\tau$,
respectively.  Most commonly, the basis functions will be chosen as
monomials in Cartesian coordinates originated at the reference point
of each control volume:
% 
% Choice of phi
\begin{equation}
  \label{eq:cart_mon}
  \Set{ \phi_\tau^i(\xyz) | i = 1 \ldots \nrec }
  =
  \Set{ \frac{1}{a!b!c!}
    (x_1-x_{\tau1})^a
    (x_2-x_{\tau2})^b
    (x_3-x_{\tau3})^c
    | a+b+c \leq k },
\end{equation}
% 
where $x_{\tau1}$, $x_{\tau2}$, and $x_{\tau3}$ are the coordinates of
the control volume's reference location, which is usually chosen as
the center of volume. This particular choice of basis functions has
the advantage that the discrete solution $u_h$, will resemble the
Taylor expansion of the exact solution $u$, which is particularly
useful in theoretical analysis \cite{ollivier2002}. However, there are
situations, \eg{} highly anisotropic meshes, in which it would be more
beneficial to use other basis functions \cite{jalali2017}. Such a case
will be introduced in Chapter \ref{ch:c3_threed}.


% --------------- Least square problem
The discrete solution must satisfy the conservation of the mean
constraint, given in Equation \eqref{eq:cons_mean}. Moreover, for
every control volume $\tau$, a specific set of its neighbors are
chosen as its reconstruction stencil $\Stencil(\tau)$. The $k$-exact
reconstruction requires $u_{h,\tau}$ to predict the average values of
the members of $\Stencil(\tau)$ closely. Furthermore, if the control
volume is located on the boundary
($\partial \tau \cap \partial \Omega \neq \emptyset$), enforcing
$u_{h,\tau}$ or $\nabla u_{h,\tau} \cdot \nvec n$ to have certain
values at boundary quadrature points, can improve convergence in
presence of certain boundary conditions \cite{ollivier2002}. Thus, the
reconstruction coefficients can be found by solving the following
constrained minimization problem:
% 
% Least squares problem
\begin{equation}
  \label{eq:recon_min}
  \begin{aligned}
    % 
    & \underset{a_{\tau}^1 \ldots a_{\tau}^{\nrec}}{\text{minimize}}
    & & \sum_{\sigma \in \Stencil(\tau)}
    \left(
      \frac{1}{\Omega_\sigma}\int_{\sigma}  u_{h,\tau}(\xyz) d\Omega -
      U_{h,\sigma} 
    \right)^2
    +
    \sum_{g \in \BdGauss(\tau)}
    \left(
      B( u_{h,\tau}(\xyz), \nabla u_{h,\tau}(\xyz) , \BdryCond)
    \right)^2    
    \\
    & \text{subject to}
    & & \frac{1}{\Omega_\tau} \int_{\tau}
     u_{h}(\xyz) d\Omega = U_{h,\tau},
  \end{aligned}
\end{equation}
% 
where $\Stencil(\tau)$ and $\BdGauss(\tau)$ are the reconstruction
stencil, and the set of boundary quadrature points for control volume
$\tau$, respectively. Furthermore, $B$ is the boundary condition
constraint function. The number of control volumes in $\Stencil(\tau)$
must be greater than the number of reconstruction coefficients
$\nrec$, so that the minimization problem does not become
undetermined. To construct $\Stencil(\tau)$, all the neighbors at a
given level of $\tau$ are added to the stencil until the number of
stencil members gets bigger than a given value $\MinNeigh(k)$. For a
well behaved problem on a mesh with high regularity, choosing
$\MinNeigh(k) = \nrec$ can result in an acceptable solution. However,
a value of $\MinNeigh(k) = 1.5\nrec$ is chosen to cope with mesh
irregularities and oscillatory solution behavior. Figure
\ref{fig:c2_stencil} shows a control volume and its reconstruction
stencil for different $k$ values.

By introducing the integral of each basis function of every control
volume inside itself and its stencil members:
% 
% Moments 
\begin{equation}
  I_{\tau\sigma}^i = \int_{\sigma} \phi_\tau^i(x) d\Omega
  \qquad \sigma \in \Stencil(\tau) \cup \Set{\tau},
\end{equation}
% 
the constrained minimization problem in Equations
\eqref{eq:recon_min}, can be written in a compact matrix form:
% 
% Compact matrix form
\begin{equation}
  \label{eq:recon_min_mat}
  \begin{bmatrix} 
    I_{\tau\tau}^1         &\dots  &I_{\tau\tau}^{\nrec}        \\
    \hline 
    I_{\tau\sigma_1}^1      &\dots  &I_{\tau\sigma_1}^{\nrec}     \\
    \vdots               &\ddots & \vdots                \\
    I_{\tau\sigma_{\text{NS}(\tau)}}^1   &\dots  &I_{\tau\sigma_{\text{NS}(\tau)}}^{\nrec}
  \end{bmatrix}
  % 
  \begin{bmatrix} 
    a_\tau^1 \\
    \vdots \\
    a_\tau^{\nrec} 
  \end{bmatrix}
  % 
  =
  % 
  \begin{bmatrix} 
    U_{h,\tau}  \\
    \hline
    U_{h,\sigma_1}  \\
    \vdots \\
    U_{h,\sigma_{\text{NS}(\tau)}}  
  \end{bmatrix},
  % 
\end{equation}
% 
where the boundary constraints have been neglected for the sake of
simplicity. The symbols $\sigma_1$, $\sigma_2$, \ldots,
$\sigma_{\text{NS}(\tau)}$ represent the members of
$\Stencil(\tau)$. The first row of the matrix is a constraint and must
be exactly satisfied, while the other rows correspond to equations
that have to be minimized in a least squares fashion. As the left hand
side matrix in Equation \eqref{eq:recon_min_mat} is only dependent on
geometric terms, and does not include the average solution values
$\vvec U_h$, the solution can be written as:
% 
% Pseudo inverse
\begin{equation}
  % 
  \begin{bmatrix} 
    a_\tau^1 \\
    \vdots \\
    a_\tau^{\nrec} 
  \end{bmatrix}
  % 
  =
  \mmat A_\tau^{\dagger}
  % 
  \begin{bmatrix} 
    U_{h,\tau}  \\
    U_{h,\sigma_1}  \\
    \vdots \\
    U_{h,\sigma_{\text{NS}(\tau)}}  
  \end{bmatrix},
\end{equation}
% 
where the matrix $\mmat A_\tau^{\dagger}$ is the pseudo-inverse of the
left hand side matrix. A change of variables inspired by the Gaussian
Elimination method is used to convert Equation
\eqref{eq:recon_min_mat} into an unconstrained optimization problem
\cite{ollivier2002}. Then the unconstrained problem is solved by SVD
decomposition \cite{golub2012} to yield the pseudo-inverse
matrix. This process has to be evaluated for every control volume,
only as a preprocessing step, which prevents it from becoming a
bottleneck in our computations.

% SHOCK
In the case of equations with discontinuous solutions, shock capturing
numerical schemes such as slope limiters \cite{vonneumann1950} or
artificial diffusion \cite{sweby1984} are required in conjunction with
the $k$-exact reconstruction method to ensure convergence to the
correct solution. For more recent implementation of these methods in
conjunction with higher-order schemes see \cite{barter2010,
  michalak2009}. In this thesis, however, the emphasis is on model
problems without discontinuities. Thus, no shock capturing methods
have been used.

% ------------------------------FIGURE: K-EXACT STENCIL
\begin{figure}
  \includegraphics[width=.5\linewidth,center]{img/stencil.pdf}
  \caption[Reconstruction stencils for a control
  volume]{Reconstruction stencils for a control volume $\tau$: A set
    of values $\MinNeigh(1)=3$, $\MinNeigh(2)=9$, and
    $\MinNeigh(3)=18$ have been used, which have resulted in
    reconstruction stencils $\Set{\text{blue}}$,
    $\Set{\text{blue, magenta}}$ and
    $\Set{\text{blue, magenta, cyan}}$ for the $1$-exact, $2$-exact
    and $3$-exact reconstruction schemes, respectively.}
  \label{fig:c2_stencil}
\end{figure}

% ------------------------------------------------------------------------------
% SECTION: Studied Equations
% ------------------------------------------------------------------------------
\section{Studied Equations}


% As the main objective of this work is the solution the turbulent
% Navier-Stokes equations, they will be introduced briefly in this
% section. Moreover, the simpler Poisson, and Euler equations will also
% be presented, as they are used in the later chapters for the purpose
% of verification and assessment of the solver performance.

This section introduces the equations of interest. Namely, Poisson's,
Euler, laminar and Reynolds averaged Navier-Stokes, and the
Spalart-Allmaras turbulence closure equations.

% ---------------------------THE POISON EQUATION-------------------------------
\subsection{Poisson's Equation}

The Poisson's equation is a simple yet powerful tool to verify the
correct implementation of many algorithms in \anslib{}. This equation
has a scalar unknown $u$ and a solution-independent source term $f$ in
the form of:
% 
\begin{equation}
  \label{eq:poisson}
  -\nabla \cdot ( \nabla u ) = f(\xyz).
\end{equation}
% 
To simplify the notation, we consider the gradient operator on a scalar
function as a row vector. For example, $\nabla u = [\frac{\partial
  u}{\partial x_1}, \frac{\partial u}{\partial x_2}, \frac{\partial
  u}{\partial x_3} ] $. Thus the Poisson equation can be recovered
from the steady state form of the conservative Equation
\eqref{eq:conserve} by replacing $\mmat F = \nabla u$, $\mmat Q =
0$, and $S = f(\xyz)$.

% --------------------THE NAVIER STOKES EQUATION-------------------------------
\subsection{Navier-Stokes Equations}

The Navier-Stokes and the continuity equations are widely used for the
simulation of fluid flow. In the compressible form of these equations,
the vector of unknowns is $\vvec u=[\rho, \rho \vel^T, E]^T$. Where
$\rho$ is the fluid density, $\vel=[v_1,v_2,v_3]^T$ is the velocity
vector, and $E$ is the total energy. When combined with the ideal gas
internal energy and state equations, the nondimensionalized
Navier-Stokes equations can be identified by a zero source term vector
and flux matrices:
% 
% Navier Stokes
\begin{equation}
  \mmat F = 
  \begin{bmatrix} 
    \rho \vel^T       \\
    \rho \vel \vel^T + P \mmat I \\
    (E+P) \vel^T
  \end{bmatrix}
  % 
  \quad
  % 
  \mmat Q =
  \begin{bmatrix} 
    0                                \\
    \frac{\Mach}{\Reyn} \mmat \tau   \\
    (E+P) \mmat \tau \vel +
    \frac{1}{\gamma - 1} \left( \frac{\mu}{\Pran} \right) \nabla T 
  \end{bmatrix}.
\end{equation}
% 
Where $\Mach$, $\Reyn$, $\Pran$, and $\gamma$ represent the Mach
number, the Reynolds number, the Prandtl number, and the specific heat
ratio, respectively. $T$ is the temperature, $()^T$ denotes matrix
transpose, $P$ is the pressure, $\mmat \tau$ is the viscous stress
matrix, $\mu$ is the dimensionless fluid viscosity, and $\mmat I$ is
the identity matrix. Since the emphasis is on air as the working
fluid, the values $\gamma=1.4$, $\Pran=0.72$ are used, and $\mu$ is
found from Sutherland's law. The pressure is related to the dependent
variables via the formula:
% 
% Pressure
\begin{equation}
  P = (\gamma-1)
  \left(
    E - \frac{1}{2}\rho(\vel \cdot \vel)
  \right).
\end{equation}
% 
Similarly, temperature is related to pressure and density in the form:
% 
% Temperature
\begin{equation}
  T = \frac{\gamma P}{\rho}.
\end{equation}
For Newtonian compressible fluids, the viscous stress tensor is
related to the velocity as:
% 
% Viscous Stress tensor
\begin{equation}
  \mmat \tau =
  2 \mu
  \left(
    \frac{1}{2} \left( \nabla \vel + ( \nabla \vel)^T \right) -
    \frac{1}{3} \trace(\nabla \vel) \mmat I
  \right),
\end{equation}
% 
where $\nabla \vel$ represents the gradient of the velocity vector,
such that $(\nabla \vel)_{ij} = \frac{\partial v_i}{\partial x_j}$.
% 
% gradient of velocity vector
% \begin{equation}
%   \nabla \vel =
%   \begin{bmatrix} 
%     \frac{\partial v_1}{\partial x_1}
%     &\frac{\partial v_1}{\partial x_2}
%     &\frac{\partial v_1}{\partial x_3} \\
%     \frac{\partial v_2}{\partial x_1}
%     &\frac{\partial v_2}{\partial x_2}
%     &\frac{\partial v_2}{\partial x_3} \\
%     \frac{\partial v_3}{\partial x_1}
%     &\frac{\partial v_3}{\partial x_2}
%     &\frac{\partial v_3}{\partial x_3}
%   \end{bmatrix}.
% \end{equation}

If the viscous terms are neglected, i.e., either $\mmat Q$, or
equivalently $\mu$ is set to zero, the Euler equations would be
recovered. The Euler equations are used in this work to asses the
capability of the solver in handling reasonably big three-dimensional
problems.

% --------------------SPALART ALLMARAS EQUATION-------------------------------
\subsection{Extension to Turbulent Flows}

% MOVE IMPORTANCE TO INTRODUCTION
% Because of the important role of turbulence in aerodynamics,
% It is possible to use the Navier-Stokes equations to model turbulent
% flows directly. However, this process requires capturing very small
% length and time scales, which requires tremendous computational
% power. A workaround for this issue is to use the Reynolds-Averaged
% Navier-Stokes (RANS) equations that are derived from averaging the
% Navier-Stokes equations over a representative time scale. RANS
% equations, nevertheless, have additional parameters called the
% Reynolds stress terms, which have to be evaluated from empirical
% closure equations.

% The RANS equations along with the Spalart-Allmaras \cite{spalart1}
% closure equation are used in this work, to evaluate the capability
% of high-order FVM in solving turbulent flow problems. The popularity
% and simplicity of the Spalart-Allmaras model makes it an ideal
% stepping stone for developing turbulent flow solvers. More
% complicated models, however, can be studied after the numerical
% method has proven to be successful in handling their simpler
% counterparts.

In this thesis, the RANS equations are coupled with the negative
Spalart-Allmaras (negative S-A) turbulence model \cite{spalart2}.
This model includes a number of dimensionless empirical constants,
listed in Table \ref{tab:c2_sa_con}, and a few empirical functions,
shown by the symbol $f$ and an appropriate subscript, which will be
introduced shortly. The nondimensionalized flux matrices for the RANS
+ negative S-A are defined as:
% 
% Spalart Allmaras fluxes
\begin{equation}
  \label{eq:sa_flux}
  \mmat F = 
  \begin{bmatrix} 
    \rho \vel^T                  \\
    \rho \vel \vel^T + P \mmat I \\
    (E+P) \vel^T                 \\
    \nut \rho \vel^T          
  \end{bmatrix}
  % 
  \quad
  % 
  \mmat Q =
  \begin{bmatrix} 
    0                                      \\
    \frac{\Mach}{\Reyn} \mmat \tau         \\
    (E+P) \mmat \tau \vel +
    \frac{1}{\gamma - 1}
    \left( \frac{\mu}{\Pran} + \frac{\mu_T}{\Pran_T} \right) \nabla T \\
    -\frac{\Mach}{\Reyn \sigma} ( \mu + \mu_T ) \nabla \nut
  \end{bmatrix},
\end{equation}
% 
where $\nut$ is the negative S-A working variable, $\mu_T$ is the
turbulent viscosity, and $\Pran_T$ is the turbulent Prandtl number,
which has a value of $0.9$ for air. The source term of the
nondimensionalized RANS + negative S-A is defined as:
% 
% Source term
\begin{equation}
  \label{eq:sa_source}
  \vvec S =
  \begin{bmatrix} 
    0             \\
    0             \\
    0             \\
    \Diff + \rho (P-D+T)
  \end{bmatrix},
\end{equation}
% 
where $\Diff$, $P$, $D$, and $T$ represent diffusion, production,
destruction, and trip terms, respectively. The viscous stress tensor
can be found via the modified equation:
% SA Viscous Stress tensor
\begin{equation}
  \label{eq:sa_tau}
  \mmat \tau =
  2 ( \mu + \mu_T)
  \left(
    \frac{1}{2} \left( \nabla \vel + ( \nabla \vel)^T \right) -
    \frac{1}{3} \trace(\nabla \vel) \mmat I
  \right).
\end{equation}
% 
The turbulent viscosity is expressed as:
% Turbulent viscosity
\begin{equation}
  \label{eq:sa_mut}
  \mu_T =
  \begin{cases}
    \mu' f_{v1} \rho \nut     & \nut \geq 0 \\
    0                        & \nut < 0
  \end{cases},
\end{equation}
% 
where $\mu'$ is the reference value that is used to nondimensionalize
the turbulence working variable. Bigger values for $\mu'$ result in
smaller magnitudes for $\nut$ which can enhance the performance of
numerical solvers \cite{ceze2015}. In this work, we have used
$\mu'=1000$. The production term $P$ in Equation \eqref{eq:sa_source}
is defined as:
% 
% Production
\begin{equation}
  \label{eq:sa_prod}
  P =
  \begin{cases}
    c_{b1}(1-f_{t2})\tilde{S}\nut     & \nut \geq 0 \\
    c_{b1}(1-c_{t3})S                 & \nut < 0
  \end{cases}.
\end{equation}
% 
The destruction term $D$ is found from the equation:
% 
% Destruction
\begin{equation}
  \label{eq:sa_dest}
  D =
  \begin{cases}
    \frac{\mu' \Mach}{\Reyn}
    \left( c_{w1}f_w - \frac{c_{b1}}{\kappa^2}f_{t2} \right)
    \left( \frac{\nut}{d} \right)^2             & \nut \geq 0 \\
    -\frac{\mu' \Mach}{\Reyn}
    c_{w1} \left( \frac{\nut}{d} \right)^2      & \nut < 0
  \end{cases}.
\end{equation}
% 
Where $d$ is the minimum distance to wall boundaries. The diffusion
term $\Diff$ is given as:
% 
% Diffusion
\begin{equation}
  \label{eq:sa_diff}
  \Diff = \frac{\Mach}{\Reyn \sigma}
  \left(
    \mu' c_{b2} \rho \nabla \nut \cdot \nabla \nut -
    \frac{\mu}{\rho} \left(1 + \chi f_n \right) \nabla \rho \cdot \nabla \nut
  \right),
\end{equation}
where $\chi$ is:
\begin{equation}
  \chi = \frac{\mu' \rho \nut}{\mu}.
\end{equation}
The trip term $T$, in Equation \eqref{eq:sa_source} models flows that
include the laminar to turbulent transisition phenomenon. Since the
emphasis in this thesis is on fully turbulent flows, the trip term is
neglected and set to zero. The vorticity $S$, and its modified forms,
$\tilde{S}$, $\bar{S}$, are found from the equations:
% S vorticity
\begin{equation}
  \begin{gathered}
    S = \sqrt
    {
      \frac{1}{2}
      \sum_i \sum_j
      \left(
        \frac{ \partial v_i}{ \partial x_j} -
        \frac{\partial v_j}{\partial x_i}
      \right)^2
    }, \\
    % 
    \bar{S} = \frac{\mu' \Mach}{\Reyn} \frac{\nut}{\kappa^2 d^2} f_{v2},  \\
    % 
    \tilde{S} =
    \begin{cases}
      S + \bar{S}                    &\bar{S} \geq -c_{v2} S \\
      S + \frac{c_{v2}^2S + c_{v3}\bar{S}}{(c_{v3}-2c_{v2})S-\bar{S}}
      &\bar{S} < -c_{v2} S \\
    \end{cases}
  \end{gathered}
\end{equation}
% 
The empirical function $f_n$ ensures the positivity of $\mu_T$, and is
defined as:
% 
% fn
\begin{equation}
  f_n =
  \begin{cases}
    1                                     &\nut \geq 0 \\
    \frac{c_{n1}+\chi^3}{c_{n1}-\chi^3}     &\nut < 0 
  \end{cases}.
\end{equation}
% 
The functions $f_{v1}$, $f_{v2}$, $f_{v3}$ are given as:
% 
% fv1, fv2, ft2
\begin{equation}
  f_{v1} = \frac{\chi^3}{\chi^3+c_{v1}^3}, \quad
  f_{v2} = 1 - \frac{\chi}{1 + \chi f_{v1}}, \quad
  f_{t2} = c_{t3} \exp(-c_{t4} \chi^2),
\end{equation}
% 
and the function $f_w$ is given as:
% 
% fw
\begin{equation}
  r = \frac{\mu' \Mach}{\Reyn} \frac{\nut}{\tilde S \kappa^2 d^2}, \quad
  g = r + c_{w2}(r^6-r), \quad
  f_w = g \left( \frac{1+c_{w3}^6}{g^6+c_{w3}^6}  \right).
\end{equation}


% ------------------------------Table of constants 
\begin{table}
  \center
  \caption{Dimensionless empirical parameters used in the negative S-A
    model}
  \label{tab:c2_sa_con}
  \begin{tabular}{ c  c  c  c  c c}
    \hline
    Name &Value &Name &Value &Name &Value \\
    \hline
    $c_{b1}$  &$0.1355$  &$c_{b2}$ &$0.622$  &$c_{w1}$
                                   &$\frac{c_{b1}}{\kappa^2}+\frac{1+c_{b2}}{\sigma}$        \\
    $c_{w2}$  &$0.3$  &$c_{w3}$ &$2.0$  &$c_{t3}$   &$1.2$      \\
    $c_{t4}$  &$0.5$  &$c_{n1}$ &$16$   &$c_{v1}$   &$7.1$      \\
    $c_{v2}$  &$0.7$  &$c_{v3}$ &$0.9$  &$\kappa$  &$0.41$      \\
    $\sigma$ &$0.66$ &         &       &          &          \\
    \hline
  \end{tabular}
\end{table}


Having defined the equations of interest, let us now introduce the
numerical flux functions.

% ------------------------------------------------------------------------------
% SECTION: Flux Evaluation Method
% ------------------------------------------------------------------------------
\section{Numerical Flux Functions}
\label{ch:c2_flux}

As discussed earlier, the FVM relies on flux functions that not only
take into account the discontinuity of the solution along internal
faces, but also correctly capture the influence of the boundary
conditions. In this section, we will introduce the numerical flux
functions used in \anslib{} for the solution of the more general RANS
+ negative S-A equations. Flux functions for the simpler Euler and
Poisson equations can simply be derived by omitting the relevant terms
in those of the more general case.

% ------------------------------ INVISCID - Internal------------------------
\subsection{Inviscid Flux}

The inviscid fluxes represent propagation of information via finite
speed waves, and are convective in nature. Most numerical flux
functions seek a solution to the approximate one-dimensional equation:
% 
% One dimensional approximation
\begin{equation}
  \label{eq:inv1d}
  \frac{\partial \vvec u}{ \partial t} +
  \frac{\partial \mmat F(\vvec u) \nvec n}{ \partial s} =
  0,
\end{equation}
% 
and then use this solution to find the numerical flux. In Equation
\eqref{eq:inv1d}, $\mmat F$ is the inviscid flux matrix, $\nvec n$ is
the face normal vector, and $s$ is the unit of length. The artificial
viscosity method \cite{woodward1984numerical} adds extra nonlinear
dissipative terms to Equation \eqref{eq:inv1d} to give it an elliptic
nature. On the other hand, the Godunov method \cite{godunov} is based
on the exact solution of Equation \eqref{eq:inv1d}, with piecewise
constant initial conditions corresponding to the left and right states
(the Riemann Problem). Due to the expensive cost of exactly solving
the Riemann problem, many researchers have developed approximate
solutions such as the Rusanov \cite{rusanov}, HLL family
\cite{harten1997,toro1994}, and Roe \cite{roe1981} methods. In this
thesis, a computationally efficient formulation \cite{roe1985} of the
approximate Riemann solver of Roe has been used due to its effectivity
and simplicity. This formulation has the form of
% 
% Roe flux ---- general form
\begin{equation}
  \vvec \NumFluxF_I (\vvec u_h^+,\vvec u_h^-) =
  \frac{1}{2}
  \left(
    \mmat F(\vvec u_h^+) \nvec n +
    \mmat F(\vvec u_h^-) \nvec n -
    \vvec D(\vvec u_h^+, \vvec u_h^-)
  \right),
\end{equation}
% 
where $\vvec D$ represents the diffusion vector given by the formula:
% 
% Roe flux ---- Diffusion vector
\begin{equation}
  \label{eq:roediff}
  \vvec D(\vvec u_h^+, \vvec u_h^-) =
  \begin{bmatrix}
    |\tilde \lambda_2| \Delta \rho
    + \delta_1\\
    |\tilde \lambda_2| \Delta (\rho \vel)
    + \delta_1 \tilde \vel + \delta_2 \nvec n \\
    |\tilde \lambda_2| \Delta E  + \delta_1 \tilde H
    + \delta_2 \tilde v^T \nvec n \\
    |\tilde \lambda_2| \Delta (\rho \nut)  + \delta_1 \tilde \nut \\
  \end{bmatrix}.
\end{equation}
% 
The symbol $\Delta$ stands for $()_h^+ - ()_h^-$, and the variables
$\lambda_1, \ldots, \lambda_6$ represent the six eigenvalues of the
Jacobian matrix $\frac{\partial \mmat F(\vvec u) \nvec n}{ \partial
  \vvec u}$, expressed as:
% 
% Eigenvalues
\begin{equation}
  \label{eq:wavespeed}
  \lambda_1 = \vel \cdot \nvec n - c, \quad
  \lambda_2 = \ldots = \lambda_5 = \vel \cdot \nvec n, \quad
  \lambda_6 = \vel \cdot \nvec n + c.
\end{equation}
% 
The variable $c$ is the speed of sound, defined as:
% 
% Speed of sound
\begin{equation}
  c = \sqrt { \frac{\gamma P}{\rho} }.
\end{equation}
% 
In Equation \eqref{eq:roediff} the symbol $\tilde{()}$ represents
the Roe average state, evaluated from the equations:
% 
% Roe flux ---- Average state
\begin{equation}
  \begin{aligned}
    &\tilde \rho = \sqrt{\rho_h^- \rho_h^+}, 
    &\tilde \vel =
    \frac{ \sqrt{\rho_h^-}\vel_h^- + \sqrt{\rho_h^+} \vel_h^+}{\sqrt{\rho_h^+}+\sqrt{\rho_h^+}}, \\
    &\tilde H =
    \frac{ \sqrt{\rho_h^-}H_h^- + \sqrt{\rho_h^+} H_h^+}{\sqrt{\rho_h^+}+\sqrt{\rho_h^+}}, 
    &\tilde \nut =
    \frac{ \sqrt{\rho_h^-}\nut_h^- + \sqrt{\rho_h^+} \nut_h^+}{\sqrt{\rho_h^+}+\sqrt{\rho_h^+}},
  \end{aligned}  
\end{equation}
% 
where $H=\frac{P+E}{\rho}$ is the entropy. Note that $\tilde \vel$ and
$\tilde \nut$ are the velocity and the S-A working variable at the Roe
average state, respectively. The variables $\delta_1$ and $\delta_2$
in Equation \eqref{eq:roediff} are given as:
% 
% Roe flux ---- Delta 1 and Delta 2
\begin{equation}
  \begin{gathered}
    \delta_1 =
    \frac{\Delta P}{\tilde c^2}
    \left(
      -|\tilde \lambda_2| + \frac{|\tilde \lambda_1| + |\tilde \lambda_6|}{2} 
    \right) +
    \frac{\tilde \rho  }{2 \tilde c^2} \Delta (\vel \cdot \nvec n)
    \left(
      |\tilde \lambda_6| - |\tilde \lambda_1| 
    \right),  
    \\
    \delta_2 =
    \frac{\Delta P}{2 \tilde c^2}
    \left(
      |\tilde \lambda_6| - |\tilde \lambda_1| 
    \right) +
    \tilde \rho \Delta (\vel \cdot \nvec n)
    \left(
      -|\tilde \lambda_2| + \frac{|\tilde \lambda_1| + |\tilde \lambda_6|}{2} 
    \right).    
  \end{gathered}
\end{equation}


% ---------------------------------- INVISCID - Bdry----------------------------

%% WALL
\anslib{} evaluates the the inviscid boundary fluxes using the method
of characteristics \cite{hirsch2007}.  For wall and symmetry boundary
conditions, the mass flux must be zero. Thus, the inviscid flux is
evaluated as:
\begin{equation}
  \vvec \NumFluxF_B(\vvec u_h, \BdryCond) = [ 0, P_h \nvec n^T, 0]^T
  \qquad \BdryCond \text{ is symmetry or wall.}
\end{equation}
On farfield boundaries, however, the boundary flux formulation changes
based on the sign of the normal velocity, $\vel \cdot \nvec n$. In
this work, we consider subsonic inflow and outflow boundary conditions
which are identified by the inequalities
$0 \leq \vel \cdot \nvec n < c$, and $-c < \vel \cdot \nvec n \leq 0$,
respectively. In either case, the boundary flux is evaluated in terms
of an intermediate state $\vvec u_h^*$, defined as a function of both
the boundary conditions and the discrete solution:
\begin{equation} 
  \vvec \NumFluxF_B(\vvec u_h, \BdryCond) = \vvec F(\vvec u_h^*) \nvec n,
  \qquad \vvec u_h^* = (\vvec u_h, \BdryCond),
  \qquad \BdryCond \text{ is inflow or outflow.}
\end{equation}
For subsonic inflow, five values of farfield turbulence working
variable $\nut_\text{far}$, total temperature $T_t$, total pressure
$P_t$, side slip angle $\psi$, and angle of attack $\alpha$ must be
specified. Subsequently, the intermediate state $\vvec u_h^*$, can be
found as:
\begin{equation}
  \begin{gathered}
    P_h^* = P_h, \quad
    T^*_h =
    T_t \left( \frac{P^*_h}{P_t} \right)^{\left( \frac{\gamma-1}{\gamma}\right)}, \quad
    \rho_h^* = \gamma \frac{P_h^*}{T_h^*}, \quad
    \nut_h^* = \nut_\text{far}, \\
    \|\vel_h^* \|_2 =
    \sqrt{ \frac{2}{\gamma-1} \left( \frac{T_t}{T_h^*} - 1 \right) }, \\
    v_{h1}^* = \|\vel_h^* \|_2 \cos\alpha \cos\psi, \quad
    v_{h2}^* = \|\vel_h^* \|_2 \sin\alpha \cos\psi, \quad
    v_{h3}^* = \|\vel_h^* \|_2 \sin \psi.
  \end{gathered}
\end{equation}
For subsonic outflow, however, only the back-pressure value $P_b$ must
be specified. The intermediate state will then be defined as:
\begin{equation}
  \rho_h^* = \rho_h, \quad
  \vel_h^* = \vel_h, \quad
  P_h^* = P_b, \quad
  \nut_h^* = \nut_h.
\end{equation}
Which fully defines the inviscid numerical flux functions.

% ---------------------------- VISCOUS - Internal & Bdry ----------------------
\subsection{Viscous Flux}

The viscous flux functions are evaluated in a different manner
compared to their inviscid counterparts. When evaluating internal flux
values, an intermediate state $\vvec u_h^*$ is used in the form:
\begin{equation}
  \vvec \NumFluxQ_I
  (
  \vvec u_h^+, \nabla \vvec u_h^+,
  \vvec u_h^-, \nabla \vvec u_h^-
  ) =
  \mmat Q(\vvec u_h^*, \nabla \vvec u_h^*)\nvec n.
\end{equation}
Finding the intermediate solution as the numerical average of the left
and right states,
\begin{equation}
  \vvec u_h^* = \frac{1}{2} \left( \vvec u_h^+ + \vvec u_h^-
  \right),
\end{equation}
results in a sufficiently accurate solution \cite{nishikawa2011}. When
evaluating the intermediate solution gradient, however, simple
averaging may lead to spurious solutions and instabilities. Nishikawa
\cite{nishikawa2010} suggested the following modified formula, in
lines with the interior penalty formulation \cite{arnold1982} used in
the DG framework:
\begin{equation}
  \nabla \vvec u_h^* =
  \frac{1}{2} \left( \nabla \vvec u_h^+ + \nabla \vvec u_h^- \right) +
  \eta \left(
    \frac{\vvec u_h^+ - \vvec u_h^-}{ \|\xyz_{\tau+} - \xyz_{\tau-} \|_2  }
  \right)   \nvec n,
\end{equation}
where $\xyz_{\tau-}$ and $\xyz_{\tau+}$ are the reference locations of
the adjacent control volumes, respectively, and $\eta$ is a heuristic
damping factor, known as the jump term. Jalali \etal{}
\cite{jalali2014} have numerically tested different values of $\eta$
in high-order finite volume simulations, and have recommended a value
of $\eta=1$, which is also used in this thesis.

The boundary viscous fluxes are evaluated in the same manner as
reference \cite{jalali2017}, by simply replacing the interior state
into the viscous flux matrix,
% 
$
\vvec \NumFluxQ_B(\vvec u_h, \nabla \vvec u_h, \BdryCond) =
\mmat Q(\vvec u_h, \nabla \vvec u_h)\nvec n
$.
% 
The boundary conditions are enforced through reconstruction
constraints. For isothermal walls, the velocity, heat flux, and
turbulence working variable have to be zero. This can be expressed via
the boundary constraint:
% 
\begin{equation}
  B( \vvec u_h, \nabla \vvec u_h , \BdryCond) =
  \sqrt{
    \| \vel_h \|_2^2 +
    \| \nabla T_h \|_2^2 +
    \nut_h^2
  } \quad
  \BdryCond \text{ is isothermal wall.}
\end{equation}
Similarly, we require a zero normal derivative of the turbulence
working variable, zero heat flux, and zero mass flux for symmetry
boundary conditions. These conditions yield the boundary constraint:
% \begin{equation}
%   B( \vvec u_h, \nabla \vvec u_h , \BdryCond) =
%   \sqrt{
%     \| \vel_h -  (\vel_h \cdot \nvec n) \nvec n \|_2^2  +
%     \| \nabla \nut_h \cdot \nvec n \|_2^2 +
%     \| \nabla T_h \|_2^2
%   } \quad
%   \BdryCond \text{ is symmetry}.
% \end{equation}
The farfield boundaries do not require imposing any constraints on the
reconstruction of the discrete solution.


%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
\endinput
