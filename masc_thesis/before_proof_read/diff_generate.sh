#!/bin/bash

## Create the folder if it is not there
mkdir -p difftex

## Produce the diff files

DIFF='latexdiff -c diff_config.cfg --exclude-textcmd="section,subsection"'
echo ${DIFF}

## OLD NEW


FILE=c1_intro.tex;      ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}
FILE=c2_background.tex; ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}
FILE=c3_threed.tex;     ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}
FILE=c4_precon.tex;     ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}
FILE=c5_res.tex   ;     ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}
FILE=c6_conc.tex   ;     ${DIFF}  ${FILE} ../${FILE}  > difftex/${FILE}

