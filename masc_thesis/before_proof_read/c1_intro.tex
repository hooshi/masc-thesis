\chapter{Introduction}
\label{ch:c1_intro}

\section{Motivation}

%% HOW GOOD IS CFD
The advent of computational fluid dynamics (CFD) has considerably
improved the design and manufacturing processes in many industries
such as aerospace, turbomachinery, oil and gas, and bioengineering.
CFD finds solutions to engineering problems by numerically solving the
governing and/or modeling partial differential equations (PDEs).
CFD simulations offer a cheap alternative to experimental setups in
many, though not all, situations while not suffering from the
limitation of analytical methods in handling complex
geometries. Nevertheless, CFD methods are not always considered a
rival to the other methods of analysis.  Computational methods are
sometimes used for tuning parameters or selecting methods of
measurement in experimental setups. For example, \citet{yoo2010cfd}
employ CFD simulations to estimate the right scale for building a
nuclear fuel cask experimental model. Conversely, many PDEs that
are solved in CFD are derived from experimental data, such as the
turbulence model of \citet{spalart1}.

%% DEFINITION OF HIGHER ORDER
A majority of CFD methods are categorized as mesh-based discretization
schemes.  A mesh-based discretization scheme seeks to approximate the
solution to a PDE $\vvec{\mathcal{L}}\vvec u(\xyz) = 0$ inside a
domain $\Omega \subset \mathbb{R}^\ndim$, where $\vvec{\mathcal{L}}$
is a differential operator, $\vvec u(\xyz) \in \mathbb{R}^{\nunk}$ is
the unknown exact solution, and no time dependence has been assumed
for simplicity. In the first step, the domain is subdivided (meshed)
into a set of non-overlapping sub-volumes $\mathcal T_h$, followed by
defining a discrete function in the form of
$\vvec u_h(\vvec x;\vvec U_h)$. Here, the term discrete means that
this function will be uniquely defined if a finite number of degrees
of freedom, $\vvec U_h \in \mathbb{R}^{\ndof} $, are all
specified. Furthermore, the subscript $h$, which represents the
smallest length scale of the subdivision, emphasizes the dependency of
the corresponding quantities on the mesh. Finally, the goal is to find
$\vvec U_h$, such that $\vvec u_h$ closely approximates the exact
solution $\vvec u$ as the mesh gets smaller. The rigorous definition
of ``to closely approximate'' is that the discretization error,
$\vvec e_h = \vvec u_h -\vvec u$, must have an asymptotic behavior
such that: $ \| \vvec e_h \| = \BigO(h^p) $. The discretization scheme
is then said to be $p$th-order accurate.

%% WHY HIGHER-ORDER - WHEN DISCRETIZATION ERRORS ARE DOMINANT
Although conventional discretization schemes for aerodynamical flows
are only second-order accurate, there has been a growing interest in
higher-order accurate methods. Higher-order methods can be
considerably more efficient compared to second-order methods in
achieving accurate solutions, since the former results in more
accurate results on coarser meshes
\cite{fidkowski2005,nastase2006}. Even small improvements in accuracy
can be of considerable importance. For example,
\citet{vassberg2003grid} show that for a long-range jet-aircraft that
delivers a payload between distant city pairs, a $1\%$ error in
predicting the drag can reduce the carried payload by $7\%$. With the
airlines operating on profit margins of only a few percent, such a
loss can make the service completely unprofitable.


%% WHY HIGHER-ORDER - WHEN MODELING ERRORS ARE DOMINANT
Regarding the adoption of higher-order methods, there has always been
the legitimate concern of modeling errors, \ie{} the discrepancies
between the exact solution of the governing equations and the true
physical quantities. For a problem where the modeling errors are
dominant, only a limited level of reduction in the discretization
errors is of interest. Even in such a case, preliminary results have
shown that an $hp$-adaptive strategy can achieve a desired level of
accuracy faster than conventional second-order methods
\cite{leicht2008, jalali2017hp}.  An $hp$-adaptive method increases
the local order of accuracy $p$ and the mesh size $h$ at only certain
regions of the domain. In the context of design and optimization,
thousands of simulations might be required for optimizing a subject
geometry \cite{petaflops}. Thus, even a small runtime improvement for
a single simulation can result in a considerable reduction of the
overall resource consumption. Moreover, modeling errors are not always
the dominant mode. In a recent work by \citet{Mavriplis2005}, where he
studied the numerical solution of the flow around a wing-body
configuration from the AIAA drag prediction workshop (DPW), the
dominant errors were found to be those of discretization.


%% AVAILABLE NUMERICAL METHODS _ STRUCTURED
For structured meshes, highly-accurate finite difference methods have
long been developed \cite{visbal2002use, lele1992compact}, and are
known to have superior properties in terms of computational cost and
efficiency \cite{deng2012high}. Nonetheless, generation of structured
meshes for complex geometries is a challenging task, and requires
extensive amounts of human input. Therefore, grids for complex
geometries are often obtained by the fairly automatic and convenient
alternative of unstructured mesh generation techniques.

%% AVAILABLE NUMERICAL METHODS _ UNSTRUCTURED
For unstructured meshes, various approaches have been devised for
achieving higher-order accuracy, most notably: continuous
\cite{ahrabi2017adjoint} and discontinuous \cite{hartmann2016}
Galerkin finite element methods, the correction procedure via
reconstruction formulations of the discontinuous Galerkin and spectral
volume methods \cite{huynh2014high}, and finite volume schemes
\cite{jalali2017}. (see the work of \citet{andren2011comparison} for a
detailed comparison of numerical results). The use of finite volume
methods is partly motivated by their straightforward mathematical
formulation compared to the complex structure of the other mentioned
methods. Furthermore, most of the current industrial CFD codes are
based on the finite volume method. While implementing other approaches
would require the development of completely new commercial codes,
higher-order finite volume methods can be integrated into the current
industrial solvers. Finite volume methods are also attractive because
of the smaller number of degrees of freedom that they require for a
same mesh compared to finite element methods.

%% GO FOR UNSTRUCTURED FVM
In two dimensions, unstructured high-order finite volume methods have
been successfully applied to a range of aerodynamic problems: Euler
equations \cite{haider2014, michalak2009}, laminar Navier-Stokes
equations \cite{li2014efficient, jalali2017hp}, and turbulent Reynolds
Averaged Navier-Stokes (RANS) equations \cite{jalali2017}.  Although
taking the effects of turbulence into consideration is necessary for
correctly capturing many aerodynamic flows, three-dimensional results
are scarce and limited to the solution of Euler and laminar
Navier-Stokes equations \cite{li2014efficient,haider2009}.  In the
long run, the \anslab{} team at UBC is interested in developing a
three-dimensional higher-order finite volume solver for the solution
of both inviscid and viscous turbulent steady-state flow problems,
while providing approximate error bounds on target unknowns such as
lift and drag. The first step towards this goal will be taken in this
thesis: solution of well-known three-dimensional benchmark flow
problems.

\section{Objectives}

The ultimate goal of this thesis is the generalization of our current
2-D flow solver, \anslib{}, for the solution of 3-D benchmark problems
involving inviscid and viscous turbulent flows. The pursuit of this
goal is divided into the following steps:
\begin{itemize}
  % \item%
  % Choose an appropriate turbulence model that is reasonably accurate
  % for aerodynamic flows, yet is simple enough as a starting point for
  % the development of numerical algorithms.  
  % In this thesis, the objective is to employ the negative
  % Spalart-Allmaras model \cite{spalart2} for turbulent flows. This
  % one-equation model is known to give reasonable results for
  % aerodynamic flows, and its simplicity makes it a good start point
  % for the development of numerical algorithms. The negative version
  % of
  % this model is chosen because it permits negative values for the
  % model working variable, which can be present when higher-order
  % methods are employed.
\item%
  Derive the finite volume formulation of the governing equations in a
  dimension-independent manner. Of course, the choice of the
  turbulence model in this part considerably affects the solution. In
  this thesis, the objective is to employ the negative
  Spalart-Allmaras turbulence model \cite{spalart2}. This one-equation
  model is reasonably accurate for many flow conditions, and its
  simplicity makes it a good starting point for the development of
  numerical algorithms. The negative version of this model is chosen
  because it permits negative values for the model working variable,
  which can be present when higher-order methods are employed.
\item%
  Identify and undertake the preprocessing steps involved in handling
  three-dimensional grids required for turbulent simulations.
\item%
  Design a practical and parallel scalable method for the solution of
  the discretized system of nonlinear equations. Since
  three-dimensional problems can require much more memory than their
  two-dimensional counterparts, solution methods that work in two
  dimensions might not be feasible in three dimensions anymore.
\item%
  Verify the performance of the solver, and the accuracy of the
  solutions obtained.
\end{itemize}

\section{Thesis Outline}
This thesis is organized in the following manner:

\autoref{ch:c2_background} provides an overview of our in-house flow
solver \anslib{}, in which the algorithms of this thesis have been
implemented. The key concepts of the solver, \ie{} $k$-exact
reconstruction and finite volume discretization are discussed in
detail. Also, governing and modeling equations of interest are
introduced. Finally, the flux functions and the boundary conditions
are revisited in a dimension-independent notation.

\autoref{ch:c3_threed} discusses the preprocessing steps of
three-dimensional meshes for a finite volume simulation. First,
numerical integration is addressed, where Gauss quadrature rules are
employed in conjunction with reference element mappings to construct
quadrature information for the control volumes and their faces. Then,
the capturing of boundary curvature in highly anisotropic meshes is
addressed, which is a necessity for higher-order solution
methods. Finally, the issues of $k$-exact reconstruction in handling
highly anisotropic meshes are reviewed and addressed.


In \autoref{ch:c4_precon}, the solution of the discretized system of
nonlinear equations is discussed. First, the pseudo transient
continuation method is revisited, which transforms the solution of the
nonlinear system of equations into the solution of a series of linear
systems. Then, a memory lean, yet effective method for the solution of
the corresponding linear systems is proposed, and compared to the
previously available linear solution methods in \anslib{}.

\autoref{ch:c5_res} presents the solution of four three-dimensional
test problems. To test the correct implementation of the mesh
preprocessing algorithms, Poisson's equation is solved in a simple
geometry, where the discretization error is explicitly evaluated and
its asymptotic behavior is verified. Then, the subsonic inviscid flow
around a sphere is studied, where the solution accuracy is verified by
measuring the deviation of the entropy from the inflow conditions
throughout the domain. Finally, the problems of viscous turbulent flow
over a flat plate and an extruded \nacaoolz{} airfoil are solved, and
the solutions are verified against the reference data provided in the
NASA turbulence modeling (TMR) website \cite{nasatmr}.

In the end, \autoref{ch:c6_conc} summarizes the research of the
thesis, provides conclusions, and proposes possible future work.

In addition, the command-line options that were provided to the
\anslib{} executable for running each test case are provided in
\autoref{ch:app1_comm}. A sample script for running a parallel job on
the WestGrid Grex cluster \cite{westgrid} is also given in
\autoref{ch:app2_grex}, as the three-dimensional cases of this thesis
were all run on this cluster.


\endinput
%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
