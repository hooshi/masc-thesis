\chapter{Solving the Discretized System of Equations}
\label{ch:c4_precon}


% -------------------------------------------------------
% -------------------------------------------------------
% Chapter beginning -------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------

% -------------------------------------------------------
% -------------------------------------------------------
% Pseudo-Transient Continuation -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Pseudo-Transient Continuation}

% SHOW NOTATION A^(0)

% -------------------------------------------------------
% -------------------------------------------------------
% The Residual Jacobian Matrix --------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{The Residual Jacobian Matrix}

% SIMPLY USE WHAT ALIREZA HAS SAID
% MATRIX FREE

% -------------------------------------------------------
% -------------------------------------------------------
% Linear Solvers    -------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Linear Solvers}

Correct solution of the modified Newton iteration, Equation
\eqref{xx}, is the most challenging part of a scalable parallel flow
solver. The exact solution of this equation is carried out by
combinatorial algorithms that typically calculate a lower-upper
factorization of the LHS matrix. In other words, they find a
lower-triangular matrix $\mmat L$ and an upper-triangular matrix
$\mmat U$, such that $\mmat A = \mmat L \mmat U$. The equation
$\mmat A \vvec x = \vvec b$ can subsequently be solved by a simple
forward and backward elimination. Although there has been great
progress in the development of such methods, e.g. the family of
parallel scalable multifront methods \cite{xx}, they still suffer from
huge consumption of memory and time resources when applied to
sufficiently large problems. Furthermore, these methods do not take
advantage of any initial guess for the solution nor the fact that only
an approximate solution is of interest. As a result, scientists in the
CFD community have resorted to the iterative methods for the solution
of huge linear system of equations.

Iterative methods are either stationary or a member of the Krylov
subspace family. Stationary methods have the form:
\begin{equation}
  \label{eq:stadef}
  \vvec x^{(k+1)}= \vvec x^{(k)} + \mmat P \vvec r^{(k)},
\end{equation}
where $\vvec x^{(k)}$ is the $k$th guess for the solution,
$\vvec r^{(k)} = \vvec b - \mmat A \vvec x^{(k)}$, and $\mmat P$ is a
constant matrix. Subsequently, the update in the solution at each step
is only dependent on the residual vector of its own. In a Krylov
subspace method, on the other hand, some or all the previous residual
vectors are used in updating the solution vector,
\begin{equation}
  \label{eq:kspdef}
  \vvec x^{(k+1)}= \vvec x^{(k)}
  + \KSPm(\mmat A; \vvec r^{(k)}, \ldots, \vvec r^{(1)}),
\end{equation}
with $\KSPm$ representing the nonlinear update in the
solution. Although the stationary and \KSP{} methods are usually
ineffective on their own, they can be combined through the
preconditioning concept to make up efficient solvers.

We will first introduce the preconditioned \GMRES{} Krylov subspace
method as implemented in our numerical library of choice, \petsc{}
\cite{balay2012}, followed by presenting simple single level
preconditioning methods and their combination in building a scalable
preconditioner for three-dimensional problems.

% ----------------------------------------------------------------------
% SECTION: GMRES
% ----------------------------------------------------------------------
\subsection{GMRES}

The Generalized minimal residual method (\GMRES{}) \cite{saad1986} is
used \anslib{} as the baseline solver for the PTC iterations. This
method has been chosen due to its flexibility in handling nonsymetric
LHS matrices, history of promising results for higher-order problems
\cite{x}, and its ability to enhance the performance of traditional
stationary iterative solvers by using them as preconditioners
\cite{x}. At each iteration, $k$, the \GMRES{} method iteratively
constructs an orthognal basis
$\Set{\vvec v^{(0)}, \vvec v^{(1)}, \ldots \vvec v^{(k-1)}}$ for the
Krylov subspace
$ \Span\Set{\vvec r^{(0)}, \mmat A \vvec r^{(0)}, \ldots, \mmat
  A^{k-1} \vvec r^{(0)}} $. Then, it seeks the next approximate
solution vector $\vvec x^{(k)} = \sum_i \alpha_i \vvec v^{(i)}$ such
that $\| \vvec r^{(k)} \|_2$ is minimized over all values of
$\alpha_i$. Furthermore, the Krylov subspace is usually restarted
after a certain number of iterations to prevent it from becoming too
large.

The behavior of the \GMRES{} method is strongly dependent on the
eigenvalue structure of the matrix $\mmat A$. The smaller the
eigenvalue spectrum the fewer iterations required for convergence. As
the LHS matrices arising from the $k$-exact FVM descretization usually
lack a nice eigenvalue distribution, the equations have to be
preconditioned to enhance the performance of the \GMRES{} solver.
Preconditioning can either be done as a left or right operation. In
the former case the equation
$ \mmat P \mmat A \vvec x= \mmat P \vvec b $ is solved instead of the
original equation, while the latter uses the equations
$ \mmat A \mmat P \vvec y= \vvec b, \mmat P \vvec y = \vvec x $. Since
the preconditioned \GMRES{} algorithm deals with matrices
$ \mmat P \mmat A$ or $\mmat A \mmat P$, its performance will improve
dramatically if the condition number, $\kappa$, of these modified
matrices are sufficiently smaller than that of $\mmat A$.

The left preconditioned \GMRES{} algorithm strictly requires $\mmat P$
to be a constant linear operator, \ie{} a matrix. Further, unlike the
original \GMRES{} algorithm that minimizes the norm
$\| \vvec r^{(k)} \|_2$ at each iteration, this version minimizes
$\| \mmat P \vvec r^{(k)} \|_2$. However, we have
\[
  \frac{ \| \mmat P \vvec r^{(k)} \|_2 } { \| \mmat P \vvec r^{(0)} \|_2 } 
  \leq \kappa(\mmat P)
  \frac{ \| \vvec r^{(k)} \|_2 } { \|  \vvec r^{(0)} \|_2 }, 
\]
which means that the two norms are equivalent for a well behaved
matrix $\mmat P$.

The right preconditioning algorithm, on the other hand, minimizes the
same norm as the original \GMRES{} method. Moreover, it is more
flexible and can handle nonlinear $\mmat P$ operators, e.g. $\mmat P$
can include a \GMRES{} solver itself. In this case, however, the
memory requirments of the algorithm doubles, as it also has to store
the vectors $\vvec z^{(k)}=\mmat P \vvec v^{(k)}$. Thus, the name
Flexible \GMRES{} (FGMRES) is used to distinguish the normal and
double memory consuming versions of the algorithm \cite{saad1993}.

Following \petsc{} authors right and left preconditioned linear
solvers are denoted by $\GMRESm-_R\mmat P$ and $\GMRESm-_L\mmat P$,
respectively \cite{brune2015}. Using this notation, even a stationary
iterative solver as in Equation \eqref{eq:stadef} can be recongnized
as $\RICHm-_L\mmat P$, where Richardson is the simplest stationary
iterative method in the form:
\begin{equation}
  \vvec x^{(k+1)}= \vvec x^{(k)} + \vvec r^{(k)}.
\end{equation}
In this way, stationary and preconditioned Krylov subspace solvers can
all be placed in a unified framework. Finally, the operator $\mmat P$
is defined as:
\begin{equation}
  \label{eq:predef}
  \mmat P \vvec v = \LISm(\mmat M, \vvec v),
\end{equation}
where $\vvec v$ is a dummy vector, $\mmat M$ can be the LHS matrix or
an approximation of it, and $\LISm$ is a preconditioner linear solver
that approximates the solution, $\vvec z$, of the equation
$\mmat M \vvec z = \vvec v$.

% ----------------------------------------------------------------------
% SECTION: Finding lines of high unknown coupling
% ----------------------------------------------------------------------
\subsection{Finding Lines of Strong Unknown Coupling}

Forming non-overlapping lines that contain control volumes with
strongly coupled average values in Equation \eqref{xx} is extremely
beneficial in constructing preconditioners. As described in Section
\ref{ch:c4_precon_bb}, these lines can be used for matrix
decomposition schemes or unknown reordering in Jacobi/Gauss-Sidel and
ILU preconditioners, respectively. Mavriplis \cite{mavriplis1998}
introduced the concept of lines in anisotropic unstructured meshes to
enhance the performance of multigrid solvers via implicit
smoothing. His approximate algorithm for finding such lines was purely
geometric based, and only considered control volume aspect ratios,
taking into account only coupling through diffusion. Okusanya \etal{}
\cite{okusanya2004} later developed an algorithm that considered both
the advection and diffusion phenomena. Fidkowski \etal{}
\cite{fidkowski2005} proved that such an algorithm results in a set of
unique lines and used them for non-linear $p$-multigrid solution of
Eulers equations, while Diosady and Darmofal \cite{diosady2009} showed
that these lines are powerful tools for linear $p$-multigrid and ILU
reordering schemes as well.

The scheme of Okusanya \etal{} has been implemented to build more
efficient preconditioners in \anslib{} for three-dimensional
simulations. In the first step of this algorithm, a weight is assigned
to every face inside the mesh. This weight is derived from the
Jacobian of the $0$-exact discretization of the advection-diffusion
equation,
\[
  \nabla \cdot (\vel u - \mu_L \nabla u) = 0,
\]
where $u$ is the unknown, $\vel$ is the velocity taken from the
initial conditions, and $\mu_L$ is an input parameter which acts as a
weight for the effect of diffusion. The weight of the face $f$ is then
defined as:
\[
  W(f) = \max
  \left(
    \frac{\partial R_\sigma }{\partial u_\tau},
    \frac{\partial R_\tau }{\partial u_\sigma}
  \right ),
\]
with $W$ representing the weight of the face, $\sigma$ and $\tau$ the
adjacent control volumes of the face, and $R$ the residual vector. As
this strategy is not directly applicable to boundary faces, a mirrored
ghost control volume must be used. Then, Algorithm \ref{alg:line} will
fully construct the lines, where $F(\tau)$ is the set of faces of
control volume $\tau$, and $\sigma=C(\tau, f)$ is the control volume
which has the face $f$ in common with control volume $\tau$. Briefly,
this algorithm ensure that two adjacent control volumes are connected
to each other if and only if their common face has the rank of first
or second in weight for both of them.

\begin{algorithm}
  \caption{Line creation}
  \label{alg:line}
  \begin{algorithmic}
    \Function{CreateLines}{}
    \Comment{Calling this function creates all lines.}
    \State Unmark all control volumes.
    \While{There a exists an unmarked control volume $\tau$}
    \State \Call{MakePath}{$\tau$}
    \Comment{Forward path creation}
    \State \Call{MakePath}{$\tau$}
    \Comment{Backward path creation}
    \EndWhile
    \EndFunction
    \vspace{0.3in}
    \Function{MakePath}{$\tau$}
    \Comment{Helper function.}
    \Repeat
    % 
    \parState{ For control volume $\tau$, pick the face $f \in F (\tau)$
      with highest weight, such that control volume $ \sigma= C(\tau,f)$
      is not part of the current line.}
    %
    \If{ $f$ is a boundary face}
    \State Terminate
    \ElsIf{$\sigma$ is marked}
    \State Terminate
    \ElsIf{$f$ is not the first or second ranked face of $\sigma$ in weight}
    \State Terminate
    \EndIf
    %
    \State Mark $\sigma$.
    \State Add $\sigma$ to the current line.
    \State $\tau \gets  \sigma$
    \Until{Termination}
    \EndFunction
  \end{algorithmic}
\end{algorithm}

To illustrate the performance of the algorithm, we consider an
anisotropic mesh for the NACA0012 airfoil geometry as shown in Figure
\ref{fig:c4_lines}. The velocity has been taken from the $2$-exact
solution of the flow resulting from a Mach number of $0.15$, a
Reynolds number of $6\times10^6$, and angle of attack of
$10^\circ$. The parameter $\mu_L$ is set to the heuristic value of
$10^{-5}$. As it is desired, the lines follow the direction of mesh
anisotropy near wall boundaries, while their pattern changes and
follows the flow direction in other parts of the geometry.

\begin{figure}
  \centering
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_view_whole.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_lines_whole.eps}
  \end{subfigure}

  \vspace{0.5in}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_view_close.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth, center]{img/sts_lines_close.eps}
  \end{subfigure}
  \vspace{0.5in}

  \caption[Lines of high unknown coupling]{Lines of high unknown
    coupling, NACA0012 airfoil with $\Mach=0.15$,
    $\Reyn=6\times 10^6$, $\alpha=10^{\circ}$, and $\mu_L=10^{-5}$}
  \label{fig:c4_lines}
\end{figure}

% ----------------------------------------------------------------------
% SECTION: Stationary iterative methods
% ----------------------------------------------------------------------
\subsection{Preconditioning Building Blocks}
\label{ch:c4_precon_bb}

% We will now introduce simple yet effective choices of $\mmat M$ and
% $\LISm$ that might not be usefull on their own, but can be combined to
% build effective preconditioners. Also, the previous preconditioning
% method used in \anslib{} will be presented along with its defects when
% facing equations arising from three-dimensional geometries.

ILU($k$) is the preconditioner that has been used in \anslib{} for
two-dimensional problems, which builds an approximate lower-upper
factorization of the LHS matrix, \ie{}
$\mmat M = \tilde{\mmat L} \tilde{\mmat U}$. Where $\tilde{\mmat U}$
and $\tilde{\mmat L}$ are only approximates of the original
factorization of $\mmat A$, and have a fill structure resembling that
of $\mmat A^k$. The preconditioner linear solver, $\LISm$, in this
method is simply exact inversion, as the the matrix $\mmat M$ is the
product of two triangular matrices. Since this method is known to work
for a variety of problems, and is offered as a black box in most
numerical libraries, it is usually the first candidate to be
implemeted in CFD solvers. The catch is that there is not much
theoretical guarantee for the performance of this method neither is
there scalable parallelized versions of it readily available. Further,
the memory cost of this method can become unaffordable for high-order
three-dimensional problems, where a typical ILU($3$) factorization can
take up to ten times more memory than the original LHS matrix.

Since the ILU preconditioner is based on matrix factoring, it is
dramatically affected by the ordering of \dof s in the matrix
$\mmat A$. Quotient minimum degree (QMD) and reverse Cuthill-McKee
(RCM) are among the famous reordering algorithms that are offered in
\petsc{} and are used in \anslib{}. The former is aimed at reducing
the fill of the factored matrices $\mmat L \mmat U$, while the latter
tries to reduce the fill of the matrix $\mmat A$ itself. Another novel
reordering method which has been proved quite effective is to number
the \dof s according to the previously presented lines of highest
coupling, such that members of a line are numbered sequentially one
after the other.

The Jacobi/Gauss-Sidel family is another simple yet effective method
of preconditioning. The idea is to decompose the matrix $\mmat A$ into
three diagonal $\mmat D$, lower triangular $\mmat L$, and upper
triangular $\mmat U$ matrices, such that
$\mmat A = \mmat D + \mmat L + \mmat U$, which is then used to define
$\mmat M=\mmat D$ or $\mmat M=\mmat D + \mmat L$ for the Jacobi and
Gauss-Sidel preconditioners, respectively. The Gauss-Sidel method is
tremendously better performing than the Jacobi method. Nevertheless,
complications that arise from implementing the former in parallel,
\eg{} graph coloring and job synchronization, are hardly worth the
trouble. Thus, Gauss-Sidel is mostly confined to serial cases, only,
and paralellization is achieved by a processor decomposed Jacobi
method that will be described shortly.

There are different ways to decompose the LHS matrix which have an
inverse relation between computational cost and robustness. The most
common decomposition methods used in this work---control volume, line,
and partition decomposition---are shown in Figure
\ref{fig:c4_decomp_mats}. The first two methods only work well for
decomposition of the LHS matrix $\mmat A^{(0)}$ arising from the
$0$-exact reconstruction scheme, and fail miserably when derived from
the LHS matrix of higher-order discretizations. Nonetheless, as
discussed in the next section, they can still prove beneficial when
used as the base of a hierarchic preconditioner. The control volume
decomposition method assigns only the \dof s of a single control
volume to each block in the $\mmat D$ matrix, which performs
excellently for Euler or Poisson equations on isotropic meshes, while
failing in the face of highly anisotropic meshes. Many sources suggest
a line block decomposition to mitigate this shortcoming, where the
matrix $\mmat D$ will become tri-diagonal for a compact $0$-exact line
reordered discretization, allowing it to simply get inverted using the
Thomas tri-diagonal solver. Due to programming complications, however,
this work has only included the implementation of the Jacobi version
of the line block decomposition preconditioner, which has proven to be
inferior to ILU($0$) with line reordering. Nonetheless, we are
strongly hopeful about the performance of the Gauss-Sidel version of
this method and firmly recommend its implementation as a part of the
future works.

\begin{figure}
  \centering
  % CV
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.7\linewidth,
    center]{img/naca0012_trunc_cv-figure0.eps}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth,
    center]{img/naca0012_trunc_cv.eps}
  \end{subfigure}
  \vspace{0.5in}
  
  % LINES
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.7\linewidth,
    center]{img/naca0012_trunc_line-figure0.eps}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth,
    center]{img/naca0012_trunc_line.eps}
  \end{subfigure}
  \vspace{0.5in}
  
  % PROC
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.7\linewidth,
    center]{img/naca0012_trunc_proc-figure0.eps}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=0.99\linewidth,
    center]{img/naca0012_trunc_proc.eps}
  \end{subfigure}
  \vspace{0.5in}
  
  \caption[Jacobi/Gauss-Sidel matrix decomposition
  methods]{Jacobi/Gauss-Sidel matrix decomposition methods: Control
    volume based (top), line based (middle), and partition based
    (bottom) methods are presented. The right figures show the control
    volumes whose corresponding rows belong to the same diagonal block
    in the LHS matrix, and the left figures show the decomposition of
    the LHS matrix with the diagonal block highlighted in color.}
  \label{fig:c4_decomp_mats}
\end{figure}


The processor decomposition method is the simplest yet still an
effective parallel preconditioner, where each diagonal block of matrix
$\mmat D$ corresponds to one of the processors involved in the
simulation, and contains all the rows and columns associated with the
control volumes belonging to that processor. This requires the \dof s
to be ordered such that the member control volumes of a processor have
consecutive numbers. Not surprisingly, Gauss-Sidel iterations are not
parallel scalable for this method at all, since one processor has to
wait until all its neighbouring processors have updated their
solutions. Thus, only the Jacobi version of this decomposition is used
in practice. Unlike its aforementioned counterparts, the matrix
$\mmat D$ cannot be inverted exactly anymore.  Thus, the $\LISm$, as
in Equation \eqref{eq:predef}, should itself be an iterative
solver. But since each block belongs only to a single processor, it
allows serial preconditioners such as ILU and Gauss-Sidel to be used
for the construction of $\LISm$. For example, we will use the solver
GMRES$-_R$JACOBI\_PROC$-_L$RICHARDSON$-_L$GS\_CV in Chapter \ref{xx}
for the solution of Euler equations in parallel. Where JACOBI\_PROC
and GS\_CV are the processor decomposed Jacobi and control volume
decomposed Gauss-Sidel preconditioners, respectively.

Other common preconditioners that have been used for higher-order
aerodynamic simulations include the multigrid and domain decomposition
methods. Although, there are theoretical guarantees for the
performance of these methods in ideal conditions, they are not robust
at all, \ie{} there are many different factors involved in these
methods such as restriction, prolongation, smoothing, and coarsening
that can be done in many different ways, with only a few of them
bringing about desired results. The correct choice of these factors,
however, is not an easy task, and is different from case to case. This
can be a reason regarding the inconsistent results in the literature
obtained by applying the linear multigrid method to DG
discretizations. Shahbazi \etal{} \cite{shahbazi2009} have reported up
to ten times speed up by using linear multigrid, while Diosday and
Darmofal \cite{diosady2009}, and Wallraff \etal{} \cite{wallraff2015}
have only observed less than two times the speed up, compared to
conventional single grid methods. Therefore, we have only considered
ILU and Jacobi/Gauss-Sidel preconditioners in this work.

%% INTRODUCE THE DAMN NOTATION

% ----------------------------------------------------------------------
% SECTION: Design of an effective preconditioning strategy
% ----------------------------------------------------------------------
\newcommand{\preconA}[1]{
  GMRES$(A^{(#1)})-(\text{ILU3}(A^{(k)}))^{-1}$ }
\newcommand{\preconB}[1]{
  GMRES$(A^{(#1)})-(\text{ILU0}(A^{(0)}))^{-1}$ }
\newcommand{\preconC}[1]{
  GMRES$(A^{(#1)})-$GMRES($A^{(k)}$)$-(\text{ILU0}(A^{(0)}))^{-1}$ }
\newcommand{\preconD}[1]{
  GMRES$(A^{(#1)})-$GMRES($A^{(0)}$)$-(\text{ILU0}(A^{(0)}))^{-1}$ }

\subsection{Design of an Effective Preconditioning Strategy}

Michalak and Ollivier-Gooch \cite{michalak2010} showed that an ILU
preconditioner based on the full order LHS matrix, makes up an
efficient preconditioning strategy for solving Equation
\eqref{xx}. Based on the notation introduced in Equation \eqref{xx},
this strategy is denoted as \preconA{k} when applied to a
$k$-exact scheme. Michalak and Ollivier-Gooch further showed that
storing the factored matrix only takes up to 50-100\% more memory
compared to storing the LHS matrix itself. Nevertheless, they only
studied two-dimensional problems. Jallali and Ollivier Gooch
\cite{jalali2017} used the same strategy for the solution of turbulent
flows in two-dimensions, and showed that an ILU fill level of three or
larger is required for solving such problems. Unfortunately, this
strategy is impractical for three-dimensional problems. As it will be
shown in Chapter \ref{xx}, the factored matrix resulting from an ILU3
preconditioner can take up to ten times more memory than the LHS
matrix of a $3$-exact discretization. Thus, a different preconditioner
must be designed that takes memory equal to or less than the LHS
matrix.

Let us use a representative problem as the testbed of the design
process. A $32000$ control volume mesh for the NACA0012 airfoil is
considered similar to that of Figure \ref{fig:c4_decomp_mats}, with
the same values of $\Mach=0.15$, $\Reyn=6\times10^6$, and
$\alpha=10^\circ$. We will study the performance of various
preconditioning strategies for the $3$-exact solution of the RANS +
neg S-A equations for this problem. The $3$-exact scheme is chosen
because it results in PTC iterations with the stiffest LHS
matrices. Further, we use the solution of the $2$-exact scheme as the
initial condition. Since using the solution of a lower-order method as
the intitial condition, known as order ramping, is a typical strategy
for three-dimensional problems \cite{hartmann2016}.

Figure \ref{fig:c4_designa} shows the residual history and number of
KSP iterations when the \precon{4} linear solver is used.

% PAGE 1
\begin{figure}
  \centering
  \begin{subfigure}{0.99\linewidth}
     \centering
    \includegraphics[width=0.38\textwidth]{img/sts_res_full.eps}
    \includegraphics[width=0.38\textwidth]{img/sts_kspit_full.eps}
    \caption{GMRES$(A^{(3)})-_L $ILU3$(A^{(3)})$}
    \label{fig:c4_designa}
  \end{subfigure}
   
  \begin{subfigure}{0.99\linewidth}
     \centering
    \includegraphics[width=0.38\textwidth]{img/sts_res_one.eps}
    \includegraphics[width=0.38\textwidth]{img/sts_kspit_one.eps}
    \caption{GMRES$(A^{(3)})-_L $ILU0$(A^{(0)})$}
    \label{fig:c4_designb}
  \end{subfigure}
   
  \begin{subfigure}{0.99\linewidth}
     \centering
    \includegraphics[width=0.38\textwidth]{img/sts_res_ansksp.eps}
    \includegraphics[width=0.38\textwidth]{img/sts_kspit_ansksp.eps}
    \caption{FGMRES$(A^{(3)})-_R $GMRES$(A^{(3)})-_L $ILU0$(A^{(0)})$}
    \label{fig:c4_designc}
  \end{subfigure}
  
  \begin{subfigure}{0.99\linewidth}
    \centering
    \includegraphics[width=0.38\textwidth]{img/sts_res_ksp.eps}
    \includegraphics[width=0.38\textwidth]{img/sts_kspit_ksp.eps}
    \caption{ FGMRES$(A^{(3)})-_R $GMRES$(A^{(0)})-_R $ILU0$(A^{(0)})$}
    \label{fig:c4_designd}
  \end{subfigure}
  
  \caption[Residual history and number of \KSP{} iterations for
  different precondiotiong strategies]{Residual history (left) and
    number of \KSP{} iterations per PTC iteration (right) for
    different preconditioning strategies for NACA0012 airfoil with
    $\Mach=0.15$, $\Reyn=6\times 10^6$, and $\alpha=10^{\circ}$ are
    shown.}
  \label{fig:c4_design}
\end{figure}
% PAGE 2
% \begin{figure}\ContinuedFloat
%   \centering
%   \begin{subfigure}[b]{\textwidth}
%     \includegraphics[width=0.6\textwidth]{example-image}
%     \subcaption{$Q^{*}$ values for arm 3}
%     \label{fig:arm3}
%   \end{subfigure}
% \end{figure} 

% \undef\precon1
% \undef\precon2
% \undef\precon3
% \undef\precon4

%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
\endinput
