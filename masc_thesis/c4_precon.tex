\chapter{Solving the Discretized System of Equations}
\label{ch:c4_precon}


% -------------------------------------------------------
% -------------------------------------------------------
% Chapter beginning -------------------------------------
% -------------------------------------------------------
% -------------------------------------------------------

This chapter begins by introducing the Pseudo Transient Continuation
(PTC) method that finds the solution of
Equation~\eqref{eq:nonlineareq} via solving a series of systems of
linear equations. Available methods for the solution of these linear
systems are introduced, and their shortcoming in handling
three-dimensional finite volume problems is addressed. Finally, a new
solution scheme is proposed, and its performance is compared to other
previously available methods in \anslib{}.

% -------------------------------------------------------
% -------------------------------------------------------
% Pseudo-Transient Continuation -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Pseudo Transient Continuation}


The PTC method \cite{jalali2017, ceze2015, michalak2010, kelley1998}
starts from an initial guess for the steady-state solution of
Equation~\eqref{eq:nonlineareq}. The initial guess is usually taken
from the free-stream conditions or the converged solution of a
lower-order accurate scheme. Then, the solution is updated iteratively
until the norm of the residual vector, $\|\vvec R ( \vvec U_h )\|_2$,
falls below a desired threshold.  Consider the backward Euler time
advance scheme:
\begin{equation}
  \label{eq:BEULER}
  \frac{  \vvec U_h^+ - \vvec U^{}_h }{\Delta t} +
  \vvec R ( \vvec U_h ) = 0,
\end{equation}
where $\Delta t$ is the time step size, and $\vvec U_h^+$ is the the
\dof{} vector at the next time level. Linearizing
Equation~\eqref{eq:BEULER} gives:
\begin{equation}
  \label{eq:BEULERlinear}
  \left(
    \frac{\mmat I}{\Delta t} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right)
  \delta \vvec U_h =
  -\vvec R ( \vvec U_h ), 
\end{equation}
where $\delta \vvec U_h$ is the change in the \dof{} vector between
the current and next time levels. Furthermore,
$\frac{\partial \vvec R}{\partial \vvec U_h}$ is the residual Jacobian
matrix, and is evaluated using the algorithm proposed by
\citet{michalak2010}.

The PTC method is derived from the backward Euler scheme by making a
few modifications. First, a nonuniform time step is used for every
control volume. Thus, Equation~\eqref{eq:BEULERlinear} is changed to:
\begin{equation}
  \label{eq:PTC}
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right)
  \delta \vvec U_h =
  -\vvec R ( \vvec U_h ), 
\end{equation}
where $\CFL$ is the Courant-Friedrichs-Lewy number, and the diagonal
matrix $\mmat V$ is the time step scaling matrix. The entries of
$\mmat V$ corresponding to control volume $\tau$ are denoted as
$V_\tau$, and found from the equation:
\begin{equation}
  V_\tau = \frac{\lambda_{\max,\tau}}{h_\tau},
\end{equation}
where $h_\tau$ is the hydraulic diameter of the control volume, and
$\lambda_{\max,\tau}$ is the maximum eigenvalue of the inviscid flux
Jacobian over all the control volume surface quadrature points.
Secondly, after $\delta \vvec U_h$ is solved for, the solution is
updated according to the line search algorithm:
\begin{equation}
  \vvec U_h \gets \vvec U_h + \omega \delta \vvec U_h,
\end{equation}
where $\omega \in (0\quad 1]$ is the line search parameter, which must
satisfy:
\begin{equation}
  \label{eq:lsearch}
  \left \| \frac{\omega \mmat V \delta \vvec U_h}{\CFL} +
  \vvec R ( \vvec U_h + \omega \delta \vvec U_h) \right\|_2
  \leq
  \kappa \| \vvec R ( \vvec U_h ) \|_2.
\end{equation}
Here, $\kappa$ controls the strictness of the line search algorithm,
and $\kappa=1.2$ performs well for both viscous and inviscid flows
\cite{ceze2015}. In addition, the vector entries corresponding to the
turbulence working variable are omitted in the evaluation of the norms
in Equation~\eqref{eq:lsearch} to enhance convergence
\cite{wallraff2015}. Finally, the CFL number is updated at each
iteration according to the value of $\omega$:
\begin{equation}
\label{eq:cflramp}
  \CFL \gets
  \begin{cases}
    1.5 \CFL    &  \omega = 1 \\
    \CFL        &   0.1 < \omega < 1 \\
    0.1 \CFL    &  \omega < 0.1 
  \end{cases}.
\end{equation}
At the beginning of the solution process, the approximate solution is
away from its steady-state value, and a small $\CFL$ number prevents
divergence by making the approximate solution follow a physical
transient path. On the other hand, when the approximate solution gets
close to its steady-state value, Equation~\eqref{eq:cflramp} will
increase the $\CFL$ number. Therefore, the effect of the term
$\frac{\mmat V}{\CFL}$ in Equation~\eqref{eq:PTC} will be reduced, and
Newton iterations would be recovered. Thus, as better solution
approximations are obtained, the convergence rate will get closer to
the optimum behavior of Newton's iterations.

% -------------------------------------------------------
% -------------------------------------------------------
% GMES Method                   -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Linear Solvers}

Every PTC iteration requires the solution of the linear system
$\mmat A \vvec x = \vvec b$, where:
\begin{equation}
  \mmat A =
  \left(
    \frac{\mmat V}{\CFL} +
    \frac{\partial \vvec R}{\partial \vvec U_h}
  \right) \quad
  \vvec x = \delta \vvec U_h \quad
  \vvec b = -\vvec R ( \vvec U_h ).
\end{equation}
The exact solution of the linear system is carried out by combinatorial
algorithms that typically calculate a lower-upper (LU) factorization
of the LHS matrix. Although there has been great progress in the
development of such methods, \eg{} MUMPS linear solver library
\cite{amestoy1998}, they still suffer from huge consumption of memory
and time resources, when applied to sufficiently large
problems. Furthermore, these methods do not take advantage of any
initial guess for the solution, nor the fact that only an approximate
solution is of interest. As a result, scientists in the CFD community
have resorted to iterative methods for the solution of huge linear
systems.

Iterative methods start from an initial guess $\vvec x^{(0)}$, and
then generate a sequence of improving approximate solutions.
Iterative methods are either categorized as stationary, or a member of
the Krylov Subspace (KSP) family. In a stationary method the
approximate solution at step $k+1$, $\vvec x^{(k+1)}$, is only
dependent on the residual vector of the previous iteration,
$\vvec r^{(k)}$, which is defined as:
\begin{equation}
  \vvec r^{(k)} =  \vvec b - \mmat A \vvec x^{(k)}.
\end{equation}
Conversely, a KSP method updates the approximate solution based on all
or some of the previous residual vectors. \anslib{} uses a KSP method,
GMRES \cite{saad2003}, as its linear solver because of its successful
history in solving aerodynamic problems \cite{pueyo1998, luo1998,
  wong2008, nejat2008, ceze2015}, and the fact that it is readily
available as a black box solver in many numerical computation
libraries, such as \petsc{} \cite{balay2012}.

The GMRES method iteratively constructs an orthogonal basis for the
search space spanned by the vectors $\vvec r^{(0)}$,
$\mmat A \vvec r^{(0)}$, \ldots, $\mmat A^{k-1} \vvec r^{(0)}$. Then,
it seeks the next approximate solution vector, $\vvec x^{(k)}$, as the
member of the search space that minimizes $\| \vvec r^{(k)} \|_2$.
Furthermore, the search space is usually restarted after a certain
number of iterations to prevent it from becoming too large. The
behavior of the GMRES method is strongly dependent on the eigenvalue
structure of matrix $\mmat A$. The more compact the eigenvalue
spectrum, the fewer iterations are required for convergence. As the
LHS matrices arising from a $k$-exact finite volume discretization usually lack
a nice eigenvalue distribution, the performance of GMRES must be
improved by means of preconditioning, which is introduced in the
following section.

% -------------------------------------------------------
% -------------------------------------------------------
% Preconditioning               -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Preconditioning}

Preconditioning is the transformation of the original linear system
into one which has the same solution, but is easier to solve using
iterative methods. Preconditioning is usually done by first finding a
matrix $\mmat P$, such that the condition number of the product matrix
$\mmat{AP}$ is smaller than that of $\mmat A$. Then, the original
equation is changed to:
\begin{equation}
  \mmat{AP} \vvec y = \mmat b \quad
  \vvec x = \mmat P \vvec y.
\end{equation}
The preconditioning matrix can be constructed either directly from
$\mmat A$, or the LHS matrix resulting from a lower-order
discretization scheme. The matrix from which the preconditioner is
constructed will be denoted as $\mmat A^*$.

The main steps of the preconditioned GMRES algorithm are shown in
\autoref{alg:gmres} \cite{saad2003}, where no restart strategy and a
zero initial guess have been assumed for simplicity. In this
algorithm, the columns of the
$\mmat{V}_m \in \mathbb{R}^{\nunk\times m}$ matrix form an orthonormal
basis for the search space spanned by the vectors $\vvec b$,
$(\mmat{AP}) \vvec b$, $\ldots$, $(\mmat{AP})^{m-1} \vvec b$. At each
iterations $m$, the $\mmat{H}_m$ and $\mmat{V}_m$ matrices are used to
find the solution guess, $\vvec x^{(m)}=\mmat{APV}_m\vvec\alpha_m$,
and the norm of its corresponding residual,
$\| \vvec r^{(m)}\|_2=\gamma_m$. Also, $n$ is the maximum number of
iterations while $\rtol$ is the tolerance in the reduction of the
residual norm. A smaller tolerance value will result in a more
accurate solution, but requires more GMRES iterations. Finally, note
that the GMRES solver only requires the matrix-by-vector product of
the $\mmat P$ matrix, and does not need its explicit form.

\begin{algorithm}
  \caption{Preconditioned GMRES with no restart and a zero initial
    guess.}
  \label{alg:gmres}
  \begin{algorithmic}
    \Function{GMRES}{$\mmat{A}, \mmat{P}, \vvec{b},n,\rtol$}
    %
    \State $\beta = \| \vvec b \|_2$%
    \State $\vvec v_1 = \vvec b / \beta$%
    \State $\mmat V_1 = \left[  \vvec{v}_1 \right]$, %
    $\mmat H_0 = \left[  \right]$%
    \State $m = 0$
    %
    \Repeat%
    \State $m=m+1$%
    \State $\vvec{z}_m = \mmat P \vvec v_m$%
    \State $\vvec{w} = \mmat A \vvec z_m$%
    \State $\vvec h_m, \vvec v_{m+1}$ = \Call{Arnoldi}{$\mmat V_m,\vvec w$}%
    \State $\mmat V_{m+1} = \left[  \mmat V_{m}, \vvec v_{m+1} \right]$, \ %
    $\mmat H_m = \left[ \mmat H_{m-1}, \vvec h_m \right]$ %
    \State $\vvec\alpha_{m},\gamma_{m}$ =%
    \Call{DenseSolve}{$\beta,\mmat H_m$}%
    \Until{$m > n$ or $\gamma_m/\beta < rtol$}%
    \State Return $\vvec \xyz^{(m)} = \mmat{PV}_{m}\vvec\alpha_{m}$%
    \EndFunction%

    \vspace{0.3in}%
    \Function{Arnoldi}{$\mmat V_m, \vvec w$}{}%
    \parState{Returns the vector $\vvec v_{m+1}$, which is the unit
      normal component of $\vvec w$ to the space spanned by the
      previous $\vvec v_i$ ($i\leq m$) vectors.}%
    \parState{Returns the column vector $\vvec h_m$, where $(h_m)_i$
      is the length of the projection of $\vvec w$ over the
      $\vvec v_i$ vector if $i \leq m+1$, and is zero otherwise.}%
    \EndFunction

    \vspace{0.3in}
    \Function{DenseSolve}{$\beta, \mmat H_m$}{}
    \parState{Returns
      $\vvec\alpha_{m} = \argmin_{\vvec\alpha} \| \vvec b -
      \mmat{APV}_{m}\vvec\alpha \|$ and
      $\gamma_{m} = \min_{\vvec\alpha} \| \vvec b -
      \mmat{APV}_{m}\vvec\alpha \|$.  }
    \parState{The objective of this function is achieved by solving
      the equivalent problem
      $ \argmin_{\vvec\alpha} \left\| \beta \vvec{e}_1 - \mmat{H}_m
        \vvec{\alpha} \right\|_2 $ using a rotational change of
      variables. Solving this problem includes the solution of a dense
      $m\times m$ linear system, and yields both $\gamma_{m}$ and
      $\vvec\alpha_{m}$. Also, $\vvec e_1 = [1,0,\ldots,0]^T$.}
    \EndFunction
  
  \end{algorithmic}
\end{algorithm}


In this section, the preconditioners available in \anslib{}, namely
Point Gauss-Seidel, Block Jacobi, and ILU will be introduced, and
their shortcoming in solving large three-dimensional problems will be
discussed.  More complicated preconditioners, such as multigrid and
domain decomposition have not been considered in this thesis because
of the inconsistent results available in the literature regarding
their performance. For example, \citet{shahbazi2009} reported that the
linear multigrid method can be ten times faster than conventional
single grid algorithms. On the other hand, \citet{diosady2009}, and
\citet{wallraff2015} reported cases for which they were not able to
achieve a significant speedup factor.


% ----------------------------------------------------------------------
% SECTION: Jacobi Preconditioner
% ----------------------------------------------------------------------
\subsection{Point Gauss-Seidel}

The Point Gauss-Seidel (PGS) method is a stationary iterative solver
that can also be used as a preconditioner for the GMRES method. First,
$\mmat A^*$ is decomposed as:
\begin{equation}
  \mmat A^* = \mmat L + \mmat D + \mmat U,
\end{equation}
where $\mmat D$ is the control volume diagonal part of $\mmat A$,
$\mmat L$ is the strict lower part, and $\mmat U$ is the strict upper
part. Then, the matrix-by-vector product $\mmat P \vvec v$ of the PGS
preconditioner is defined as:
\begin{equation}
  \begin{aligned}
  &\vvec z^{(0)} = 0 \\
  &\vvec z^{(1)} =
  (\mmat D + \mmat L)^{-1}(\vvec v - \mmat A^* \vvec z^{(0)}) \\
  & \vdots \\
  &\vvec z^{(n)} =
  (\mmat D + \mmat L)^{-1}(\vvec v - \mmat A^* \vvec z^{(n-1)}) \\
  & \mmat P \vvec v = \vvec z^{(n)},
  \end{aligned}
\end{equation}
where $n$ is the number of PGS iterations. Although PGS is a strong
preconditioner for solving linear systems arising from discretization
on isotropic meshes, it performs poorly when dealing with anisotropic
meshes \cite{mavriplis1999}.  Therefore, PGS is only used for the
solution of Poisson's and Euler equations in this thesis, for which an
isotropic mesh correctly captures the solution.

% ----------------------------------------------------------------------
% SECTION: ILU Preconditioner
% ----------------------------------------------------------------------
\subsection{Block Jacobi}

The Block Jacobi (BJ) preconditioner also uses a decomposition of
matrix $\mmat A^*$, with the difference that the diagonal blocks of
matrix $\mmat D$ must be composed of all the degrees of freedom
belonging to the same processor. \autoref{fig:c4_bjacobi}a shows a
partitioned mesh, and \autoref{fig:c4_bjacobi}b shows the
corresponding structure and decomposition of the LHS matrix.
\begin{figure}
  \centering
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/naca0012_trunc_proc.eps}
  \end{subfigure}
  % PROC
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.7\linewidth]{img/naca0012_trunc_proc-figure0.eps}
  \end{subfigure} \\
  (a) \hspace{0.23\linewidth} \hfil (b)
  % 
  \caption[Schematic of Block Jacobi decomposition]{Schematic of Block
    Jacobi decomposition: (a) The mesh, and the partition belonging to
    each processor (b) The lower, upper, and block diagonal matrices. }
  \label{fig:c4_bjacobi}
\end{figure}
The matrix-by-vector product for this preconditioner is denoted as:
\begin{equation}
  \begin{aligned}
    &\vvec z^{(0)} = 0 \\
    &\mmat D \vvec z^{(1)} =
    \vvec v - \mmat A^* \vvec z^{(0)} \\
    & \vdots \\
    &\mmat D \vvec z^{(n)} =
    \vvec v - \mmat A^* \vvec z^{(n-1)} \\
    & \mmat P \vvec v = \vvec z^{(n)},
  \end{aligned}
\end{equation}
where $n$ is the number of BJ iterations. Note that the block diagonal
matrix $\mmat D$ cannot be inverted exactly anymore, and each
intermediate vector $\vvec z^{(k)}$ must be solved for by using an
inner iterative solver. Nevertheless, since every block of $\mmat D$
only belongs to a single processor, the inner iterative solver can be
serial. Thus, BJ is a means of parallelizing serial preconditioners,
and is used for this purpose in \anslib{}.

% ----------------------------------------------------------------------
% SECTION: ILU Preconditioner
% ----------------------------------------------------------------------
\subsection{ILU}

The incomplete lower-upper factorization with fill level $p$ (ILU$p$)
produces an approximation of the LU factorization of $\mmat A^*$, such
that the approximate matrices have a much smaller nonzero structure
than the exact LU factorization. The factored lower and upper
triangular matrices $\IL$ and $\IU$ are computed by performing
Gaussian elimination on $\mmat{A}^*$, but ignoring certain matrix
entries. Then, the preconditioner matrix is set to
$\mmat P = (\IL{}\IU{})^{-1}$. A larger fill level results in $\IL{}$
and $\IU{}$ matrices with bigger nonzero structure, but is likely to
construct a more effective preconditioner
\cite{saad2003}. Nevertheless, the ILU method is inherently a serial
algorithm, so it is implemented in conjunction with the BJ method for
parallel simulations.

Since the ILU preconditioner is based on matrix factoring, its
performance is dramatically affected by the ordering of \dof s in the
matrix $\mmat A^*$. Quotient minimum degree (QMD) and reverse
Cuthill-McKee (RCM) are among the reordering algorithms that are
offered in \petsc{} \cite{balay2012}, and are used in \anslib{}. The
former reduces the fill of the factored matrices, while the latter
reduces the fill of matrix $\mmat A^*$ itself.

In the course of development of \anslib{}, \citet{nejat2008} used an
ILU preconditioner factored from the LHS matrix of the $0$-exact
scheme to solve the Euler equations. They showed the good performance
of their preconditioning method for 1- and 2-exact finite volume
schemes, but concluded that the 3-exact scheme requires a more
effective preconditioner. Later, \citet{michalak2010} demonstrated
that incomplete factorization of the higher-order LHS matrix results
in faster convergence compared to factoring the $0$-exact LHS matrix
for inviscid problems. They further conjectured that factoring the LHS
matrix to the full order using a fill level of $3$ has a feasible
memory consumption, and can be a practical preconditioner for
three-dimensional flow problems. Hereinafter, the ILU preconditioner
of fill level $p$ factored from the $0$-exact and full-order LHS
matrices will be denoted as LO-ILU$p$ and HO-ILU$p$, respectively.

Viscous flow problems are more challenging than their inviscid
counterparts. \citet{jalali2017} observed that a fill level of three
or larger is required for HO-ILU preconditioning of viscous turbulent
flow problems. The HO-ILU$3$ method is a practical preconditioner for
two-dimensional problems, but its memory cost soars drastically in
three-dimensions, hindering its implementation for such problems. To
mitigate this issue, a new ILU based algorithm will be introduced in
the next section that not only has a less memory consumption than
HO-ILU$3$, but also does not suffer from poor performance of LO-ILU
when used with the $3$-exact reconstruction scheme.

% -------------------------------------------------------
% -------------------------------------------------------
% Proposed Algorithm            -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Improved Preconditioning Algorithms}

In this section, an effective preconditioning algorithm is proposed
based on inner GMRES iterations. Furthermore, the lines of strong
unknown coupling are presented for reordering of the LHS matrix. As
will be shown in \autoref{ch:c4_cmp}, this reordering method can
improve the speed of the solver considerably.

% ----------------------------------------------------------------------
% SECTION: Inner GMRES Iterations
% ----------------------------------------------------------------------
\subsection{Inner GMRES Iterations}
\label{ch:c4_innergmres}

Some researchers have observed that the lower-order LHS matrix can
construct a more effective preconditioner compared to its higher-order
counterpart because the structure of the lower-order LHS matrix only
includes the immediate neighbors of every control volume
\cite{wong2008, luo1998, pueyo1998}. Nevertheless, as will be shown in
\autoref{ch:c4_cmp}, the LO-ILU method can behave poorly for
high-order reconstruction schemes applied to viscous flow
problems. Our conjecture is that using the exact inverse of the
lower-order LHS matrix as the preconditioner:
$\mmat{P}=(\mmat{A^*})^{-1}$, rather than the product of the ILU
matrices: $\mmat{P}=(\IL\IU)^{-1}$, can mitigate the mentioned
issue. In this case, the matrix-by-vector product
$\vvec{z}=\mmat{P}\vvec{v}$ can be found by solving the system:
\begin{equation}
  \label{eq:losystem}
  \mmat{A}^* \vvec{z}=\vvec{v}.
\end{equation}
Nevertheless, the linear system of Equation~\eqref{eq:losystem} can be
as large as the original linear system $\mmat A \vvec x = \vvec b$,
and its solution cannot be carried out exactly. Instead of seeking the
exact solution, the proposal is to use further inner ILU
preconditioned GMRES iterations to find an approximate solution for
Equation~\eqref{eq:losystem}. The resulting preconditioner will be
referred to as GMRES-LO-ILU in this thesis, and its performance will be
examined in \autoref{ch:c4_cmp}.

When an inner GMRES solver is used as the preconditioner, $\mmat P$
will not be a linear operator anymore, and its effect changes from
iteration to iteration. Therefore, the equation
$\xyz^{(m)}=\mmat{PV}_m\vvec{\alpha}_m$ cannot be used any longer. The
solution to this issue is using a Flexible GMRES (FGMRES) outer solver
\cite{saad1993}. The resulting combination, \ie{} a GMRES-LO-ILU
preconditioned FGMRES solver, is shown in \autoref{alg:fgmres}.
Unlike normal GMRES, the $\vvec z$ vectors are found by applying the
inner GMRES solver to the $\vvec v$ vectors, and are explicitly kept
track of as the columns of the $\mmat Z$ matrix. Moreover, the final
approximate solution is written as
$\xyz^{(m)}=\mmat Z_m \vvec \alpha_m$ while the ILU factored matrices
are still supplied to the algorithm for preconditioning the inner
GMRES solver. Also, tolerance values and limits for the maximum number
of iterations have to be specified for both the inner and outer
solvers which are identified by the subscripts $(.)_i$ and $(.)_o$,
respectively.

\begin{algorithm}
  \caption{GMRES-LO-ILU preconditioned FGMRES with no restart and a
    zero initial guess.}
  \label{alg:fgmres}
  \begin{algorithmic}
    \Function{FGMRES}{$\mmat{A}, \mmat{A}^*, \IL, \IU,
      \vvec{b},n_o,\rtol_o,n_i,\rtol_i$}
    %
    \State $\beta = \| \vvec b \|_2$%
    \State $\vvec v_1 = \vvec b / \beta$%
    \State $\mmat V_1 = \left[  \vvec{v}_1 \right]$, %
    $\mmat H_0 = \left[  \right]$, %
    $\mmat Z_0 = \left[  \right]$, %   
    \State $m = 0$
    %
    \Repeat%
    \State $m=m+1$%
    \State $\vvec z_m=$%
    \Call{GMRES}{$\mmat{A}^*, (\IL\IU)^{-1}, \vvec v_m, n_i, \rtol_i$}%
    \State $\vvec{w} = \mmat A \vvec z_m$%
    \State $\vvec h_m, \vvec v_{m+1}$ = \Call{Arnoldi}{$\mmat V_m,\vvec w$}%
    \State $\mmat V_{m+1} = \left[  \mmat V_{m}, \vvec v_{m+1} \right]$, %
    $\mmat H_m = \left[ \mmat H_{m-1}, \vvec h_m \right]$, %
    $\mmat Z_m = \left[ \mmat Z_{m-1}, \vvec z_m \right]$ %
    \State $\vvec\alpha_{m},\gamma_{m}$ =%
    \Call{DenseSolve}{$\beta,\mmat H_m$}%
    \Until{$m > n$ or $\gamma_m/\beta < rtol$}%
    \State Return $\vvec \xyz^{(m)} = \mmat{Z}_{m}\vvec\alpha_{m}$%
    \EndFunction%

  \end{algorithmic}
\end{algorithm}
%----------------------------------------------------------------------
% SECTION: Lines of Strong Unknown Coupling
% ----------------------------------------------------------------------
\subsection{Lines of Strong Coupling Between Unknowns}
\label{c4:lines}

Forming non-overlapping lines that contain control volumes with
strongly coupled degrees of freedom is beneficial in constructing
effective preconditioners. \citet{mavriplis1998} introduced the
concept of lines in anisotropic unstructured meshes to enhance the
performance of multigrid solvers via implicit smoothing. His
approximate algorithm for finding such lines was purely
geometric-based, and only considered control volume aspect
ratios. Thus, his method only captured coupling through
diffusion. \citet{okusanya2004} later developed an algorithm that
considered both the advection and diffusion
phenomena. \citet{fidkowski2005} proved that such an algorithm results
in a set of unique lines, and used the lines for nonlinear
$p$-multigrid solution of Euler equations. \citet{diosady2009} showed
that reordering the unknowns, such that members of a line have
consecutive numbers, is an effective reordering strategy for the ILU
preconditioner. In this thesis, the line reordering algorithm of
\cite{diosady2009} is implemented to improve the speed of the linear
solver.

In the first step of the line creation algorithm, a weight is assigned
to every face inside the mesh. This weight is derived from the
Jacobian of the $0$-exact discretization of the advection-diffusion
equation,
\begin{equation}
  \nabla \cdot (\vel u - \mu_L \nabla u) = 0,
\end{equation}
where $u$ is the unknown variable, $\vel$ is the velocity taken from
the initial conditions, and $\mu_L$ is an input parameter that
controls the sensitivity of the lines to mesh anisotropy. The weight
of a face $f$ is then defined as:
\begin{equation}
  \label{eq:edge_lw}
  W(f) = \max
  \left(
    \frac{\partial R_\sigma }{\partial u_\tau},
    \frac{\partial R_\tau }{\partial u_\sigma}
  \right ),
\end{equation}
where $W$ is the weight of the face, $\sigma$ and $\tau$ are the
adjacent control volumes of the face, and $\vvec{R}$ the residual
vector. If a face is located on the boundary,
Equation~\eqref{eq:edge_lw} will not directly be applicable to
it. Thus, a mirrored ghost control volume is placed adjacent to every
boundary face. After finding the face weights, calling
\autoref{alg:line} will construct the lines, where $F(\tau)$ is the
set of faces of control volume $\tau$, and $\sigma=C(\tau, f)$ is the
control volume which has the face $f$ in common with control volume
$\tau$. This algorithm ensures that two adjacent control volumes are
connected to each other if and only if they have their first or second
highly weighted face lying between them \cite{fidkowski2005}.

\begin{algorithm}
  \caption{Line creation}
  \label{alg:line}
  \begin{algorithmic}
    \Function{CreateLines}{}
    \Comment{Calling this function creates all lines.}
    \State Unmark all control volumes.
    \While{there a exists an unmarked control volume $\tau$}
    \State \Call{MakePath}{$\tau$}
    \Comment{Forward path creation}
    \State \Call{MakePath}{$\tau$}
    \Comment{Backward path creation}
    \EndWhile
    \EndFunction
    \vspace{0.3in}
    \Function{MakePath}{$\tau$}
    \Comment{Helper function.}
    \Repeat
    % 
    \parState{ For control volume $\tau$, pick the face
      $f \in F (\tau)$ with the highest weight, such that control
      volume $ \sigma= C(\tau,f)$ is not part of the current line.}
    %
    \If{ $f$ is a boundary face}
    \State Terminate
    \ElsIf{$\sigma$ is marked}
    \State Terminate
    \ElsIf{$f$ is not the first or second ranked face of $\sigma$ in weight}
    \State Terminate
    \EndIf
    %
    \State Mark $\sigma$.
    \State Add $\sigma$ to the current line.
    \State $\tau \gets  \sigma$
    \Until{Termination}
    \EndFunction
  \end{algorithmic}
\end{algorithm}

To illustrate the performance of the algorithm, an anisotropic mesh
for the geometry of a \nacaoolz{} airfoil is considered, as shown in
\autoref{fig:c4_lines}. The velocity has been taken from the $2$-exact
solution of the flow resulting from a Mach number of $0.15$, a
Reynolds number of $6\times10^6$, and an angle of attack of
$10^\circ$. The parameter $\mu_L$ is set to the heuristic value of
$10^{-5}$. As desired, the lines follow the direction of mesh
anisotropy near wall boundaries, while their pattern changes, and
follows the flow direction in other parts of the geometry.

\begin{figure}
  \centering
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/sts_view_whole.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/sts_lines_whole.eps}
  \end{subfigure}

  \vspace{0.5in}
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/sts_view_close.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=0.99\linewidth]{img/sts_lines_close.eps}
  \end{subfigure}
  \vspace{0.5in}

  \caption{Mesh and lines of strong unknown coupling for the geometry of
    a \nacaoolz{} airfoil}
  \label{fig:c4_lines}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% Numerical Comparisons         -------------------------
% -------------------------------------------------------
% -------------------------------------------------------
\section{Numerical Comparisons}
\label{ch:c4_cmp}

In this section, the performance of the proposed preconditioning
algorithm is studied by considering the turbulent flow around a
\nacaoolz{} airfoil, with $\alpha=10^\circ$, $\Mach=0.15$, and
$\Reyn=6 \times 10^6$. Three nested meshes with approximately $25$K,
$100$K, and $400$K control volumes ($\ncv=25$K,$100$K, and $400$K) are
considered, where the coarsest mesh and its corresponding lines of
strong unknown coupling are shown in \autoref{fig:c4_lines}. The
farfield boundaries are located almost $500$ chords away from the
airfoil, and the chord length has a nondimensional value of one.  This
problem is taken from the NASA Turbulence Modeling Resource (TMR)
website \cite{nasatmr}, and has been previously studied by
\citet{jalali2017}, where they used a HO-ILU$3$ preconditioner with
QMD reordering. Only the highest-order discretization scheme of
\anslib{} ($3$-exact) is considered here, since it results in the
stiffest linear systems that are most difficult to solve. Also, the
initial conditions are taken from the steady-state solution of the
lower-order $2$-exact scheme.


The ILU based preconditioning methods tested are shown in
\autoref{tab:c4_pname}. In all cases, the outer GMRES solver stops
when the linear residual of Equation~\eqref{eq:PTC} is reduced by a
factor of $10^{-3}$, or a maximum number of $500$ iterations is
performed. The maximum Krylov subspace size is $100$ for the outer
GMRES solver, and the initial $\CFL$ number is set to
$10^{-2}$. Furthermore, the simulation ends when the norm of the
residual vector $\| \vvec R(\vvec U_h) \|_2$ is reduced by a factor of
$10^{-8}$. The command-line options supplied to the \anslib{}
executable for each case are listed in \autoref{ch:app1_comm}. Cases
A-E are compared to each other on the $25$K and $100$K meshes, while
case E$^*$ is used to solve the problem on the finest mesh. Note that
other combinations of preconditioner and reordering schemes that are
not considered in \autoref{tab:c4_pname}, did not result in
convergence even for the coarse mesh. Moreover, an extensive search
for finding the optimum number of inner iterations $n_i$ has not been
carried out, and the large number of inner iterations for case E$^*$
is chosen as a safe measure to ensure convergence on the finest mesh.


\begin{table} \centering
  \caption{Preconditioning methods considered}
  \label{tab:c4_pname}
  \begin{tabular}{ccc c}
    \hline
    Case &Preconditioning &Reordering 
    &\multirow{2}{*}{$\rtol_i,n_i$}          \\
    name &method   &algorithm                        \\
    \hline
    A         &HO-ILU$3$        &QMD
     &---                                          \\
    B         &LO-ILU$0$        &RCM
     &---                                          \\
    C         &LO-ILU$0$        &lines
     &---                                          \\
    D         &GMRES-LO-ILU$0$  &RCM
     &$10^{-3},10$                                   \\
    E         &GMRES-LO-ILU$0$  &lines   
     &$10^{-3},10$                                    \\
    E$^*$         &GMRES-LO-ILU$0$  &lines
     &$10^{-3},60$                                    \\    
    \hline
  \end{tabular}
\end{table}


The residual history of the solver for each preconditioning algorithm
is shown in \autoref{fig:c4_hiscoarse}, where the wall time is
obtained from a single core of an Intel i7-4790 ($3.60$ GHz) CPU. Our
proposed preconditioning algorithm (case E) outperforms all the other
methods on both mesh sizes, and takes only half the time of the
HO-ILU$3$ algorithm (case A) to find the steady-state
solution. Furthermore, the line reordering algorithm shows its
prominence on the $100$K control volume mesh, where the RCM reordering
algorithm fails.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{img/sts_res_comparison.eps}
  \caption{Comparison of residual histories for the \nacaoolz{} problem
    obtained using different preconditioning algorithms}
  \label{fig:c4_hiscoarse}
\end{figure}
The effectiveness of the new preconditioning algorithm is further
demonstrated by solving the problem on a $400$K control volume
mesh. The residual history for this case is shown in
\autoref{fig:c4_hisfine}.
\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{img/sts_res_sfine.eps}
  \caption{Residual history for the \nacaoolz{} problem on the finest
    mesh of $400$K control volumes}
  \label{fig:c4_hisfine}
\end{figure}

\autoref{tab:c4_pperf} shows the number of PTC iterations (\nptc{}),
the number of outer GMRES iterations (\ngmres{}), total memory
consumption, CPU time spent on the linear solver (\lst{}), and total
computational time (\tct{}). The proposed preconditioning scheme (case
E) speeds up the linear solve process by a factor of three, and
results in a twofold reduction in memory consumption, compared to the
HO-ILU$3$ method. The shortcomings of the LO-ILU methods (cases B and
C) are evident because of their large number of PTC iterations and
failure in convergence, on the coarse and medium meshes,
respectively. Also, the memory consumption scales linearly with
respect to the problem size for the proposed preconditioning scheme,
whereas the linear solve time seems to be increasing at a much more
rapid rate.  Although the proposed preconditioning scheme has a huge
linear solve time for $\ncv=400$K, it still outperforms the HO-ILU3
method, which cannot handle this mesh size at all.

\begin{table}
  \centering
  \caption[Performance comparison for different preconditioning
  schemes]{Performance comparison for different preconditioning
    schemes. Cases with entries marked by ``$-$'' did not converge
    to the steady state solution.}
  \label{tab:c4_pperf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    Preconditioner &\nptc   &\ngmres
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=25$K} \\
    \hline
          A  &$37$   &$956$      &$1.4$      &$416$   &$635$  \\
          B  &$44$   &$10,893$   &$0.7$      &$264$   &$572$  \\
          C  &$51$   &$13,692$   &$0.7$      &$328$   &$705$  \\
          D  &$34$   &$1,856$    &$0.7$      &$134$   &$379$   \\
          E  &$34$   &$1,787$    &$0.7$      &$118$   &$361$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%
    \multicolumn{6}{c}{$\ncv=100$K} \\
    \hline
          A  &$39$  &$4,640$   &$5.9$      &$3,122$  &$4,068$     \\
          B  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          C  &$-$   &$-$       &$2.8$      &$-$      &$-$         \\
          D  &$-$   &$-$       &$3.2$      &$-$      &$-$         \\
          E  &$36$  &$4,396$   &$3.2$      &$1,308$  &$2,348$      \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=400$K} \\
    \hline
    E$^*$  &$38$   &$6,869$    &$13.1$      &$87,312$ &$91,502$ \\
    \hline
  \end{tabular}
\end{table}

The viscous drag ($C_{Dv}$), the pressure drag ($C_{Dp}$), and the
lift ($C_{L}$) coefficients computed from the numerical solution along
with their reference values are listed in \autoref{tab:c4_coeffs}. The
reference values are taken from the NASA TMR website, and are obtained
by the FUN3D \cite{nasafun3d} solver running on a very fine mesh
(approximately 2M quadrilaterals). As expected, the change in the
coefficients between the fine and medium meshes is much smaller than
that of the medium and coarse meshes. Furthermore, the gap between the
coefficients and their reference values gets smaller with mesh
refinement, with the exception of $C_L$. Solution singularities,
different boundary condition implementations between \anslib{} and
FUN3D, or inexact evaluation of the distance from wall function may
partly be accountable for the non-ideal behavior of the lift
coefficient. Nevertheless, the purpose of this section was the
solution of the discretized system of equations, which according to
\autoref{fig:c4_hisfine} has been performed correctly.

\begin{table}
  \centering
  \caption{Computed drag and lift coefficients for the \nacaoolz{}
    airfoil}
  \label{tab:c4_coeffs}
  \begin{tabular}{*{4}{c}}
    \hline
    $\ncv$    &$C_{Dp}$ &$C_{Dv}$ &$C_L$ \\
    \hline
    25K     &$0.00545$ &$0.00583$ &$1.0951$ \\
    100K    &$0.00615$ &$0.00623$ &$1.0905$ \\
    400K    &$0.00609$ &$0.00619$ &$1.0922$   \\
    \hline
    NASA TMR  &$0.00607$ &$0.00621$ &$1.0910$ \\
    \hline
  \end{tabular}
\end{table}


\endinput

%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
