\chapter{Three-Dimensional Results}
\label{ch:c5_res}

This chapter presents the $k$-exact finite volume solution of four
three-dimensional problems. The problems are studied in order of
difficulty, and the solutions are verified to assess the accuracy and
performance of the solver. First, Poisson's equation is solved in a
cubic domain, where the source term is obtained from a manufactured
solution \cite{salari2000}.  Then, the Euler equations are solved to
simulate the subsonic inviscid flow around a unit sphere.
Subsequently, the solution of Navier-Stokes + negative S-A equations
are presented to simulate the subsonic viscous turbulent flow over a
flat plate and the extruded \nacaoolz{} airfoil. All the command-line
options provided to the solver for running each case are provided in
\autoref{ch:app1_comm}.

All the flow simulations were performed using the Grex cluster from
the WestGrid computing facilities \cite{westgrid}. Each node of the
cluster consists of two 6-core Intel Xeon X5650 2.66GHz processors,
and has a minimum of 48GB memory. All compute nodes are connected by a
non-blocking Infiniband 4X QDR network.  The wall time and memory cost
of all the flow problems are reported using 8 processors. In addition,
a strong parallel scaling test is conducted for the flat plate and the
sphere test cases, \ie{} the same problem is solved by using different
numbers of processors ranging from 1 to 10 while the resulting speedup
is recorded.


% -------------------------------------------------------
% -------------------------------------------------------
% POISSON                       -------------------------
% -------------------------------------------------------
% -------------------------------------------------------

\section{Poisson's Equation in a Cubic Domain}
\label{ch:c5_1}

For this test case, Poisson's equation is considered inside the domain
$\Omega=[0,1]^3$. The manufactured exact solution
\begin{equation}
  u = \sinh(\sin(x_1))\sinh(\sin(x_2))\sinh(\sin(x_3))
\end{equation}
is used to find the source term, while homogeneous Dirichlet conditions
are imposed over all the boundaries. Slice plots of the exact solution
are shown in \autoref{fig:c5_1_exact}. The solution has a maximum
value of $(\sinh(1))^3$ at the point
$(\frac{1}{2},\frac{1}{2},\frac{1}{2})$, and decays to zero on the
boundaries.

\begin{figure}
  \centering
  \includegraphics[width=0.4\linewidth]{img/result_1_exact.png}\hspace{0.2in}
  \includegraphics[width=0.07\linewidth]{img/result_1_bar.png}
  \caption{Exact solution of Poisson's problem}
  \label{fig:c5_1_exact}
\end{figure}

A study of the solution accuracy is performed on four families of
unstructured grids. The first grid family is only composed of
hexahedra while the other families are composed of only prisms,
mixture of pyramids and tetrahedra, and only tetrahedra,
respectively. The hexahedral grids are constructed by perturbing the
vertices of a uniform $N\times N\times N$ structured mesh. Other grid
families are subsequently created by decomposing each hexahedron into
the desired element types.  \autoref{fig:c5_1_mesh} shows all the
grids for $N=5$.

\begin{figure}
  \centering
  \setlength{\tempwidth}{0.35\linewidth}
  \includegraphics[width=\tempwidth]{img/result_1_mesh_hex.png} \hfil %
  \includegraphics[width=\tempwidth]{img/result_1_mesh_prism.png} \\
  \makebox[\tempwidth][c]{(a)}\hfil%
  \makebox[\tempwidth][c]{(b)}\hfil \\
  \includegraphics[width=\tempwidth]{img/result_1_mesh_pyramid.png} \hfil%
  \includegraphics[width=\tempwidth]{img/result_1_mesh_tet.png} \\
  \makebox[\tempwidth][c]{(c)}\hfil%
  \makebox[\tempwidth][c]{(d)}\hfil 
  \caption[Different meshes for the solution of Poisson's
  problem]{Different meshes for the solution of Poisson's problem:
    (a) hexahedra, (b) prisms, (c) mixture of pyramids and tetrahedra,
    (d) tetrahedra.}
  \label{fig:c5_1_mesh}
\end{figure}

The problem is solved on grid sizes: $N=10,20,40$, and reconstruction
orders: $k=1,2,3$. \autoref{fig:c5_1_error} shows the $L_2$ norm of
the discretization error $\|e_h\|_2$ as a function of the mesh length
scale $h=\frac{1}{N}$ and the number of degrees of freedom. Optimal
accuracy order of $k+1$ is attained for the $k=1$ and $3$
reconstruction schemes as expected, whereas the $k=2$ scheme only
achieves a second-order accurate solution. Nevertheless, the
non-optimal behavior of $k=2$ is expected for Poisson's problem, and
was previously addressed and theoretically explained by
\citet{ollivier2002}.

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{img/result_1_norms.eps} \hfil \\
  (a) \hfil \\
  \includegraphics[width=0.8\linewidth]{img/result_1_norms2.eps} \hfil \\
  (b) \hfil \\
  \caption[Discretization error versus mesh size for Poisson's
  problem]{Discretization error versus mesh size for Poisson's
    problem: (a) Error versus mesh length scale, (b) Error versus
    number of degrees of freedom.}
  \label{fig:c5_1_error}
\end{figure}


% -------------------------------------------------------
% -------------------------------------------------------
% SPHERE EULER                  -------------------------
% -------------------------------------------------------
% -------------------------------------------------------

\section{Inviscid Flow Around a Sphere}
\label{ch:c5_2}

The second test problem is the inviscid flow around a sphere with unit
radius, and centered at the point $(0,0,0)$. The Mach number is equal
to $\Mach=0.38$, and the free-stream flow is in the $x_1$
direction. This problem is chosen as a three-dimensional extension of
the inviscid flow around a circle, the higher-order solution of which
was studied by \citet{bassi1997}. Three prismatic grids ($64$K,
$322$K, and $1$M control volumes) are used, where the farfield is
located at a distance of $100$ dimensionless units from the sphere
surface. A symmetric cut of the coarsest mesh near the sphere surface
is shown in \autoref{fig:c5_2_mesh}.  The faces and control volumes
adjacent to the wall boundaries are curved using only second-order
Lagrange polynomials, as quadratic functions are sufficient to
represent a spherical surface. To ensure that numerical quadrature
does not introduce additional error in the solution, a quadrature
scheme accurate up to order $k+2$ is employed.
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/result_2_mesh.png}
  \caption{Symmetric cut of the coarsest mesh near the sphere surface}
  \label{fig:c5_2_mesh}
\end{figure}


In all cases, the initial solution is the uniform free-stream
condition everywhere, and the initial $\CFL$ number is set to 10. The
GMRES linear solver stops when the linear residual of Equation
\eqref{eq:PTC} is $10^{-3}$ times its initial value, or a maximum of
$500$ iterations is performed. Furthermore, the GMRES solver has a
maximum Krylov subspace size of $100$, and is preconditioned using the
BJ preconditioner with $n_o=4$ iterations. The inner solver for the BJ
preconditioner is PGS with $n_i=4$ iterations. Although the LHS matrix
of the linear system is calculated to full-order, its preconditioner
is constructed from the LHS matrix of the $0$-exact reconstruction
scheme.
% The solution process is stopped when the $L_2$ norm of the residual
% vector gets smaller than $10^{-10}$

As the flow does not contain any discontinuity, the entropy must be
constant throughout the domain. Therefore, the $L_2$ norm of the
entropy relative to the free-stream conditions, $\| S-S_\infty \|_2$,
has an exact value of zero, and is used to study the solution
accuracy. The convergence of $\| S-S_\infty \|_2$ with mesh refinement
is shown in \autoref{fig:c5_2_entnorm}, where the mesh length scale is
defined as $h=(\ncv)^{-1/3}$. The $k=1$ and $3$ reconstruction schemes
converge even faster than their expected ratios of $2$ and $4$,
respectively. The convergence ratio for the $k=2$ reconstruction
scheme, however, is half an order smaller than its nominal value. The
slight deviation of the convergence orders from their theoretical
values can partly be attributed to the fact that the meshes employed
are not nested.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}
  \caption{Relative entropy norm versus mesh size for the sphere
    problem}
  \label{fig:c5_2_entnorm}
\end{figure}

The effect of the reconstruction order $k$ on solution accuracy is
also evident from the qualitative Mach contours on the $x_3=0$
symmetry plane, shown in \autoref{fig:c5_2_mach}. Not surprisingly,
only the $k=3$ reconstruction scheme has been able to capture the
symmetry of the flow on the finest mesh. Furthermore, the artificial
wake behind the sphere seems smaller for the $k=2$ solution compared
to that of $k=1$.

\begin{figure}
  \setlength{\tempwidth}{0.35\linewidth}
  \settoheight{\tempheight}{\includegraphics[width=\tempwidth]{img/result_2_h2p2.eps}}
  \centering
  \hspace{\baselineskip}
  \columnname{$\ncv = 64$K}\hfil
  \columnname{$\ncv = 1$M}\\
  \vspace{0.2in}
  \rowname{$k=1$}
  \includegraphics[width=\tempwidth]{img/result_2_h2p2.eps}\hfil
  \includegraphics[width=\tempwidth]{img/result_2_h4p2.eps} \\
  \vspace{0.2in}
  \rowname{$k=2$}
  \includegraphics[width=\tempwidth]{img/result_2_h2p3.eps}\hfil
  \includegraphics[width=\tempwidth]{img/result_2_h4p3.eps} \\
  \vspace{0.2in}
  \rowname{$k=3$}
  \includegraphics[width=\tempwidth]{img/result_2_h2p4.eps}\hfil
  \includegraphics[width=\tempwidth]{img/result_2_h4p4.eps} \\
  \caption{Computed Mach contours on the $x_3=0$ symmetry plane for the sphere
    problem}
  \label{fig:c5_2_mach}
\end{figure}

The norm of the residual vector per PTC iteration is shown in
\autoref{fig:c5_2_PTC}. Convergence is independent of the
reconstruction order $k$, but the number of iterations increases as
the mesh is refined. It is noteworthy to mention that the increase in
the initial norm corresponding to mesh refinement is due to the
definition of the residual vector $\vvec R(\vvec U_h)$ in Equations
\eqref{eq:fvm} and \eqref{eq:nonlineareq}. Each entry of the residual
vector is divided by the size of its corresponding control
volume. Therefore, when the mesh is refined, the control volume sizes
become smaller, and the residual vector entries will increase.

\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{img/result_2_ptc.eps}
  \caption{Norm of the residual vector per PTC iteration for the
    sphere problem}
  \label{fig:c5_2_PTC}
\end{figure}

%% TALK ABOUT PERFORMANCE
The number of iterations and the resource consumption of the flow
solver are listed in \autoref{tab:c5_2_perf}. The time spent on linear
solves makes up less than twenty percent of the computational time,
suggesting that the Jacobian evaluation is the major computational
bottleneck for this problem. Either increasing the reconstruction
order, or the number of control volumes results in stiffer linear
systems, which require more GMRES iterations to be
solved. Nevertheless, both the solution time and memory consumption
increase linearly with the number of control volumes for all
$k$. Also, the advantage of using a high-order reconstruction scheme
can be clearly seen: According to \autoref{fig:c5_2_entnorm}, the
$k=1$ scheme needs at least one level of uniform mesh refinement on
the finest grid to achieve an entropy norm of $10^{-7}$. While such a
mesh refinement would increase the resource consumption by a minimum
factor of eight, the $k=3$ scheme achieves the same level of accuracy
by consuming only two times the same resources.

\begin{table}
  \centering
  \caption{Number of iterations and the resource consumption of the
    flow solver for the sphere problem}
  \label{tab:c5_2_perf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=64$K} \\
    \hline
    $1$  &$15$   &$182$      &$2.94$      &$24$   &$107$  \\
    $2$  &$15$   &$181$      &$3.91$      &$20$   &$112$  \\
    $3$  &$15$   &$255$      &$6.00$      &$31$   &$320$  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=322$K} \\
    \hline
    $1$  &$16$   &$207$   &$13.24$      &$134$   &$579$     \\
    $2$  &$16$   &$212$   &$14.05$      &$132$   &$620$     \\
    $3$  &$16$   &$300$   &$28.39$      &$271$   &$1,879$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$\ncv=1$M} \\
    \hline
    $1$  &$17$   &$275$      &$39.48$      &$486$   &$1,969$  \\
    $2$  &$17$   &$277$      &$45.52$      &$536$   &$2,150$  \\
    $3$  &$17$   &$385$      &$85.78$      &$793$   &$5,904$  \\
    \hline
  \end{tabular}
\end{table}

%TALK ABOUT SCALING
To test the parallel scalability of the flow solver, the sphere
problem was solved on different numbers of processors ranging from $1$
to $10$ with $\ncv=322$K and $k=3$. The parallel speedup of the total
solution process, the Jacobian evaluation algorithm, and the linear
solver scheme were computed separately, and are shown in
\autoref{fig:c5_2_scale}. Not surprisingly, the linear solver
algorithm is less scalable than that of the Jacobian integrator mainly
because the performance of the BJ preconditioner decreases with an
increase in the number of processors. The sudden dip of the parallel
speedup for $9$ processors may be due to a less favorable mesh
partitioning, or communication problems on the cluster. Regardless,
the overall solution algorithm scales very well in parallel, as the
linear solver only constitutes a small part of the overall process.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/result_2_scaling.eps}
  \caption[The parallel speedup of the solver for the sphere
  problem]{The parallel speedup of the solver for the sphere
    problem: $\ncv=322$K and $k=3$.}
  \label{fig:c5_2_scale}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% FLAT PLATE                    -------------------------
% -------------------------------------------------------
% -------------------------------------------------------

\section{Turbulent Flow Over a Flat Plate}
\label{ch:c5_3}

%% PROBLEM, GEOMETRY
For this test case, we consider a three-dimensional extension of the
flat plate verification case from the NASA TMR website. The
computational domain is $\Omega=[-0.33, 2]\times[0,1]\times[0,1]$, and
the nondimensional parameters have the values: $\Reyn=5 \times 10^6$,
$\Mach=0.2$, $\alpha=0$, and $\psi=0$. The flat plate is located on
the ${ (0 \leq x_1 \leq 2) \wedge (x_2 = 0)}$ boundary, where
adiabatic solid wall conditions are imposed. Symmetry boundary
conditions are imposed on the $(x_3=0)$, $(x_3=1)$, and
${ (-0.33 \leq x_1 \leq 0) \wedge (x_2 = 0) }$ boundaries, while other
boundaries are considered as farfield. As symmetry boundary conditions
are imposed on all the boundaries normal to the $x_3$-axis, the exact
solution of this problem must be constant in the $x_3$
direction. Therefore, the solutions obtained in this section can be
compared to the two-dimensional results from the NASA TMR website,
which are obtained by the CFL3D solver \cite{nasacfl3d} on a
$544 \times 384$ grid.

% MESHES, INITIAL COND, PRECON
The two-dimensional meshes provided by the NASA TMR website are
extruded in the $x_3$ direction to construct a series of nested
three-dimensional grids with dimensions: $60\times34\times7$,
$120\times68\times14$, and $240\times136\times28$. The coarsest mesh
is depicted in \autoref{fig:c5_3_mesh}. An anisotropic mesh is
necessary to correctly capture the flow pattern in the boundary layer
and at the plate leading edge. On each mesh, the problem is solved for
$k=1$, then for $k=2$, and finally for $k=3$. The free-stream
conditions are used as the initial conditions for $k=1$, while the
initial solution for each $k\geq 2$ is taken from the converged
solution of the $(k-1)$-exact scheme. The GMRES-LO-ILU2 method is used
as the preconditioner, with 10 inner GMRES iterations, and the RCM
reordering algorithm.  Moreover, the ILU factorization is performed
only on the diagonal block of each processor. Other solver parameters
are the same as \autoref{ch:c5_2}.

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{img/result_3_mesh.eps}
  \caption{Coarsest mesh for the flat plate problem.}
  \label{fig:c5_3_mesh}
\end{figure}

% TALK ABOUT THE NUT PLOTS
To schematically demonstrate the correctness of the solution, the
distribution of the turbulence working variable obtained from $k=3$ on
the finest mesh is plotted on the $x_3 = 0.5$ plane, and shown in
\autoref{fig:c5_3_nutus}.  The results for a $544 \times 384$ grid
provided by the NASA TMR are also shown in
\autoref{fig:c5_3_nutfun3d}. There is a close agreement between the
reference solution of NASA TMR and the solution of this thesis,
although a few undershoots of small magnitude are present in the
latter.
\begin{figure}
  \centering
  \settoheight{\tempheight}{\includegraphics[width=0.45\linewidth]{img/result_3_fun3d_nut.png}}
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[height=\tempheight]{img/result_3_anslib_nut.png}
    \caption{$3$-exact: $\min:7\times10^{-3}$, $\max:379$ }
    \label{fig:c5_3_nutus}
  \end{subfigure}\hfil%
  % 
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[height=\tempheight]{img/result_3_fun3d_nut.png}
    \caption{NASA TMR: $\min:0$, $\max:378$ }
    \label{fig:c5_3_nutfun3d}
  \end{subfigure}%
  % 
  \caption{Distribution of the turbulence working variable on the
    plane $x_3=0.5$ for the flat plate problem}
  \label{fig:c5_3_nut}
\end{figure}
% TALK ABOUT THE MU/MU PLOTS, SAME PARAGRAPH
The next quantity of interest is the distribution of the
nondimensional eddy viscosity $\frac{\mu_T}{\mu}$ on the line
$(x_1=0.97)\wedge(x_3=0.5)$, which is depicted in
\autoref{fig:c5_3_mut}. Note how the solution discontinuity at control
volume boundaries is quite evident for $k=1$, while higher-order
reconstruction schemes result in much smoother solutions. The
reference solution from the NASA TMR website is also shown in
\autoref{fig:c5_3_mut}. An acceptable agreement is observable between
our computed results and the reference values. Also, it is evident
that a higher-order reconstruction scheme leads to a more accurate
estimate of the eddy viscosity.
\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{img/result_3_mut.eps}
  \caption{Distribution of the nondimensional eddy viscosity on the
    line $(x_1=0.97)\wedge(x_3=0.5)$ for the flat plate problem}
  \label{fig:c5_3_mut}
\end{figure}

% TALK ABOUT C_D AND C_F
To further verify the numerical solution, the convergence of the drag
coefficient $C_D$ and the skin friction coefficient $C_f$ at the point
$\vvec x = (0.97,0,0.5)$ are studied with mesh
refinement. \autoref{tab:c5_3_cpf} shows the computed values on
different meshes.  The computed values converge faster for a
higher-order reconstruction scheme, such that only $k=3$ offers an
accurate solution on the medium mesh.  The reference values from the
NASA TMR library are also shown in the same table.  As desired, there
is a good agreement between the computed values using the method of
this thesis and the reference values of the NASA TMR website. The
convergence orders of $C_D$ and $C_f$ are computed using the procedure
of \citet{celik2008}, and are also shown in \autoref{tab:c5_3_cpf}.
All the reconstruction schemes have attained optimal convergence
rates. Nevertheless, care must be taken when interpreting the
estimated convergence rates because the procedure of
\citeauthor{celik2008} is most reliable when the error has already
achieved an asymptotic behavior on the coarsest mesh, which might not
necessarily be the case here.

% is accurate enough on
% the finest mesh. The $240\times 136\times28$ mesh seems to be fine
% enough for the $k=2$ and $3$ schemes, as they have both resulted in
% the same values for $C_D$ and $C_f$. The $k=1$ solution, however,
% seems to be in need of another level of mesh refinement. Thus, the
% computed convergence order of $k=1$ is less reliable than that of the
% other two schemes.

\begin{table}
  \centering
  \caption{Computed value and convergence order of the drag coefficient
    and the skin friction coefficient at the point
    $\vvec x =(0.97,0,0.5)$ for the flat plate problem}
  \label{tab:c5_3_cpf}
  \begin{tabular}{|c|ccc|ccc|}
    \cline{2-7}
    \multicolumn{1}{c|}{} &\multicolumn{3}{c|}{$C_D$} &\multicolumn{3}{c|}{$C_f$} \\
    \cline{2-7}
    %%%%%%%%%%
    \hline
    NASA TMR    &\multicolumn{3}{c|}{$0.00286$} &\multicolumn{3}{c|}{$0.00271$} \\
    \hline
    %%%%%%%%%%% 
    \hline
    \diagbox{Mesh}{$k$} &$1$ &$2$ &$3$ &$1$ &$2$ &$3$ \\
    \hline
    $60\times34\times7$  &$0.00396$ &$0.00233$ &$0.00233$
                             &$0.00350$ &$0.00228$ &$0.00222$ \\
    $120\times68\times14$  &$0.00301$ &$0.00281$ &$0.00285$
                             &$0.00283$ &$0.00268$ &$0.00271$ \\
    $240\times136\times28$  &$0.00287$ &$0.00286$ &$0.00286$
                             &$0.00274$ &$0.00273$ &$0.00273$ \\
    \hline
    %%%%%%%%%%%%
    \hline
    Convergence order    &$2.8$ &$3.3$ &$5.4$
                             &$3$ &$3$ &$5.1$ \\
    \hline
  \end{tabular}
\end{table} 

% TALK ABOUT THE CONVERGENCE
The norm of the residual vector per PTC iteration is shown in
\autoref{fig:c5_3_PTC}. Similar to \autoref{ch:c5_2}, convergence is
not affected by the reconstruction order $k$, but is slightly degraded
as the number of degrees of freedom increases.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{img/result_3_ptc.eps}
  \caption{Norm of the residual vector per PTC iteration for the flat
    plate problem}
  \label{fig:c5_3_PTC}
\end{figure}
% TALK ABOUT RESOURCE CONSUMPTION, SAME PARAGRAPH
The number of iterations and the resource consumption of the flow
solver are listed in \autoref{tab:c5_3_perf}. The majority of the
total time is spent on linear solves, showing that the linear systems
arising from viscous turbulent flows are much stiffer compared to
their inviscid counterparts. As desired, memory consumption increases
linearly with mesh refinement, whereas the linear solve time does not
scale as nicely as for the inviscid sphere problem. Most importantly,
the benefit of higher-order methods can once again be observed: While
the $k=3$ solution on the medium mesh offers the same level of
accuracy as the $k=1$ solution on the fine mesh, the computational
time of the former is smaller than that of the latter by a factor of
four.

\begin{table}
  \centering
  \caption{ Number of iterations and the resource consumption of the
    flow solver for the flat plate problem}
  \label{tab:c5_3_perf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)  \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$60\times34\times7$ mesh} \\
    \hline
    $1$  &$26$   &$844$      &$0.42$     &$31$   &$55$    \\
    $2$  &$26$   &$1,009$     &$1.35$     &$42$   &$124$   \\
    $3$  &$26$   &$1,071$     &$2.10$     &$57$   &$202$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$120\times68\times14$ mesh} \\
    \hline
    $1$  &$28$   &$1,436$   &$5.24$      &$510$   &$713$     \\
    $2$  &$29$   &$1,864$   &$8.30$      &$742$   &$1,489$     \\
    $3$  &$29$   &$2,041$   &$14.63$     &$825$   &$2,124$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{$240\times136\times28$ mesh} \\
    \hline
    $1$  &$29$   &$2,492$      &$38.77$     &$6,222$   &$7,884$  \\
    $2$  &$27$   &$3,305$      &$60.70$     &$12,353$   &$18,137$  \\
    $3$  &$27$   &$2,906$      &$121.81$    &$23,141$   &$35,412$  \\
    \hline
  \end{tabular}
\end{table}


% TALK ABOUT SCALABILITY
A strong parallel scaling test is conducted for the $k=3$ scheme on
the $120\times68\times14$ mesh. The resulting speedup for different
parts of the solver is shown in \autoref{fig:c5_3_scale}, which is
quite similar to the results of the sphere problem, and suggests that
the preconditioning algorithm is performing well for viscous flow
problems. Nevertheless, the net scaling of the solver degrades
marginally compared to the sphere problem, as linear solves take up a
bigger portion of the total solution time.
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/result_3_scaling.eps}
  \caption[The parallel speedup of the solver for the flat plate
  problem]{The parallel speedup of the solver for the flat plate
    problem: $120\times68\times14$ mesh and $k=3$.}
  \label{fig:c5_3_scale}
\end{figure}

% -------------------------------------------------------
% -------------------------------------------------------
% EXTRUDED \nacaoolz{}             -------------------------
% -------------------------------------------------------
% -------------------------------------------------------

\section{Turbulent Flow Over an Extruded Airfoil}
\label{ch:c5_4}

%% PROBLEM, GEOMETRY
For the last test case, we consider a three-dimensional extension of
the flow around the \nacaoolz{} airfoil. The computational domain of
\autoref{ch:c4_cmp} is extruded in the $x_3$ direction with an
extrusion length of one nondimensional unit. Symmetry boundary
conditions are imposed on the two boundaries normal to the $x_3$-axis,
while other boundaries retain their conditions from
\autoref{ch:c4_cmp}.  The nondimensional parameters are:
$\Reyn=6 \times 10^6$, $\Mach=0.15$, $\alpha=10$, and $\psi=0$. As the
exact solution must be constant in the $x_3$ direction, the solutions
are compared to the reference values provided by NASA TMR, which are
obtained by the FUN3D \cite{nasafun3d} solver on a $7169 \times 2049$
grid.


% MESHES, INITIAL COND, PRECON
Two meshes are employed: a hexahedral mesh with $\ncv = 100$K, and a
mixed prismatic-hexahedral mesh with $\ncv = 176$K. In both cases,
quadrature points are obtained from the tensor product of
one-dimensional quadrature formulas and the quadrature points of the
curved two-dimensional meshes. A portion of the meshes is depicted in
\autoref{ch:c5_4_mesh}.  In both cases, there are $7$ layers of
extruded control volumes in the $x_3$ direction. A value of
$\MinNeigh(3)=30$ performed acceptably for the hexahedral mesh,
whereas the same value resulted in an ill-conditioned version of
Equation \eqref{eq:recon_min} for the mixed mesh. Therefore,
$\MinNeigh(3)=60$ was used in the latter case as a safe measure to
ensure a well-posed minimization problem for Equation
\eqref{eq:recon_min}. Other solver parameters, including the order
ramping procedure for solution initialization, are the same as
\autoref{ch:c5_3}.


\begin{figure}
  \centering
  \settoheight{\tempheight}{\includegraphics[width=0.6\linewidth]{img/result_4_mesh_hex.png}}
  \includegraphics[width=0.6\linewidth]{img/result_4_mesh_hex_gmsh.eps}\\
  (a) Hexahedral mesh\\
  \vspace{0.1in}
  \includegraphics[width=0.6\linewidth]{img/result_4_mesh_mix_gmsh.eps}\\
  (b) Mixed prismatic-hexahedral mesh\\
  \caption{Meshes for the extruded \nacaoolz{} problem}
  \label{ch:c5_4_mesh}
\end{figure}

%% NUT PLOTS
The computed contours of turbulence working variable on the $x_3=0$
plane are shown in \autoref{ch:c5_4_nut} for the $k=3$ solutions. The
mixed-mesh resolves the wake region more accurately, while providing a
smoother distribution of the turbulence working variable. The superior
performance of the mixed mesh can be explained by its bigger number of
degrees of freedom, and the presence of extra flow aligned faces that
are missing in the hexahedral mesh.

\begin{figure}
  \centering
  \newcommand{\tempwidtha}{0.6\linewidth}
  \newcommand{\tempwidthb}{0.08\linewidth}
  \includegraphics[width=\tempwidtha]{img/result_4_nut_hex.png}\hfil
  \makebox[\tempwidthb][c]{} \\
  %%%%% 
  \makebox[\tempwidtha][c]{(a) Hexahedral mesh}  \hfil
  \makebox[\tempwidthb][c]{}\hfil \\
  %%%% 
  \includegraphics[width=\tempwidtha]{img/result_4_nut_mix.png} \hfil
  \includegraphics[width=\tempwidthb]{img/result_4_nut_bar.png} \\
  %%%% 
  \makebox[\tempwidtha][c]{(b) Mixed prismatic-hexahedral mesh} \hfil
  \makebox[\tempwidthb][c]{}
  %%%%% 
  \caption{Distribution of the turbulence working variable for the
    extruded \nacaoolz{} problem on the $x_3=0$ plane}
  \label{ch:c5_4_nut}
\end{figure}


%% CP PLOT
The distribution of the surface pressure coefficient on the
intersection of the airfoil and the $x_3=0.5$ plane is computed using
the $k=1$ and $3$ solutions, and depicted in
\autoref{ch:c5_4_cp}. Generally, the computed distributions are
consistent with the reference values obtained by FUN3D. Closer views
of the plots are shown for four distinct points to better compare the
results. As expected, the $k=3$ scheme predicts the most accurate
values, particularly on the mixed mesh. For example, the FUN3D results
show that the pressure coefficient on the lower surface becomes bigger
than that of the upper surface, near the trailing edge of the
airfoil. This phenomenon is captured by the $k=3$ solutions, but not
observed by those of $k=1$.

\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{img/result_4_cp.eps}
  \caption{Distribution of surface pressure coefficient on the
    intersection of the extruded \nacaoolz{} airfoil and the $x_3=0.5$
    plane.}
  \label{ch:c5_4_cp}
\end{figure}


%% CP and CF values
The computed and the reference lift and drag coefficients are shown in
\autoref{tab:c5_cpf}. It seems that the hexahedral mesh is too coarse,
as none of the schemes can obtain accurate enough solutions. For the
mixed mesh, the $k=3$ scheme gives satisfactory results with less than
$10\%$ error for all the coefficients. The solution of the other
schemes, however, is quite off, particularly for the pressure drag
coefficient $C_{Dp}$. Note that the reference results are obtained
using a $7169\times 2049$ mesh, which is more than a thousand times
finer than the meshes employed in this thesis.

\begin{table}
  \centering
  \caption{Computed drag and lift coefficients for the extruded
    \nacaoolz{} airfoil problem}
  \label{tab:c5_cpf}
  \begin{tabular}{*{4}{c}}
    \hline
    $k$    &$C_{Dp}$ &$C_{Dv}$ &$C_L$ \\
    \hline
    \multicolumn{4}{c}{NASA TMR} \\
    \hline
    $-$      &$0.00607$  &$0.00621$ &$1.0910$      \\
    \hline
    \multicolumn{4}{c}{Hex mesh} \\
    \hline
    $1$     &$0.01703$  &$0.00582$ &$1.0619$   \\
    $2$     &$0.01702$  &$0.00497$ &$1.0507$   \\
    $3$     &$0.00301$  &$0.00472$ &$1.0417$   \\
    \hline
    \multicolumn{4}{c}{Mixed mesh} \\
    \hline
    $1$     &$0.01129$  &$0.00574$ &$1.0735$   \\
    $2$     &$0.00365$  &$0.00565$ &$1.0776$   \\
    $3$     &$0.00550$  &$0.00536$ &$1.0869$   \\
    \hline
    % \multicolumn{4}{c}{$2$-D Mixed mesh} \\
    % \hline
    % $1$     &$0.01095$  &$0.00704$  &$1.0778$  \\
    % $2$     &$0.00380$  &$0.00556$  &$1.0817$  \\
    % $3$     &$0.00545$  &$0.00583$  &$1.0951$  \\
    % \hline
  \end{tabular}
\end{table}

%% CONVERGENCE WITH PTC ITERATION
The norm of the residual vector per PTC iteration is shown in
\autoref{fig:c5_4_PTC}. Convergence is marginally affected by either
the mesh type or the reconstruction order $k$. The latter is a
desirable result of using the steady-state solution of a $k$-exact
scheme as the initial guess for the $(k+1)$-exact solution.
\begin{figure}
  \settoheight{\tempheight}{\includegraphics[width=0.5\linewidth]{img/result_2_entropy.eps}}
  \centering
  \includegraphics[height=\tempheight]{img/result_4_ptc.eps}
  \caption{Norm of the residual vector per PTC iteration for the
    extruded airfoil problem}
  \label{fig:c5_4_PTC}
\end{figure}


%% RESOURCE CONSUMPTION
Finally, parameters related to the iterative convergence of the solver
are listed in \autoref{tab:c5_4_perf}. The linear solve time makes up
around $30\%$ of the total computation time. The total computation
time is strongly dependent on $k$, but does not differ considerably
between the two meshes for the same reconstruction order. Moreover,
the memory consumption seems to be linearly increasing with both $k$
and $\ncv$.


\begin{table}
  \centering
  \caption{Resource consumption of the flow solver for the extruded
    \nacaoolz{} problem}
  \label{tab:c5_4_perf}
  \begin{tabular}{*{6}{c}}
    % 
    \hline
    $k$ &\nptc{}   &\ngmres{}
    &Memory(GB)    &\lst{}(s)   &\tct{}(s)                       \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Hex mesh, $\ncv=100$K}                       \\
    \hline
    $1$  &$33$   &$1,154$      &$4.77$      &$317$      &$744$      \\
    $2$  &$31$   &$1,788$      &$6.82$      &$730$      &$2,097$    \\
    $3$  &$31$   &$2,415$      &$12.23$      &$1,057$    &$3,215$   \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
    \multicolumn{6}{c}{Mixed mesh, $\ncv=176$K}                 \\
    \hline
    $1$  &$34$   &$1,132$   &$8.70$      &$458$     &$1,164$    \\
    $2$  &$32$   &$1,769$     &$10.87$   &$800$     &$2,427$    \\
    $3$  &$31$   &$2,185$     &$26.47$   &$1,311$   &$4,666$    \\
    \hline
    %%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%% 
  \end{tabular}
\end{table}

%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
\endinput
