\chapter{Conclusions}
\label{ch:c6_conc}

%% AN INTRO
A higher-order solution framework was presented for steady-state
three-dimensional compressible flows. The highlights of this solution
framework include: a $k$-exact finite volume discretization, a PTC
solution algorithm, a memory lean preconditioner based on inner GMRES
iterations, and an ILU reordering algorithm based on lines of strong
coupling between them.

%% TALK ABOUT GENERAL FVM
Higher-order accuracy was achieved by constructing a piecewise
continuous representation of the control volume average values. This
piecewise continuous representation, also referred to as the discrete
solution, was defined as the superposition of a set of basis
functions. Monomials of Cartesian, principal, or curvilinear
coordinates were used as the basis functions while the coefficient of
each monomial in the superposition was found by solving a constrained
minimization problem. In the finite volume method, the convective
fluxes were evaluated by the Roe flux function. The viscous fluxes
were obtained by adding a stabilizing damping term to the averaged
gradients of the two adjacent states. Specifically, the flux functions
were presented for the RANS equations fully coupled with the negative
S-A turbulence model.

%% TALK ABOUT THREE-DIMENSIONAL PROCESSING
The curvature of the domain boundaries must be correctly accounted for
in high-order numerical discretization schemes, yet simply replacing
the boundary faces of highly anisotropic meshes with higher-order
representations can result in invalid intersecting elements. Thus, a
three-dimensional finite element elasticity solver was developed to
propagate the curvature of the boundary throughout the domain, and to
prevent mesh tangling. The developed solver was tested by solving a
model problem that imitated the geometry of a three-dimensional
airfoil. Also, the principal coordinates and the curvilinear
coordinates basis functions were presented to prevent the $k$-exact
reconstruction scheme from behaving poorly on curved anisotropic
meshes.

%% TALK ABOUT PTC
The PTC method was revisited for the solution of the discretized
nonlinear equations, where each iteration required the solution of a
linear system. The challenges involved in the solution of these linear
systems were addressed for three-dimensional problems, and a novel
method was introduced to mitigate these issues. In this method, the
FGMRES linear solver was preconditioned using inner GMRES iterations,
which were subsequently preconditioned by the ILU method. The $\IL$
and $\IU$ matrices were factored from the LHS matrix of the $0$-exact
discretization scheme, and the lines of strong unknown coupling were
employed to reorder the unknowns. By considering the $2$-D fully
turbulent flow around the \nacaoolz{} airfoil, it was shown that the
proposed preconditioning algorithm is more efficient both in terms of
solution time and memory consumption, compared to the other ILU based
preconditioning methods considered.

%% TALK ABOUT RESULTS
The correct implementation of element quadrature and connectivity
information in the solver was verified by solving Poisson's equation
inside a cubic domain. Subsequently, inviscid and viscous turbulent
test problems were considered, where the solution was verified against
reference values either obtained analytically or provided by the NASA
TMR website. In all cases, a satisfactory agreement was observed
between the solutions of this thesis and the reference
values. Moreover, accuracy tests were performed with mesh refinement,
and optimal convergence order was attained for reconstruction orders
$k=1$ and $3$. The $k=2$ scheme, however, was slightly inferior in
performance compared to its theoretical convergence order.

%% TALK ABOUT TIMING
Timing results demonstrated the benefit and practicality of using
higher-order methods for obtaining a certain level of accuracy. A
second-order discretization scheme required a finer grid and more
computational time to attain the same level of accuracy compared to a
higher-order discretization scheme. Furthermore, strong parallel
scaling tests were performed, and excellent scaling was observed for
the Jacobian integration algorithm. The linear solver algorithm also
demonstrated satisfactory parallel scaling results, although the
performance in this area can still be improved. It was also observed
that the required number of PTC iterations for convergence is
independent of the reconstruction order while only slightly affected
by the grid size.


%% TALK ABOUT FUTURE WORK
Much work remains to be done to extend the solution method of this
thesis to solve industrially practical problems:
\begin{enumerate}
\item%
  A theoretical study can be conducted to investigate the inferior
  performance of the $k=2$ discretization scheme. The theoretical
  approach for unstructured mesh-based discretization methods is
  usually through the functional analysis framework. A possible
  starting point can be the work of \citet{barth2002} where they prove
  theoretical convergence orders for the $k$-exact finite volume
  method.
\item%
  The boundary condition implementations of this work, though
  functional, might not be the best choice. An investigation of other
  possible boundary condition implementations has the potential to
  improve convergence properties and solution quality
  \cite{hartmann2016}.
\item%
  The proposed preconditioning algorithm does not require the explicit
  form of the full-order LHS matrix. Thus, a matrix-free method can be
  implemented, where the matrix-by-vector product of the full-order
  LHS matrix is evaluated by finite difference Fr\'echet derivatives
  while only the $0$-exact LHS matrix is assembled. This strategy can
  reduce the memory cost considerably \cite{persson2008}, while
  possibly increasing the computation time. Sacrificing computation
  time for less memory usage might be unavoidable for very large
  problems.
\item%
  In this thesis, it was possible to directly use the $2$-D
  curvilinear coordinate basis functions for $3$-D problems, as the
  solutions of the test problems considered were constant in the $x_3$
  direction. A practical problem, on the other hand, requires the
  definition of truly $3$-D curvilinear coordinate basis functions. It
  should be possible to generalize the method of \autoref{ch:c3_3} by
  providing a definition for the third wall coordinate. Alternatively,
  a big hexahedral meta-element can be constructed by extruding a
  portion of the nearest wall surface to include all the members of a
  target reconstruction stencil. The reference coordinates of the
  constructed meta-element can then be used as the curvilinear
  coordinates.
\item%
  The original $0$-exact discretization scheme does not take the
  gradient dependent source terms into account. Modifying this scheme
  to somehow capture the influence of the gradient dependent source
  terms can greatly enhance the performance of the preconditioning
  algorithm \cite{wong2008}.
\item%
  Employing a line Gauss-Seidel preconditioner instead of the ILU
  method, in conjunction with the proposed inner GMRES preconditioner
  has the potential to improve the parallel scalability of the linear
  solver \cite{ahrabi2017}.
\item%
  As already mentioned, real world problems contain solution
  discontinuities, and require shock capturing methods to be used in
  conjunction with the $k$-exact reconstruction scheme. Shock
  capturing is even more important for higher-order methods, as they
  are more prone to developing unstable and spurious solutions.
\item%
  \citet{haider2011} recently introduced a multi-level reconstruction
  scheme that uses compact reconstruction stencils, and eliminates the
  need to directly work with large stencils.  Their high-order finite
  volume method is therefore better suited for parallel simulations.
  Although their work is limited to the linear advection equation, its
  application in the context of turbulent compressible flows is worth
  investigating.
\item%
  An $hp$-adaptive strategy can be much more efficient in achieving a
  prescribed level of accuracy, compared to uniform mesh refinement,
  or using high reconstruction orders for all the control volumes
  \cite{leicht2008, jalali2017hp}. Desirably, hanging nodes resulting
  from adaptively refined meshes fit naturally into the $k$-exact
  finite volume framework. Nevertheless, an effective error estimator
  must be designed that selects the candidate regions for
  refinement. In the case of anisotropic mesh refinement, the error
  estimator must also provide an estimate of the direction in which
  the error changes most abruptly.
\end{enumerate}

\endinput
%%% Local Variables:
%%% TeX-master: "diss"
%%% End:
